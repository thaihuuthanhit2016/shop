<?php
namespace App\Http\Controllers;

date_default_timezone_set("Asia/Bangkok");


use App\Models\Card_user;
use App\Models\Customer;
use App\Models\Login;
use App\Models\Product;
use App\Models\Product_shop;
use App\Models\Shop;
use App\Models\Social;
use App\Models\Vistors;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Facades\Socialite;
use Session;
class LoginController extends Controller
{

        public function redirect($provider)
        {
            return Socialite::driver($provider)->redirect();
        }
        public function callback($provider)
        {
            $getInfo = Socialite::driver($provider)->user();

            $user = $this->createUser($getInfo,$provider);
            if ($user!=null) {
                $check = Social::where('email', $user->email)->where('provider', 'facebook',)->first();
            }else{
                $check = Social::where('provider', 'facebook',)->where('email',$getInfo->email)->first();

                $customer=Customer::where('customer_email',$check->email)->first();
                Session::put('customer_id', $customer->id_customer);
                Session::put('customer_name', $customer->customer_name);

            }
            return redirect()->to('/account');
        }
        function createUser($getInfo,$provider){
            $user = Social::where('provider_id', $getInfo->id)->first();
            if (!$user) {
                $user = Social::create([
                    'name' => $getInfo->name,
                    'email' => $getInfo->email,
                    'provider' => $provider,
                    'provider_id' => $getInfo->id
                ]);

                $checkuser = Customer::where('customer_email', $user->email)->first();
                if($checkuser==null) {
                    $customer = new Customer();
                    $customer->customer_name = $user->name;
                    $customer->customer_email = $user->email;
                    $customer->customer_password = '0';
                    $customer->customer_phone = '0';
                    $customer->customer_address = '0';
                    $customer->verifyKYC = 'N';
                    $customer->save();

                    Session::put('customer_id', $customer->id_customer);
                    Session::put('customer_name', $customer->customer_name);
                }else{

                    Session::put('customer_id', $checkuser->id_customer);
                    Session::put('customer_name', $user->name);

                }
            return $user;
        }

        }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        try {

            $user = Socialite::driver('google')->user();

            $finduser = Social::where('google_id', $user->id)->where('provider','google')->first();

            if($finduser){



                return redirect('/');

            }else{
                $newUser = Social::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'provider' => 'google',
                    'google_id'=> $user->id,
                ]);

                $finduser = Social::where('google_id', $user->id)->where('provider','google')->first();
                $checkuser = Customer::where('customer_email', $user->email)->first();
            if($checkuser==null) {
                $customer = new Customer();
                $customer->customer_name = $user->name;
                $customer->customer_email = $user->email;
                $customer->customer_password = '0';
                $customer->customer_phone = '0';
                $customer->customer_address = '0';
                $customer->verifyKYC = 'N';
                $customer->save();

                Session::put('customer_id', $customer->id_customer);
                Session::put('customer_name', $customer->customer_name);
            }else{

                Session::put('customer_id', $checkuser->id_customer);
                Session::put('customer_name', $user->name);

            }
                return redirect('/account');
            }

        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

}
