<?php
namespace App\Http\Controllers;

date_default_timezone_set("Asia/Bangkok");


use App\Models\Mail;
use App\Models\Mail_file;
use App\Models\Shop;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use Session;
class MailController extends Controller
{



    public function getMail(){

        if(!Session::get('admin_id')&&!Session::get('username')){
            return redirect('admin');

        }
        $mailinbox=Mail::where('to_id_user',Session::get('id_shop_user'))->paginate(10);

       return view('mail.x.index',compact('mailinbox'));



    }
    public function getWritenew(){

        if(!Session::get('admin_id')&&!Session::get('username')){
            return redirect('admin');

        }
        $shop=Shop::where('id_shop_user',"<>",Session::get('id_shop_user'))->get();
        return view('mail.x.writenew',compact('shop'));




    }
    public function view($id)
    {

        $view=Mail::where('id_mail',$id)->first();

        if($view->id_user!=Session::get('id_shop_user')){


            $view_update=Mail::find($id);

            $view_update->viewed=1;
            $view_update->save();
        }
        return view('mail.x.view',compact('view'));
    }
    public function reply($id)
    {
        $view=Mail::where('id_mail',$id)->first();

        return view('mail.x.reply',compact('view'));
    }
    public function postreply($id,Request $request)
    {
        if(!Session::get('admin_id')&&!Session::get('username')){
            return redirect('admin');

        }
        $mail=new Mail();
        $mail->id_user=Session::get('id_shop_user');
        $mail->to_id_user=$request->id_user;
        $mail->subject=$request->subject;
        $mail->id_parent=$id;
        $mail->viewed=0;
        $mail->content=$request->content;
        $mail->save();

        $gallery=$request->file;

        if(count($gallery)>0){





            if($request->hasFile('file')) {
                foreach($request->file('file') as $image) {
                    $mail_file= new Mail_file();

                    $mail_file->id_mail= $mail->id_mail;

                    $rand=generateRandomString(20);
                    $day=date('Y-m-d');
                    $destinationPath = 'upload/mail/';
                    $filename = $image->getClientOriginalName();
                    $file_custom=$day.'_'.$rand.'_'.$filename;

                    $image->move($destinationPath, $file_custom);
                    $mail_file->images= $file_custom;

                    $mail_file->save();


                }
            }
        }
        Session::put('message_send','Gửi thư thành công');
        return redirect('admin/send');

    }
    public function send(){

        if(!Session::get('admin_id')&&!Session::get('username')){
            return redirect('admin');

        }
        $mailsend=Mail::where('id_user',Session::get('id_shop_user'))->paginate(10);
        return view('mail.x.send',compact('mailsend'));
    }

    public function postWritenew(Request $request){

        if(!Session::get('admin_id')&&!Session::get('username')){
            return redirect('admin');

        }
        $mail=new Mail();
        $mail->id_user=Session::get('id_shop_user');
        $mail->to_id_user=$request->to_user;
        $mail->subject=$request->subject;
        $mail->id_parent=0;
        $mail->viewed=0;
        $mail->content=$request->content;
        $mail->save();
        $gallery=$request->file;

        if(count($gallery)>0){





            if($request->hasFile('file')) {
                foreach($request->file('file') as $image) {
                    $mail_file= new Mail_file();

                    $mail_file->id_mail= $mail->id_mail;

                    $rand=generateRandomString(20);
                    $day=date('Y-m-d');
                    $destinationPath = 'upload/mail/';
                    $filename = $image->getClientOriginalName();
                    $file_custom=$day.'_'.$rand.'_'.$filename;

                    $image->move($destinationPath, $file_custom);
                    $mail_file->images= $file_custom;

                    $mail_file->save();


                }
            }

        }

        Session::put('message_send','Gửi thư thành công');
        return redirect('admin/send');






    }

}
