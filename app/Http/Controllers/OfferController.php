<?php
namespace App\Http\Controllers;

date_default_timezone_set("Asia/Bangkok");


use App\Models\Brands_product;
use App\Models\Category_product;
use App\Models\Customer;
use App\Models\Customer_shop;
use App\Models\Offer;
use App\Models\Offer_customer;
use App\Models\Offer_shop;
use App\Models\Product;
use App\Models\Product_images;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Session;
class OfferController extends Controller
{



    public function getAddCategory(){
        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $data=Category_product::all();
        $brands=Brands_product::all();
        $customer=DB::table('tbl_customer_shop')->select('id_customer')
            ->where('id_shop',Session::get('id_shop_user'))->groupBy('id_customer')
            ->get();

       return view('admin.offer.addCategory',compact('data','customer','brands'));



    }
    public function getEditCategory($id){
        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $data=Category_product::all();
        $brands=Brands_product::all();
        $product=Offer::find($id);

       return view('admin.offer.editCategory',compact('data','product','brands'));



    }



    public function getListCategory(){
        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }
//        dd('s');
        if(Session::get('id_shop_user')) {
            $data = Offer_shop::where('id_shop', Session::get('id_shop_user'))->orderby('id_offer_shop',"DESC")->paginate(10);
        }else{
            $data=Offer::orderby('id_offer',"DESC")->paginate(10);
        }
       return view('admin.offer.listCategory',compact('data'));



    }
    public function postAddCategory(Request $request){
        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $dheck=Offer::where('title',$request->title)->first();

        if($dheck==null){
            $from_day=$request->from_date;
            $end_day=$request->end_date;
            $tach_from_day=explode('-',$from_day);
            $tach_end_day=explode('-',$end_day);

            if($tach_from_day[2]>$tach_end_day[2]) {
                Session::put('message_error', 'Năm kết thúc phải sau năm bắt đầu');
                Session::put('title', $request->title);
                Session::put('code_offer', $request->code_offer);
                Session::put('content', $request->content);
                return back();
            }else{
                if($tach_from_day[1]>$tach_end_day[1]) {
                    Session::put('message_error', 'Tháng kết thúc phải sau tháng bắt đầu');
                    Session::put('title', $request->title);
                    Session::put('code_offer', $request->code_offer);
                    Session::put('content', $request->content);
                    return back();
                }else{
                    if($tach_from_day[0]>$tach_end_day[0]){
                        Session::put('message_error','Ngày kết thúc phải sau ngày bắt đầu');
                        Session::put('title',$request->title);
                        Session::put('code_offer',$request->code_offer);
                        Session::put('content',$request->content);
                        return back();
                    }else{

                        $category_product=new Offer();

                        $category_product->title=$request->title;
                        $category_product->code_offer=$request->code_offer;

                        $category_product->content=$request->content;
                        $category_product->from_date=$request->from_date;
                        $category_product->end_date=$request->end_date;
                        $category_product->percent=$request->percent;
                        $category_product->number=$request->number;
                        $category_product->type=$request->type;
                        $category_product->status=1;
                        $category_product->save();

                        $offer_shop=new Offer_shop();
                        $offer_shop->id_shop=Session::get('id_shop_user');
                        $offer_shop->id_offer=$category_product->id_offer;
                        $offer_shop->status=1;
                        $offer_shop->save();
                        $offer_customer=new Offer_customer();
                        $offer_customer->id_customer=$request->user;
                        $offer_customer->id_offer=$category_product->id_offer;
                        $offer_customer->status=1;
                        $offer_customer->save();




                        Session::put('message','Cập nhật thành công');
                        return redirect('admin/offer/list');

                    }

                }


            }

        }else{
            Session::put('message_error','Tên Khuyến mãi đã tồn tại !Cập nhật không thành công');
            Session::put('title',$request->title);
            Session::put('code_offer',$request->code_offer);
            Session::put('content',$request->content);
            return back();
        }
    }
    public function postEditCategory(Request $request,$id){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }

        $dheck=Offer::where('title',$request->title)->first();

        if($dheck==null){
            $from_day=$request->from_date;
            $end_day=$request->end_date;
            $tach_from_day=explode('-',$from_day);
            $tach_end_day=explode('-',$end_day);

            if($tach_from_day[2]>$tach_end_day[2]) {
                Session::put('message_error', 'Năm kết thúc phải sau năm bắt đầu');
                Session::put('title', $request->title);
                Session::put('code_offer', $request->code_offer);
                Session::put('content', $request->content);
                return back();
            }else{
                if($tach_from_day[1]>$tach_end_day[1]) {
                    Session::put('message_error', 'Tháng kết thúc phải sau tháng bắt đầu');
                    Session::put('title', $request->title);
                    Session::put('code_offer', $request->code_offer);
                    Session::put('content', $request->content);
                    return back();
                }else{
                    if($tach_from_day[0]>$tach_end_day[0]){
                        Session::put('message_error','Ngày kết thúc phải sau ngày bắt đầu');
                        Session::put('title',$request->title);
                        Session::put('code_offer',$request->code_offer);
                        Session::put('content',$request->content);
                        return back();
                    }else{

                        $category_product= Offer::find($id);

                        $category_product->title=$request->title;
                        $category_product->code_offer=$request->code_offer;

                        $category_product->content=$request->content;
                        $category_product->from_date=$request->from_date;
                        $category_product->end_date=$request->end_date;

                        $category_product->type=$request->type;
                        $category_product->percent=$request->percent;
                        $category_product->status=1;
                        $category_product->save();



                        Session::put('message','Cập nhật thành công');
                        return redirect('admin/offer/list');

                    }

                }


            }

        }else{
            Session::put('message_error','Tên Khuyến mãi đã tồn tại !Cập nhật không thành công');
            Session::put('title',$request->title);
            Session::put('code_offer',$request->code_offer);
            Session::put('content',$request->content);
            return back();
        }



    }

    public function getDelCategory($id){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }

        $check=Offer::where('id_offer',$id)->first();
        if($check!=null){
              Offer::destroy($id);

                Session::put('message_success','Cập nhật thành công');
                return redirect('admin/offer/list');

            }

//      Category_product::destroy($id);

//        Session::put('message_success','Cập nhật thành công');
//        return redirect('category/list');


    }
    public function getLogout(){

        Session::put('admin_name','');
        Session::put('admin_id','');
        return redirect('admin');



    }

    public function postDashboard(Request $request){
            $email=$request->Email;
            $password=$request->Password;
            $result=DB::table('tbl_admin')->where('admin_email',$email)->where('admin_password',md5($password))->first();

            if($result!=null){
    Session::put('admin_name',$result->admin_name);
    Session::put('admin_id',$result->id_admin);
    return redirect('admin/dashboard');
            }else{
                Session::put('message',"Lỗi tài khoản hoặc mật khẩu chưa đúng");
                return redirect('admin');
            }



    }

}
