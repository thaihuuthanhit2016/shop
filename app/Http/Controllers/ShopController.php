<?php
namespace App\Http\Controllers;

date_default_timezone_set("Asia/Bangkok");


use App\Models\Bill;
use App\Models\Bill_detail;
use App\Models\Brands_product;
use App\Models\Brands_product_shop;
use App\Models\Card;
use App\Models\Card_user;
use App\Models\Category_product;
use App\Models\Category_product_shop;
use App\Models\Customer;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Product_images;
use App\Models\Product_shop;
use App\Models\Shop;
use App\Models\Slider;
use App\Models\Statistical;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Laravel\Socialite\Facades\Socialite;
use Session;
use PDF;
class ShopController extends Controller
{

            public  function print($check){
            $pdf=\App::make('dompdf.wrapper');
            $pdf->loadHTML($this->print_order_convert($check));
            return $pdf->stream();
        }
        public function getAddWithraw(){


            if(!Session::get('id_shop_user')){
                return redirect('admin');

            }
            $shop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();
            return view('admin.shop.addWithraw',compact('shop'));
        }
        public function posttAddWithraw(Request $request){

            if(!Session::get('id_shop_user')){
                return redirect('admin');

            }
            if($request->type_money==1) {
                $shop_money_vnd = Statistical::where('id_shop', Session::get('id_shop_user'))->where('type', 1)->sum('profit');
                $shop_widthraw_vnd = Card_user::where('id_shop', Session::get('id_shop_user'))->where('type', 1)->sum('amount_vnd');
                $tongrut=$request->Amount+$shop_widthraw_vnd;

                if($shop_money_vnd<=$tongrut){
                    Session::put('message_withraw',"Số tiền rút lớn hơn số dư");
                    return back();
                }

            }else{
                $shop_money_coin = Statistical::where('id_shop', Session::get('id_shop_user'))->where('type', 3)->sum('profit');
                $shop_widthraw_coin = Card_user::where('id_shop', Session::get('id_shop_user'))->where('type', 3)->sum('amount_coin');
                $tongrut=$request->Amount+$shop_widthraw_coin;

                if($shop_money_coin<=$tongrut){
                    Session::put('message_withraw',"Số tiền rút lớn hơn số dư");
                    return back();
                }

            }

                $shop=new Card_user();
                $shop->address=$request->address;
                $shop->status=0;
                $shop->id_card=0;
                $shop->id_shop=Session::get('id_shop_user');
                $shop->code_transfer=generateRandomString(6);
                if($request->type_money==1) {
                    $shop->amount_vnd = $request->Amount;
                    $shop->amount_coin =0;
                }
            if($request->type_money==3) {
                $shop->amount_vnd = 0;
                $shop->amount_coin =$request->Amount;
            }
            $shop->type=1;
                $shop->save();


            return redirect('admin/withraw/list');

        }
        public function postEditWithraw($id,Request $request){

            if(!Session::get('admin_id')){
                return redirect('admin');

            }
            $shop=Card_user::find($id);
            $shop->status=1;
            $shop->save();
            return redirect('admin/withraw/list');

        }
        public function getEditWithraw($id){

            if(!Session::get('admin_id')){
                return redirect('admin');

            }
                $shop=Card_user::find($id);
            return view('admin.shop.editWithraw',compact('shop'));
        }
        public function listWithraw(){
            if(!Session::get('id_shop_user')&&!Session::get('admin_id')){
                return redirect('admin');

            }
                if(Session::get('id_shop_user')) {
                    $shop = Card_user::where('type', 1)->where('id_shop', Session::get('id_shop_user'))->paginate(10);
                }else{
                    $shop = Card_user::where('type', 1)->where('status',0)->paginate(10);
                }
            return view('admin.shop.listWithraw',compact('shop'));

        }
        public function print_order_convert($check){

            $bill=Bill::where('code_bill',$check)->first();
            $billdetail=Bill_detail::where('id_bill',$bill->id_bill)->get();
            $customer=Customer::where('id_customer',$bill->id_customer)->first();
            $payment=Payment::where('id_bill',$bill->id_bill)->first();

                $html='  <style>
  body {
    font-family: DejaVu Sans, sans-serif;
 }
</style>  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<body>
<h2><center>Công ty Thiết kế và phát triển wesite Toàn Thắng</center></h2>
<h4><center>Độc Lập - Tự Do - Hạnh Phúc</center></h4>
<center><h1>Hóa đơn Bán hàng</h1></center>
<p><b>Thông tin người nhận hàng</b></p>
    <label>Tên Khách hàng:<b>';$html.=$customer->customer_name;$html.='</b></label><br>
    <label>Số điện thoại:<b>';$html.=$customer->customer_phone;$html.='</b></label><br>
    <label>Địa chỉ:<b>';$html.=$customer->customer_address;$html.='</b></label><br>
    <hr style="background: black;width: 100%;height: 1px;">
';
            $total=0;
foreach ($billdetail as $p){
    $product=Product::where('id_product',$p->id_product)->first();

    if($product->product_price_km==0){
        $price=$product->product_price;
    }else{
        $price=$product->product_price_km;
    }
    if($payment->payment_method!=3) {
        $subtotal = $p->quantity * $price;
        $total += $subtotal;
    }else{
        $subtotal = ($p->quantity * $price)*800/20000;
        $total += $subtotal;

    }
    $html.='<label>Tên sản phẩm:<b>';$html.=$product->product_name;$html.='</b></label><br>';
    if($payment->payment_method!=3) {
        $html .= '
    <label>Đơn giá:<b>';
        $html .= number_format($price, 0, ',', '.');
        $html .= ' VNĐ</b></label><br>
    <label>Số lượng:<b>';
        $html .= $p->quantity;
        $html .= '</b></label><br>
    <label>Thành tiền:<b>';
        $html .= number_format($subtotal, 0, ',', '.');
        $html .= ' VNĐ</b></label><br>';
    }else{

        $html .= '
    <label>Đơn giá:<b>';
        $html .= number_format($price*800/20000, 0, ',', '.');
        $html .= ' BITCOINSJC</b></label><br>
    <label>Số lượng:<b>';
        $html .= $p->quantity;
        $html .= '</b></label><br>
    <label>Thành tiền:<b>';
        $html .= number_format($subtotal, 0, ',', '.');
        $html .= ' BITCOINSJC</b></label><br>';
    }
    $html.='<br>';

   }
   $html.='
    <hr style="background: black;width: 100%;height: 1px;">';
        if($payment->payment_method!=3) {
            $html .= '
    <label style="float:right;">Tổng tiền:<b style="color:#fe980F;">';
            $html .= number_format($total, 0, ',', '.');
            $html .= ' VNĐ</b></label>';
        }else{

            $html .= '
    <label style="float:right;">Tổng tiền:<b style="color:#fe980F;">';
            $html .= number_format($total, 0, ',', '.');
            $html .= ' BITCOINSJC</b></label>';
        }
$html.='
</body>';

            return $html;
        }
    public function getApproveCategory($id){
        $shop=Shop::find($id);
        $shop->verifyKYC="Y";
        $shop->save();

        return redirect('admin/shop/list');

    }
    public function postEditorder($id,Request $request){
        $bill=Bill::find($id);
        $bill->status=$request->status_order;
        $bill->save();

        return redirect('admin/order/list');

    }
    public function getEditorder($id){
        $billdetail=Bill_detail::find($id);
        $bill=Bill::find($billdetail->id_bill);
        return view('admin.shop.edit',compact('bill'));
    }
    public function getDeltailOrder($id){
                if(!Session::get('id_shop_user')){
                    return redirect('admin');

                }
        $billdetail=Bill_detail::where('id_bill',$id)->paginate(10);
        $bill=Bill::find($id);
        return view('admin.shop.viewOrder',compact('billdetail','bill'));
    }
    public function getAllOrdershop(){

        if(!Session::get('id_shop_user')){
            return redirect('admin');

        }
        $bill=DB::table('tbl_bill_detail')->select('id_bill',)->groupBy('id_bill')->paginate(10);

        return view('admin.shop.order',compact('bill'));

    }
    public function getRejectCategory($id){
        $shop=Shop::find($id);
        $shop->verifyKYC="Y";
        $shop->save();

        return redirect('admin/shop/list');
    }
    public function getViewCategory($id){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }

        $data=Category_product::all();
        $brands=Brands_product::all();
        $product=Shop::find($id);

       return view('admin.shop.viewCategory',compact('data','product','brands'));



    }

    public function loginofshop($id){
                Session::flush();
        $user=Shop::where('id_shop_user',$id)->first();
        if($user){
            Session::put('id_shop_user',$user->id_shop_user);
            if($user->name_shop!=null){
                Session::put('name_shop',$user->name_shop);
            }else{
                Session::put('name_shop',$user->username);
            }
        }
        return redirect('admin/shop/info');
    }
    public function getListCategory(){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }


        $data=Shop::orderby('id_shop_user','DESC')->paginate(10);
       return view('admin.shop.listCategory',compact('data'));



    }
    public function getDelImages($id){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }

        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();
        if($checkshop->verifyKYC!='Y'){
            Session::put('message_error_kyc','Shop chưa được active!');
            return  back();
        }

        Product_images::destroy($id);
        Session::put('message_success','Cập nhật thành công');
        return back();

    }
    public function getUnactive($id){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }

        $data=Category_product::find($id);
        $data->category_status=0;
        $data->save();
        Session::put('message_success','Cập nhật thành công');
       return redirect('slider/list');



    }
    public function getActive($id){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $data=Category_product::find($id);
        $data->category_status=1;
        $data->save();
        Session::put('message_success','Cập nhật thành công');
       return redirect('slider/list');



    }
    public function postAddCategory(Request $request){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }

        $dheck=Slider::where('title',$request->title)->first();
        if($dheck==null) {
            $category_product = new Slider();

            $category_product->title = $request->title;
            $category_product->link = $request->link;

            $category_product->type = $request->loai;
            if ($request->hasFile('images')) {
                $file = $request->images->getClientOriginalName();
                $rand = generateRandomString(20);
                $day = date('Y-m-d');
                $file_custom = $day . '_' . $rand . '_' . $file;
                $category_product->images = $file_custom;
                $request->images->move('upload/slider/', $file_custom);
            }
            $category_product->save();
            return redirect('admin/slider/list');
        }
    }
    public function postEditCategory(Request $request,$id){

        if(!Session::get('admin_id')){
            return redirect('admin');

        }
        $category_product=Slider::find($id);

        $category_product->title = $request->title;
        $category_product->link = $request->link;

        $category_product->type = $request->loai;
        if ($request->hasFile('images')) {
            $file = $request->images->getClientOriginalName();
            $rand = generateRandomString(20);
            $day = date('Y-m-d');
            $file_custom = $day . '_' . $rand . '_' . $file;
            $category_product->images = $file_custom;
            $request->images->move('upload/slider/', $file_custom);
        }
        $category_product->save();
        return redirect('admin/slider/list');
    }

    public function getDelCategory($id){
        if(!Session::get('admin_id')&& !Session::get('id_shop_user')){
            return redirect('admin');

        }

        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();
        if($checkshop->verifyKYC!='Y'){
            Session::put('message_error_kyc','Shop chưa được active!');
            return  back();
        }

        $check=Category_product::where('id_category_product',$id)->first();
        if($check!=null){
            $check=Category_product::where('category_parent',$check->id_category_product)->get();
            if(count($check)==0){

              Category_product::destroy($id);

                Session::put('message_success','Cập nhật thành công');
                return redirect('admin/slider/list');

            }else{

                Session::put('message_error','Danh mục vừa xóa có danh mục con. Cập nhật không thành công');
                return redirect('admin/slider/list');

            }

        }
//      Category_product::destroy($id);

//        Session::put('message_success','Cập nhật thành công');
//        return redirect('category/list');


    }
    public function getLogout(){

        Session::put('admin_name','');
        Session::put('admin_id','');
        return redirect('admin');



    }

    public function postDashboard(Request $request){
            $email=$request->Email;
            $password=$request->Password;
            $result=DB::table('tbl_admin')->where('admin_email',$email)->where('admin_password',md5($password))->first();

            if($result!=null){
    Session::put('admin_name',$result->admin_name);
    Session::put('admin_id',$result->id_admin);
    return redirect('admin/dashboard');
            }else{
                Session::put('message',"Lỗi tài khoản hoặc mật khẩu chưa đúng");
                return redirect('admin');
            }



    }

}
