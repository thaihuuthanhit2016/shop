<?php
namespace App\Http\Controllers;
date_default_timezone_set("Asia/Bangkok");


use App\Mail\TestMail;
use App\Models\Answer_result;
use App\Models\Bill;
use App\Models\Bill_detail;
use App\Models\Brands_product;
use App\Models\Card;
use App\Models\Card_user;
use App\Models\Category_news;
use App\Models\Category_product;
use App\Models\City;
use App\Models\Comment;
use App\Models\Config;
use App\Models\Customer;
use App\Models\Customer_shop;
use App\Models\Depot;
use App\Models\Email_deposit_promotion_information;
use App\Models\Feeback;
use App\Models\Loto;
use App\Models\Multiple_choice;
use App\Models\News;
use App\Models\Offer;
use App\Models\Offer_customer;
use App\Models\Pages;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Product_compare;
use App\Models\Product_customer_viewed;
use App\Models\Product_images;
use App\Models\Product_shop;
use App\Models\Product_specifications;
use App\Models\Rating;
use App\Models\Shop;
use App\Models\Social;
use App\Models\User;
use App\Models\User_chat;
use App\Models\Wallet;
use App\Models\Wallet_user;
use App\Models\Wishlist;
use App\Services\SocialFacebookAccountService;
use BaconQrCode\Encoder\QrCode;
use Cart;
use Cassandra\Custom;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use Mail;
use DB;
use phpDocumentor\Reflection\Element;
use PhpOffice\PhpSpreadsheet\Calculation\Category;
use function Matrix\cofactors;


class HomeController extends Controller
{



    public function postContact(Request $request){
        $feeback=new  Feeback();
        $feeback->fullname=$request->name;
        $feeback->phone=$request->phone;
        $feeback->email=$request->email;
        $feeback->subject=$request->subject;
        $feeback->content=$request->message;
        $feeback->save();
        Session::put('message_feeback',"Cám ơn bạn đã phản hồi ý kiến! Chúng tôi sẽ cải thiện theo ý kiến của bạn đã góp ý");
        return back();
    }
    public function getContact(){
        $config=DB::table('tbl_config')->first();
        return view('pages.contact',compact('config'));
    }
    public function shop($id){
        $shop=Product_shop::where('id_shop',$id)->get();
        $shop_user=Shop::where('id_shop_user',$id)->first();
        $category=Category_product::where('category_status',1)->orderby('category_name','ASC')->get();
        $brands=Brands_product::where('brands_status',1)->orderby('brands_name','ASC')->get();
        return view('pages.shop',compact('category','brands','shop','shop_user'));
    }
    public function compare_customer(){
        $product_compare=Product_compare::where('id_customer',Session::get('customer_id'))->orderby("id_product_compare","DESC")->get();
        return view('pages.compare',compact('product_compare'));
    }
    public function compare($id){
        if(!Session::get('customer_id')){
            return redirect('login');
        }
        $check_product=Product_compare::where('id_product',$id)->first();
        if($check_product==null) {
            $product_compare = new Product_compare();
            $product_compare->id_product = $id;
            $product_compare->id_customer = Session::get('customer_id');
            $product_compare->created = date('d-m-Y');
            $product_compare->save();

            return redirect('compare');
        }else{

            return back();
        }
    }
    public function loadpricebig(Request $request){
        $product=Product_specifications::where('id_product',$request->id_product)->first();
        $html='';
        $html.='<div class="small">';
        $check_product=DB::table('tbl_product_specifications')->where('id_product',$product->id_product)->first();
                            if ($check_product==null){
                                $html.=' <span id="price_m"style="color:#FE980F;">';$html.=number_format($check_product->price_big,0,',','.');$html.=' VNĐ</span>';
                            }else{
                                if($check_product->price_big_km!=0){
                                    $html.='<span id="price_m" style="font-size: 16px;    text-decoration: line-through;color: #ccc">';
                                    $html.=number_format($check_product->price_big,0,',','.');
                                    $html.='VNĐ </span>';
                                    $html.='<div class="clearfix"></div>';
                                    $html.='<span id="price_km_m" style="color:#FE980F;">';$html.=number_format($check_product->price_big_km,0,',','.');$html.=' VNĐ   <sup style="color:red;font-size: 15px">-';
                                    $html.=100-ceil($check_product->price_big_km*100/$check_product->price_big).'%';
                                    $html.='</sup></span>';
                                    Session::put('price',$check_product->price_big_km);

                                } else{
                                    $html.='<span id="price_m"style="color:#FE980F;">';$html.=number_format($check_product->price_big,0,',','.');
                                    $html.='VNĐ</span>';
                                    Session::put('price',$check_product->price_big);
                                }
                            }
                            $html.='</div>';
                            return $html;
    }
    public function loadpricesmall(Request $request){
    $product=Product_specifications::where('id_product',$request->id_product)->first();
    $html='';
    $html.='<div class="small">';
    $check_product=DB::table('tbl_product_specifications')->where('id_product',$product->id_product)->first();
    if ($check_product==null){
       $html.=' <span id="price_m"style="color:#FE980F;">';$html.=number_format($check_product->price_small,0,',','.');$html.=' VNĐ</span>';
    }else{
        if($check_product->price_small_km!=0){
            $html.='<span id="price_m" style="font-size: 16px;    text-decoration: line-through;color: #ccc">';
            $html.=number_format($check_product->price_small,0,',','.');
            $html.='VNĐ </span>';
            $html.='<div class="clearfix"></div>';
            $html.='<span id="price_km_m" style="color:#FE980F;">';$html.=number_format($check_product->price_small_km,0,',','.');$html.=' VNĐ   <sup style="color:red;font-size: 15px">-';
            $html.=100-ceil($check_product->price_small_km*100/$check_product->price_small).'%';
            $html.='</sup></span>';    Session::put('price',$check_product->price_small_km);

        } else{
            $html.='<span id="price_m"style="color:#FE980F;">';$html.=number_format($check_product->price_small,0,',','.');
            $html.='VNĐ</span>';
            Session::put('price',$check_product->price_small);

        }
    }

    $html.='</div>';
    return $html;
}
    public function index(){

        $category=Category_product::where('category_status',1)->where('category_parent',0)->orderby('category_name','ASC')->get();
        $category_cash=Category_product::where('category_status',1)->where('id_category_product',9)->orderby('category_name','ASC')->first();
        $category_candy=Category_product::where('category_status',1)->where('id_category_product',1)->orderby('category_name','ASC')->first();
        $category_laptop=Category_product::where('category_status',1)->where('id_category_product',7)->orderby('category_name','ASC')->first();
        $brands=Brands_product::where('brands_status',1)->orderby('brands_name','ASC')->get();
        $product_new_cash=Product::where('product_status',1)->where('category_id',9)->orderby('product_name','ASC')->paginate();
        $product_new_candy=Product::where('product_status',1)->where('category_id',1)->orderby('product_name','ASC')->paginate();
        $product_new_laptop=Product::where('product_status',1)->where('category_id',7)->orderby('product_name','ASC')->paginate();
        $product_new=Product::where('product_status',1)->orderby('id_product','DESC')->paginate(12);

        $config=Config::first();
        return view('pages.home',compact('category','config','category_laptop','product_new','product_new_laptop','category_cash','category_candy','brands','product_new_candy',"product_new_cash"));




    }
    public function index_(){

        $category=Category_product::where('category_status',1)->orderby('category_name','ASC')->get();
        $category_cash=Category_product::where('category_status',1)->where('id_category_product',4)->orderby('category_name','ASC')->first();
        $category_candy=Category_product::where('category_status',1)->where('id_category_product',1)->orderby('category_name','ASC')->first();
        $brands=Brands_product::where('brands_status',1)->orderby('brands_name','ASC')->get();
        $product_new_cash=Product::where('product_status',1)->where('category_id',4)->orderby('product_name','ASC')->paginate();
        $product_new_candy=Product::where('product_status',1)->where('category_id',1)->orderby('product_name','ASC')->paginate();

       return view('pages.home',compact('category','category_cash','category_candy','brands','product_new_candy',"product_new_cash"));



    }

    public function category($id){

        $category=Category_product::where('category_status',1)->where('category_parent',0)->orderby('category_name','ASC')->get();
        $category_id=Category_product::where('category_name_slug',$id)->first();

        $brands=Brands_product::where('brands_status',1)->get();


        if(isset($_GET['sort_by'])){
            $sort_by=$_GET['sort_by'];
            if($sort_by=='giamdan'){
                $category_name=Category_product::where('category_parent',$category_id->id_category_product)->first();

                $category_product=Product::where('category_id',$category_name->id_category_product)->where('product_status',1)->orderby('product_price','DESC')->paginate(6);
            }elseif($sort_by=='tangdan'){
                $category_name=Category_product::where('category_parent',$category_id->id_category_product)->first();

                $category_product=Product::where('category_id',$category_name->id_category_product)->where('product_status',1)->orderby('product_price','ASC')->paginate(6);

            }elseif($sort_by=='kytu_az'){
                $category_name=Category_product::where('category_parent',$category_id->id_category_product)->first();

                $category_product=Product::where('category_id',$category_name->id_category_product)->where('product_status',1)->orderby('product_name','ASC')->paginate(6);

            }elseif($sort_by=='kytu_za'){
                $category_name=Category_product::where('category_parent',$category_id->id_category_product)->first();

                $category_product=Product::where('category_id',$category_name->id_category_product)->where('product_status',1)->orderby('product_name','DESC')->paginate(6);

            }
        }elseif(isset($_GET['min'])&&isset($_GET['max'])){
            $category_name=Category_product::where('category_parent',$category_id->id_category_product)->first();

            $category_product=Product::where('category_id',$category_name->id_category_product)->where('product_status',1)->whereBetween('product_price',[$_GET['min'],$_GET['max']])->orderby('product_name','DESC')->paginate(6);

        }
        else{
            $category_name=Category_product::where('category_parent',$category_id->id_category_product)->first();
            $category_product=Product::where('category_id',$category_name->id_category_product)->where('product_status',1)->orderby('id_product','DESC')->paginate(6);

        }

$price_max=Product::orderby('product_price',"DESC")->first();
       return view('pages.category',compact('category','price_max','brands',"category_product",'category_id'));



    }
    public function changepass(Request $request){
        $account=Customer::where('id_customer',Session::get('customer_id'))->first();
       return view('pages.changepass',compact('account'));



    }

    public function brands($id){

        $category=Category_product::where('category_status',1)->where('category_parent',0)->orderby('category_name','ASC')->get();

        $brands=Brands_product::where('brands_status',1)->get();
        $brands_id=Brands_product::where('brands_name_slug',$id)->first();
        $brands_product=Product::where('brands_id',$brands_id->id_brands_product)->where('product_status',1)->orderby('id_product','DESC')->paginate();
       return view('pages.brands',compact('category','brands',"brands_product",'brands_id'));



    }
    public function AddToCart($id){
        if(Session::get('customer_id')==''){
            return redirect('login');
        }
        $product = Product::findOrFail($id);


        if($product->product_price_km==0){
            $price=$product->product_price;
        }else{
            $price=$product->product_price_km;
        }
            $cart = array();
            $cart["id"] = $product->id_product;
            $cart["name"]= $product->product_name;
            $cart["qty"] = 1;
            $cart["price"]= $price;
            $cart["weight"]= $product->product_price;
            $cart['options']["image"]= $product->product_images;
            Cart::add($cart);



        return redirect('cart');



    }
    public function PostAddToCart(Request $request){
        if(Session::get('customer_id')==''){
            return redirect('login');
        }
        $id=$request->product_id;
        $qty=$request->qty;

        $product = Product::findOrFail($id);
        if($product->product_specifications=="GOI"){

                $price =Session::get('price');
                Session::put('total',$price*$qty);
        }else {


            if ($product->product_price_km == 0) {
                $price = $product->product_price;
            } else {
                $price = $product->product_price_km;
            }}
            $cart = array();
            $cart["id"] = $product->id_product;
            $cart["name"] = $product->product_name;
            $cart["qty"] = $qty;
            $cart["price"] = $price;
            $cart["weight"] = $product->product_price;
            $cart['options']["image"] = $product->product_images;
            Cart::add($cart);

        Session::put('total',$price*$qty);


        return redirect('cart');



    }
    public function doOffer(Request $request)
    {
        if(Session::get('customer_id')==''){
            return redirect('login');
        }
        $check_offer=Offer::where('id_offer',$request->id_offer)->first();

        $day=strtotime(date('d-m-Y'));
        $from_day=strtotime($check_offer->from_date);
        $end_day=strtotime($check_offer->end_date);
        if($day<=$end_day &&$day>=$from_day) {
            $check_offer_status = Offer_customer::where('id_offer', $check_offer->id_offer)->where('status', 1)->first();
            if ($check_offer_status != null) {

                Session::put('id_offer', $check_offer->percent);
                Session::put('offer', $check_offer->id_offer);
                return redirect('payment');

            }
        }
    }
    public function ShowCart(Request $request){

        $content=Cart::content();
        $city=City::orderby('matp','ASC')->get();


        $offer=Offer_customer::where('id_customer',Session::get('customer_id'))->get();
        if(count($content)==0){
            return redirect('/');
        }
        if(Session::get('customer_id')==''){
            return redirect('login');
        }



        return view('pages.cart',compact('content','offer','city'));



    }
    public function postchangepass(Request $request){
        if(Session::get('customer_id')==''){
            return redirect('login');
        }

        $c_pass=$request->c_pass;
        $n_pass=$request->n_pass;
        $r_pass=$request->r_pass;
        $id_customer=$request->id_customer;

        $accout=Customer::where('id_customer',$id_customer)->where('customer_password',$c_pass)->first();

        if($accout==null){
            Session::put('message_pass','Mật khẩu hiện tại không đúng');
            return back();
        }
        if($n_pass!=$r_pass){

            Session::put('message_pass_match','Hai mật khẩu không đúng');
            return back();
        }

        $accout_update=Customer::find($accout->id_customer);
        $accout_update->customer_password=$n_pass;
        $accout_update->save();

        Session::put('message_success','Cập nhật thành công');
        return back();




    }
    public function wishlist_user()
    {
        if(Session::get('customer_id')==''){
            return redirect('login');
        }

        $wishlist=Wishlist::where('id_customer',Session::get('customer_id'))->get();
        return view('pages.wishlist',compact('wishlist'));
    }
    public function wallet_user()
    {
        if(Session::get('customer_id')==''){
            return redirect('login');
        }

        $account=Customer::where('id_customer',Session::get('customer_id'))->first();
        $wallet=Wallet_user::where('id_customer',Session::get('customer_id'))->first();
        return view('pages.wallet',compact('wallet','account'));
    }
    public function wishlist($id)
    {
        if(Session::get('customer_id')==''){
            return redirect('login');
        }

        $wishlist=Wishlist::where('id_product',$id)->first();
        if($wishlist==null) {
            $wishlist_insert = new Wishlist();
            $wishlist_insert->id_product = $id;
            $wishlist_insert->id_customer = Session::get("customer_id");
            $wishlist_insert->save();
            Session::put('message_wishlist', 'Thêm vào danh sách yêu thích thành công');
            return back();
        }else{
            Session::put('message_wishlist', 'Sản phẩm này bạn đã thêm vào danh sách yêu thích');
            return back();
        }

    }
    public function order()
    {
        if(Session::get('customer_id')==''){
            return redirect('login');
        }

        $bill=Bill::where('id_customer',Session::get('customer_id'))->get();
        return view('pages.order',compact('bill'));
    }
    public function Chat()
    {
         return view('pages.chat');
    }
    public function UpdateCart(Request $request){
        if(Session::get('customer_id')==''){
            return redirect('login');
        }

       $id=$request->id;
       $qty=$request->quantity;
//       dd($qty);
       Cart::update($id,$qty);

        return redirect('cart');



    }
    public function DelCart($id)
    {        if(Session::get('customer_id')==''){
        return redirect('login');
    }

        Cart::remove($id);

        return redirect('cart');

    }
    public function getLogoutAccount()
    {
        Session::put('customer_id','');
        Session::put('customer_name','');
        Cart::destroy();
        return redirect('/');

    }
    public function LoginCheckout()
    {
        if(Session::get('customer_id')){
            return redirect('checkout');
        }
        $category=Category_product::where('category_status',1)->get();

        $brands=Brands_product::where('brands_status',1)->get();

        return view('pages.login_checkout',compact('brands','category'));

    }
    public function Register(Request $request)
    {
        if($request->pass!=$request->Re_pass){
            Session::put('error',"Hai mật khẩu không trùng khớp");
            return back();
        }
        $check_name=Customer::where('customer_name',$request->name)->first();
        if($check_name!=null){
            Session::put('error',"Tên này đã được sử dụng");
            return back();
        }
        if($request->type==null) {
            $customer = new Customer();
            $customer->customer_name = $request->name;
            $customer->customer_email = $request->name . '@huuthanh.com';
            $customer->customer_password = $request->pass;
            $customer->customer_address = $request->address;
            $customer->customer_phone = $request->phone;
            $customer->verifyKYC = "N";
            $customer->save();

            Session::put('customer_id', $customer->id_customer);
            Session::put('customer_name', $customer->customer_name);

            return redirect('checkout');
        }else{
            $customer = new Shop();
            $customer->username = $request->name;
            $customer->address = $request->name . '@huuthanh.com';
            $customer->password = md5($request->pass);
            $customer->phone = $request->phone;
            $customer->isOnline =0;
            $customer->verifyKYC = "N";
            $customer->save();
            $user_chat=new User_chat();
            $user_chat->unique_id=generateRandomStringSo(6);
            $user_chat->fname=$request->name;
            $user_chat->lname=$request->name;
            $user_chat->email=$request->name . '@huuthanh.com';
            $user_chat->password=md5($request->pass);
            $user_chat->img='user_.jpg';
            $user_chat->status='Active now';
            $user_chat->save();


            Session::put('id_shop_user', $customer->id_shop_user);
            Session::put('customer_name', $customer->username);
            return redirect('admin/shop/info');

        }
    }
    public function viewed_producut()
    {
        if(Session::get('customer_id')==null){
            return redirect('login');

        }
        $product_viewed=Product_customer_viewed::where('id_customer',Session::get('customer_id'))->get();
        $account=Customer::where('id_customer',Session::get('customer_id'))->first();
        return  view('pages.viewed_product',compact('account','product_viewed'));
    }
    public function account()
    {
        if(Session::get('customer_id')==null){
            return redirect('login');

        }
        $account=Customer::where('id_customer',Session::get('customer_id'))->first();
        return  view('pages.account',compact('account'));
    }
    public function search(Request $request)
    {

        $category=Category_product::where('category_status',1)->get();
        $brands=Brands_product::where('brands_status',1)->get();
        $keyword=$request->keyword;
        $product_new=Product::where('product_name','like','%' .$keyword.'%')->where('product_status',1)->orderby('id_product','DESC')->get();
        return  view('pages.search',compact('product_new','category','brands'));
    }
    public function offer()
    {
        if(Session::get('customer_id')==null){
            return redirect('login');

        }


        $account=Customer::where('id_customer',Session::get('customer_id'))->first();
        return  view('pages.offer',compact('account'));
    }
    public function update_profile(Request $request)
    {
        $id_customer=$request->id_customer;
        $update_customer=Customer::find($id_customer);
        $update_customer->customer_phone=$request->phone;
        $update_customer->customer_name=$request->name;

        if($request->hasFile('images1')){
            $file=$request->product_images->getClientOriginalName();
            $rand=generateRandomString(20);
            $day=date('Y-m-d');
            $file_custom=$day.'_'.$rand.'_'.$file;
            $update_customer->images1=$file_custom;
            $request->images1->move('upload/customer/',$file_custom);
        }
        if($request->hasFile('images2')){
            $file=$request->product_images->getClientOriginalName();
            $rand=generateRandomString(20);
            $day=date('Y-m-d');
            $file_custom=$day.'_'.$rand.'_'.$file;
            $update_customer->images2=$file_custom;
            $request->images2->move('upload/customer/',$file_custom);
        }
        $update_customer->verifyKYC="P";


        $update_customer->save();
        Session::put('error_profile',"Cập nhật thành công");
        return back();

    }
    public function Login(Request $request)
    {
        $check_name=Customer::where('customer_email',$request->email)->where('customer_password',$request->pass)->first();
        if($check_name==null){
            Session::put('error_login',"Tài khoản hoặc mật khẩu không đúng");
            return back();
        }

        Session::put('customer_id',$check_name->id_customer);
        Session::put('customer_name',$check_name->customer_name);
        return redirect('account');

    }
    public function postUpdate_wallet(Request $request)
    {
        $wallet=Wallet_user::where('id_customer',$request->id_customer)->first();
        if($wallet!=null){
            $wallet=Wallet_user::find($wallet->id_wallet_user);
            $wallet->wallet_address=$request->address_wallet;
            $wallet->wallet_address_bitcoinsjc=$request->coin;
            $wallet->save();


            Session::put('messeage_wallet','Cập nhật thành công');
            return back();

        }
    }
    public function Checkout()
    {
        $content=Cart::content();
$check_account=Customer::where('id_customer',Session::get('customer_id'))->first();
        if(Session::get('customer_id')==''){
            return redirect('login');
        }
        if($check_account->verifyKYC!='Y'){
            return redirect('account');

        }
        else{
            if (count($content)==0){
                return redirect('/');
            }
            return view('pages.checkout',compact('check_account','content'));
        }
    }

//    public function postCheckout(Request $request){
//
//
//        return redirect('payment');
//    }
    public function GetBill($id){
        $user=Bill::where('code_bill',$id)->first();
        if($user!=null) {

            return response()->json([

                'wallet'=>$user->total
            ]);

        }else{
            return response()->json([
                'data'=>'false',

            ]);

        }


    }

    public function orderSuccess()
    {
        if(Session::get('customer_id')==''){
            return redirect('login');
        }

        Cart::destroy();
        $bill=Bill::orderby('id_bill',"DESC")->first();

        $payment=Payment::where('id_bill',$bill->id_bill)->first();
        if($payment->payment_method==3){
            $addressvi = 'lGAJDaThG28Y7UtGTU7jJ1apRLczA2PfdUUSw1';

            return redirect('http://localhost/coindemo_1/payment/'.$bill->total.'/'.$bill->code_bill.'/'.$addressvi);
        }else{
            return view('pages.order_success',compact('bill'));
        }



    }
    public function orderSuccessed()
    {

        Cart::destroy();
        $bill=Bill::where('code_bill',Session::get('code_bill'))->first();


        return view('pages.order_successed',compact('bill'));
    }
    public function orderSuccess1($id)
    {
        if(Session::get('customer_id')==''){
            return redirect('login');
        }

        Cart::destroy();
        $bill=Bill::where('code_bill',$id)->first();

        $payment=Payment::where('id_bill',$bill->id_bill)->first();
        if($payment->payment_method==3){
            return redirect('http://localhost/coindemo_1/payment/'.$bill->total.'/'.$bill->code_bill);
        }


        return view('pages.order_success',compact('bill'));
    }
    public function delCarrt($id)
    {
        Cart::remove($id);
        return back();

    }
    public function loadcomment(Request $request){
        $news_id=$request->id_news;
        $comment=Comment::where('comment_news_id',$news_id)->where('comment_status',1)->where('id_comment_parent',0)->get();

        $comment_html='';
$i=0;
        foreach ($comment as $c) {
            $i++;
            $comment_html.='<div class="col-md-2" style="margin-bottom: 10px">
<input type="hidden" value="';$comment_html.=$c->id_comments;$comment_html.='" id="id_comments';$comment_html.=$i;$comment_html.='">
<input type="hidden" value="';$comment_html.=$c->comment_news_id;$comment_html.='" id="id_news';$comment_html.=$i;$comment_html.='">
                <img src="http://localhost/shopbitcoin/upload/avatar/user.jpg" width="100%" class="img img-thumbnail img-responsive">
            </div>
            <div class="col-md-10"style="margin-bottom: 10px;text-align: justify">
                <p style="color:blue;">@';$comment_html.=$c->comment_name;$comment_html.='</p>
                <p style="color:black;">';$comment_html.=$c->comment_date;$comment_html.='</p>
            <p>';$comment_html.=$c->comment;$comment_html.='</p>
            </div>

            <div class="clearfix"></div>';
            $comment_re=Comment::where('comment_status',1)->where('id_comment_parent',$c->id_comments)->get();

            foreach ($comment_re as $c_r){
            $comment_html.='
<div class="col-md-1"></div>
            <div class="col-md-2" style="padding-left:  20px">
<input type="hidden" value="';$comment_html.=$c_r->id_comments;$comment_html.='" id="id_comments';$comment_html.=$i;$comment_html.='">
<input type="hidden" value="';$comment_html.=$c_r->comment_news_id;$comment_html.='" id="id_news';$comment_html.=$i;$comment_html.='">
                <img src="http://localhost/shopbitcoin/upload/avatar/user.jpg" width="100%" class="img img-thumbnail img-responsive">
            </div>
            <div class="col-md-9"style="margin-bottom: 10px;text-align: justify">
                <p style="color:blue;">@';$comment_html.=$c_r->comment_name;$comment_html.='</p>
                <p style="color:black;">';$comment_html.=$c_r->comment_date;$comment_html.='</p>
            <p>';$comment_html.=$c_r->comment;$comment_html.='</p>
            </div>
                        <div class="clearfix"></div>
                        <br>'
                ;
                        }
$comment_html.='
            <p id="repley';$comment_html.=$i;$comment_html.='" style="    cursor: pointer;">Trả lời</p>
            <div id="show_comment';$comment_html.=$i;$comment_html.='"></div>
        <div class="col-md-12 none" id="repley_comment';$comment_html.=$i;$comment_html.='">
        <form action="#">
										<span>
											<input type="text"  class="form-control name';$comment_html.=$i;$comment_html.='" placeholder="Your Name"/><br>
										</span>
            <textarea name="" class="form-control id_message';$comment_html.=$i;$comment_html.='"  placeholder="message" ></textarea><br>
            <button type="button" class="btn btn-primary send_comment';$comment_html.=$i;$comment_html.='" >
                Submit
            </button>
        </form></div>

            <hr style="background: #FE980F;width: 100%;height: 1px;">



';
        }
        $comment_html.='
<script>
    $(document).ready(function (){
        ';

        for ($i=1;$i<10;$i++){
        $comment_html.='
        $("#repley';$comment_html.=$i;$comment_html.='").click(function (){

            $("#repley_comment';$comment_html.=$i;$comment_html.='").removeClass("none");
            });
                $(".send_comment';$comment_html.=$i;$comment_html.='").click(function (){

            var name1=$(".name';$comment_html.=$i;$comment_html.='").val();

            var id_comments1=$("#id_comments';$comment_html.=$i;$comment_html.='").val();
            var id_news1=$("#id_news';$comment_html.=$i;$comment_html.='").val();

            var id_message1=$(".id_message';$comment_html.=$i;$comment_html.='").val();
             $.ajax({
               url:"http://localhost/shopbitcoin/repleycomment",
               method:"get",
               data:{id_comments:id_comments1,id_news:id_news1,name:name1,id_message:id_message1},
               success:function (data){
                   $("#show_comment';$comment_html.=$i;$comment_html.='").html("Bình luận thành công");
                               $("#repley_comment';$comment_html.=$i;$comment_html.='").addClass("none");

               }
    });
        })

                ';
        }$comment_html.='
    });

</script>
';
        return $comment_html;
    }
    public function addcomment(Request $request){
        $name=$request->name;
        $id_message=$request->id_message;
        $id_news=$request->id_news;
        $comment=new Comment();
        $comment->comment=$id_message;
        $comment->comment_name=$name;
        $comment->comment_date=date('d-m-Y');
        $comment->comment_news_id=$id_news;
        $comment->id_comment_parent=0;
        $comment->comment_status=0;

        $comment->save();



    }
    public function repleycomment(Request $request){


        $name=$request->name;
        $id_message=$request->id_message;
        $id_news=$request->id_news;
        $id_comments=$request->id_comments;
        $comment=new Comment();
        $comment->comment=$id_message;
        $comment->comment_name=$name;
        $comment->comment_date=date('d-m-Y');
        $comment->comment_news_id=$id_news;
        $comment->comment_status=0;
        $comment->id_comment_parent=$id_comments;
        $comment->save();

    }
    public function detailnews($id){
        $detail_new=News::where('product_name_slug',$id)->first();

        $product_goiy=News::where('category_id',$detail_new->category_id)->where('id_news',$detail_new->id_news)->orderby('id_news',"DESC")->get();
        return view('pages.detail_news',compact('detail_new','product_goiy'));
    }
    public function category_news($id){
        $category_news=Category_news::where('category_name_slug',$id)->first();
        $category_product=News::where('category_id',$category_news->id_category_news)->paginate(1);
        return view('pages.category_news',compact('category_news','category_product'));
    }
    public function news(){
        $product_new_cash=News::orderby('id_news',"DESC")->limit(8)->get();

        return view('pages.news',compact('product_new_cash'));
    }
    public function game(){

        $category=Category_product::where('category_status',1)->get();

        $brands=Brands_product::where('brands_status',1)->get();
        $config=DB::table('tbl_config')->first();

        if(date('d-m-Y')==$config->date_quaythuong && $config->so_lan==1) {
            $customer = Customer::orderByRaw("RAND()")->limit(2)->get();

            $today = date('Y-m-d');
// Cộng thêm 1 tuần
            $week = strtotime(date("d-m-Y", strtotime($today)) . " +5 day");
            $week = strftime("%d-%m-%Y", $week);
            $config=Config::find(1);
            $config->date_quaythuong=$week;
            $config->so_lan=1;
            $config->save();

        }else{
            $customer=array();
        }

        return view('pages.game',compact('category','config','brands','customer'));

    }
    public function test(){


        $category=Category_product::where('category_status',1)->get();

        $brands=Brands_product::where('brands_status',1)->get();

        return view('pages.test',compact('category','brands'));
    }
    public function testdata()     {
        $data12=DB::table('tbl_test')->get();

        foreach ($data12 as $d){
            $data[] = array(
                'title'   => $d->title,
                'start'   => $d->from_date
            );
        }
echo json_encode($data);

}
    public function payment()
    {
        if(Session::get('customer_id')==''){
            return redirect('login');
        }
        if(Session::get('fee')==''){
            Session::put('fee_m','Vui lòng chọn tỉnh thành phố,quận huyên,xã phường');
            return back();

        }
        $content=Cart::content();
        if(count($content)==0){
            return redirect('/');
        }
        $check_account=Customer::where('id_customer',Session::get('customer_id'))->first();
        $offer=Offer_customer::where('id_customer',Session::get('customer_id'))->where('status',1)->get();

        return view('pages.payment',compact('content','offer','check_account'));
    }
    public function postpayment(Request $request)
    {

        if($request->check_option==3){
            $total=Session::get('total_percent');

            $total_coin=(($total*800)/20000);
            Session::put('total_percent',$total_coin);

        }
        if(Session::get('customer_id')==''){
            return redirect('login');
        }

        if(Session::get('offer')!=''){
            $customer=Offer_customer::where('id_offer',Session::get('offer'))->first();
            $customer=Offer_customer::find($customer->id_offer_customer);
            $customer->status=0;
            $customer->save();
        }
        $bill=new Bill();
        $bill->id_customer=Session::get('customer_id');
        $bill->code_bill=generateRandomString(6);
        $bill->total=Session::get('total_percent');
        $bill->created=date('Y-m-d');
        $bill->note=$request->note;
        $bill->status=0;

        $bill->save();


        $content = Cart::content();
        foreach ($content as $c) {
            $bill_detail=new Bill_detail();
            $bill_detail->id_bill=$bill->id_bill;
            $bill_detail->id_product=$c->id;
            $product_shop=Product_shop::where('id_product',$c->id)->first();
            $bill_detail->id_shop=$product_shop->id_shop;
            $bill_detail->quantity=$c->qty;
            $bill_detail->rate=0;
            $bill_detail->save();
            $check_custom=Customer_shop::where('id_customer',Session::get('customer_id'))->first();
            if($check_custom==null) {
                $customer_shop = new Customer_shop();
                $customer_shop->id_shop = $product_shop->id_shop;

                $customer_shop->id_customer = Session::get('customer_id');
                $customer_shop->save();
            }
            $depot=Depot::where('id_product',$c->id)->first();
            $depot=Depot::find($depot->id_depot);
            if($depot->stock!=0) {
                $depot->stock = $depot->stock - 1;
                $depot->save();

            }




        }
        $payment=new Payment();
        $payment->payment_method	= $request->check_option;
        $payment->id_bill	= $bill->id_bill;
        $payment->payment_status	= 0;
        $payment->id_shop=$product_shop->id_shop;
        $payment->id_shop=$product_shop->id_shop;

        $payment->save();

        if(Session::get('id_offer')!=''){
            $offer_user=Offer_customer::where('id_customer',Session::get('customer_id'))->where('id_offer',Session::get('id_offer'))->first();
            $offer_user_update=Offer_customer::find($offer_user->id_offer_customer);
            $offer_user_update->status=1;
            $offer_user_update->save();

        }


        if($request->check_option==3) {

            $addressvi = 'lGAJDaThG28Y7UtGTU7jJ1apRLczA2PfdUUSw1';

            return redirect('http://localhost/coindemo_1/payment/' . $bill->total . '/' . $bill->code_bill . '/' . $addressvi);
        }else{

            return redirect('order-success');
        }
    }
    public function Paymented(Request $request,$id){


        $bill =Bill::where('code_bill',$id)->first();
        $payment=Payment::where('id_bill',$bill->id_bill)->first();
        $payment_up=Payment::find($payment->id_payment);

        $payment_up->payment_status=1;
        $payment_up->save();
Session::put('code_bill',$id);
        return redirect('order-successed');

    }
    public function PaymentBuyCard(Request $request,$id){

            $card=Card::find($id);
            if ($request->address_wallet_back==null){
                Session::put('err_update', "Your wallet is not authenticated or you have not provided a wallet address");

            }
            if(Session::get('address_url')!=null){

        $card1=new Card_user();
        $card1->id_card=$id;
        $card1->id_shop=0;
        $card1->type=0;
        $card1->amount_vnd=0;
        $card1->amount_coin=0;
        $card1->status=0;
        $card1->code_transfer=generateRandomString(10);
        $card1->address=Session::get('address_url');
        $card1->save();
        $card_data=Card_user::where('id_card_user',$card1->id_card_user)->first();
        return view('pages.paymentcard',compact('card_data','card'));
        }
        else {

                if ($request->address_coin != null){

                    $card1 = new Card_user();
                $card1->id_card = $id;
                $card1->id_shop = 0;
                $card1->type = 0;
                $card1->amount_vnd = 0;
                $card1->amount_coin = 0;

                $card1->address = $request->address_coin;
                    $card1->code_transfer=generateRandomString(10);

                    $card1->status=0;
                    $card1->save();
                    Session::put('err_update', "");

                Session::put('message_update', "Update success");
                    $card_data=Card_user::where('id_card_user',$card1->id_card_user)->first();

                    return view('pages.paymentcard',compact('card','card_data'));

                }else{

                    $card_data=null;
                    return view('pages.paymentcard',compact('card','card_data'));

                }

            }
    }
    public function BuyCard(Request $request,$id){

            Session::put('address_url',$id);
        $category=Category_product::where('category_status',1)->orderby('category_name','ASC')->get();
        $category_cash=Category_product::where('category_status',1)->where('id_category_product',4)->orderby('category_name','ASC')->first();
        $category_candy=Category_product::where('category_status',1)->where('id_category_product',1)->orderby('category_name','ASC')->first();

        $brands=Brands_product::where('brands_status',1)->orderby('brands_name','ASC')->get();
        $product_new_cash=Product::where('product_status',1)->where('category_id',4)->orderby('product_name','ASC')->paginate();
        $product_new_candy=Product::where('product_status',1)->where('category_id',1)->orderby('product_name','ASC')->paginate();
        $card=Card::paginate(10);

        return view('pages.card',compact('category','category_cash','category_candy','brands','product_new_candy',"product_new_cash",'card'));


    }
    public function BuyCard1(Request $request){
        $category=Category_product::where('category_status',1)->orderby('category_name','ASC')->get();
        $category_cash=Category_product::where('category_status',1)->where('id_category_product',4)->orderby('category_name','ASC')->first();
        $category_candy=Category_product::where('category_status',1)->where('id_category_product',1)->orderby('category_name','ASC')->first();

        $brands=Brands_product::where('brands_status',1)->orderby('brands_name','ASC')->get();
        $product_new_cash=Product::where('product_status',1)->where('category_id',4)->orderby('product_name','ASC')->paginate();
        $product_new_candy=Product::where('product_status',1)->where('category_id',1)->orderby('product_name','ASC')->paginate();
        $card=Card::all();

        return view('pages.card',compact('category','category_cash','category_candy','brands','product_new_candy',"product_new_cash",'card'));


    }
    public function rating(Request $request){

        $rating=new Rating();
        $rating->id_product=$request->product_id;
        $rating->start=$request->index;
        $rating->save();
        $bill_detail=Bill_detail::where('id_product',$request->product_id)->first();
        $bill_detail=Bill_detail::find($bill_detail->id_bill_detail);
        $bill_detail->rate=1;
        $bill_detail->save();
        echo 'done';


    }
    function createUser($getInfo,$provider){
        $user = Customer::where('provider_id', $getInfo->id)->first();
        if (!$user) {
            $user = User::create([
                'name'     => $getInfo->name,
                'email'    => $getInfo->email,
                'provider' => $provider,
                'provider_id' => $getInfo->id
            ]);
        }
        return $user;
    }
    public function getPages($id){
        $pages=Pages::where('title_slug',$id)->first();
        return view('pages.pages',compact('pages'));
    }
    public function getLoto(){
        for($i=1;$i<=90;$i++){
        $so=generateRandomStringLoto(2);
        $so1=generateRandomStringLoto(2);
        $so2=generateRandomStringLoto(2);
        $so3=generateRandomStringLoto(2);
        $so4=generateRandomStringLoto(2);
        $so5=generateRandomStringLoto(2);
        $so6=generateRandomStringLoto(2);
        $so7=generateRandomStringLoto(2);
        $so8=generateRandomStringLoto(2);
            $check_so=Loto::where('number',$so)->first();
            if($check_so==null) {
                $loto = new Loto();
                $loto->number = $so1;
                $loto->number2 = $so2;
                $loto->number3 = $so3;
                $loto->number4 = $so4;
                $loto->number5 = $so5;
                $loto->number6 = $so6;
                $loto->number7 = $so7;
                $loto->number8 = $so8;

                $loto->save();

        }
        }
        $loto=Loto::orderByRaw("RAND()")->limit(5)->get();
        return view('pages.loto',compact('loto'));
    }
    public function get404(){
        $category=Category_product::where('category_status',1)->get();

        $brands=Brands_product::where('brands_status',1)->get();

        return view('errors.404',compact('category','brands'));
    }
    public function api_show(Request $request){
        $test_data=Category_product::all();

        dd(json_encode($test_data));
    }
    public function promotion_information(Request $request){
        $email_deposit_promotion_information=new Email_deposit_promotion_information();
        $email_deposit_promotion_information->email=$request->Email;
        $email_deposit_promotion_information->created=date('d-m-Y');
        $email_deposit_promotion_information->save();

        return back();



    }
    public function product(){

        $category=Category_product::where('category_status',1)->where('category_parent',0)->orderby('category_name','ASC')->get();
        $category_cash=Category_product::where('category_status',1)->where('id_category_product',4)->orderby('category_name','ASC')->first();
        $category_candy=Category_product::where('category_status',1)->where('id_category_product',1)->orderby('category_name','ASC')->first();
        $brands=Brands_product::where('brands_status',1)->orderby('brands_name','ASC')->get();
        $product=Product::where('product_status',1)->orderby('product_price',"DESC")->get();

       return view('pages.product',compact('category','category_cash','category_candy','brands','product'));

    }
    public function detail($id){

        $category=Category_product::where('category_status',1)->where('category_parent',0)->orderby('category_name','ASC')->get();

        $brands=Brands_product::where('brands_status',1)->get();
        $product=Product::where('product_name_slug',$id)->first();
        $category_product=Category_product::where('id_category_product',$product->category_id)->first();
        $brands_product=Brands_product::where('id_brands_product',$product->brands_id)->first();
        $category_id=Category_product::where('id_category_product',$product->category_id)->first();
        $brands_id=Brands_product::where('id_brands_product',$product->brands_id)->first();
        $product_img=Product_images::where('id_product',$product->id_product)->get();
        $product_goiy=Product::where('id_product','<>',$product->id_product)->where('category_id',$product->category_id)->get();
        $rating=Rating::where('id_product',$product->id_product)->avg('start');
        $rating=round($rating);
        $so_luot=Rating::where('id_product',$product->id_product)->get();
        if(Session::get('customer_id')!='')
        {

            $check_product=Product_customer_viewed::where('id_product',$product->id_product)->first();
            if($check_product==null){
            $product_customer_viewed=new Product_customer_viewed();
            $product_customer_viewed->id_customer = Session::get('customer_id');
            $product_customer_viewed->id_product = $product->id_product;
            $product_customer_viewed->created=date('d-m-Y');
            $product_customer_viewed->save();


        }
        }
       return view('pages.detail',compact('so_luot','category_product','brands_product','category','rating','category_id','brands_id','product_img','product_goiy','brands',"product"));



    }
    public function multiple_choice(){

        if(!Session::get('customer_id')){
            return redirect('login');
        }
        Session::put('check_qs',"ok");
        $category=Category_product::where('category_status',1)->get();

        $brands=Brands_product::where('brands_status',1)->get();
        return view('pages.multiple_choice',compact('category','brands'));
    }
    public function checkresult(Request $request){
        $cau1=$request->cauhoi_1;
        $cau2=$request->cauhoi_2;
        $cau3=$request->cauhoi_3;
        $cau4=$request->cauhoi_4;
        $cau5=$request->cauhoi_5;
        $cau6=$request->cauhoi_6;
        $cau7=$request->cauhoi_7;
        $cau8=$request->cauhoi_8;
        $cau9=$request->cauhoi_9;
        $cau10=$request->cauhoi_10;

        $da1=$request->group_1;
        $da2=$request->group_2;
        $da3=$request->group_3;
        $da4=$request->group_4;
        $da5=$request->group_5;
        $da6=$request->group_6;
        $da7=$request->group_7;
        $da8=$request->group_8;
        $da9=$request->group_9;
        $da10=$request->group_10;

        $q_a1=Multiple_choice::where('id_multiple_choice',$cau1)->first();

        $q_a2=Multiple_choice::where('id_multiple_choice',$cau2)->first();
        $q_a3=Multiple_choice::where('id_multiple_choice',$cau3)->first();
        $q_a4=Multiple_choice::where('id_multiple_choice',$cau4)->first();
        $q_a5=Multiple_choice::where('id_multiple_choice',$cau5)->first();
        $q_a6=Multiple_choice::where('id_multiple_choice',$cau6)->first();
        $q_a7=Multiple_choice::where('id_multiple_choice',$cau7)->first();
        $q_a8=Multiple_choice::where('id_multiple_choice',$cau8)->first();
        $q_a9=Multiple_choice::where('id_multiple_choice',$cau9)->first();
        $q_a10=Multiple_choice::where('id_multiple_choice',$cau10)->first();
$dapandung=0;

        $answer = new Answer_result();
        $answer->id_customer = Session::get('customer_id');
        $answer->answer = $da1;
        $answer->question = $cau1;
        $answer->created = date('d-m-Y');
        $answer->save();
        if($q_a1->answer==$da1){

            $dapandung=$dapandung+1;
        }

        $answer = new Answer_result();
        $answer->id_customer = Session::get('customer_id');
        $answer->answer = $da2;
        $answer->question = $cau2;
        $answer->created = date('d-m-Y');
        $answer->save();
        if($q_a2->answer==$da2){

            $dapandung=$dapandung+1;
        }

        $answer = new Answer_result();
        $answer->id_customer = Session::get('customer_id');
        $answer->answer = $da3;
        $answer->question = $cau3;
        $answer->created = date('d-m-Y');
        $answer->save();
        if($q_a3->answer==$da3){

            $dapandung+=1;
        }

        $answer = new Answer_result();
        $answer->id_customer = Session::get('customer_id');
        $answer->answer = $da4;
        $answer->question = $cau4;
        $answer->created = date('d-m-Y');
        $answer->save();
        if($q_a4->answer==$da4){

            $dapandung+=1;
        }

        $answer = new Answer_result();
        $answer->id_customer = Session::get('customer_id');
        $answer->answer = $da5;
        $answer->question = $cau5;
        $answer->created = date('d-m-Y');
        $answer->save();
        if($q_a5->answer==$da5){

            $dapandung+=1;
        }

        $answer = new Answer_result();
        $answer->id_customer = Session::get('customer_id');
        $answer->answer = $da6;
        $answer->question = $cau6;
        $answer->created = date('d-m-Y');
        $answer->save();
        if($q_a6->answer==$da6){

            $dapandung+=1;
        }

        $answer = new Answer_result();
        $answer->id_customer = Session::get('customer_id');
        $answer->answer = $da7;
        $answer->question = $cau7;
        $answer->created = date('d-m-Y');
        $answer->save();
        if($q_a7->answer==$da7){

            $dapandung+=1;
        }

        $answer = new Answer_result();
        $answer->id_customer = Session::get('customer_id');
        $answer->answer = $da8;
        $answer->question = $cau8;
        $answer->created = date('d-m-Y');
        $answer->save();
        if($q_a8->answer==$da8){

            $dapandung+=1;
        }

        $answer = new Answer_result();
        $answer->id_customer = Session::get('customer_id');
        $answer->answer = $da9;
        $answer->question = $cau9;
        $answer->created = date('d-m-Y');
        $answer->save();
        if($q_a9->answer==$da9){

            $dapandung+=1;
        }

        $answer = new Answer_result();
        $answer->id_customer = Session::get('customer_id');
        $answer->answer = $da10;
        $answer->question = $cau10;
        $answer->created = date('d-m-Y');
        $answer->save();
        if($q_a10->answer==$da10){

            $dapandung+=1;
        }

        $id=Customer::where('id_customer',Session::get('customer_id'))->first();
        $customer=Customer::find($id->id_customer);
        $customer->total_yes=$dapandung;
        $code=generateRandomStringVoucher(6);
        if($dapandung>7){
Session::put('code','ok');
$offer=new Offer();
$offer->code_offer=$code;
$offer->title="Quay thưởng";
$offer->percent=10;
$offer->content='Quay thưởng';
$offer->number=1;
$offer->from_date=date('d-m-Y');
            $today = date('d-m-Y');

            $week=strtotime(date("d-m-Y", strtotime($today)) . " +1 week");
            $week = strftime("%d-%m-%Y", $week);

$offer->end_date=$week;
$offer->status=1;
$offer->type=3;
$offer->save();

$offer_customer=new Offer_customer();
$offer_customer->id_customer=Session::get('customer_id');
$offer_customer->id_offer=$offer->id_offer;
$offer_customer->status=1;
$offer_customer->save();




        }else{

            $customer->solanquaythuong=$customer->solanquaythuong+0;

        }
        $customer->save();


    echo $dapandung;


    }
    public function view_result(){


        if(!Session::get('customer_id')){
            return redirect('login');
        }if (Session::get('check_qs')==''){
            return redirect('account');
        }


        $category=Category_product::where('category_status',1)->get();

        $brands=Brands_product::where('brands_status',1)->get();


        return view('pages.view_result',compact('category','brands'));

    }
    public function loadquestion(){
        $html='';
        $i=0;
        foreach($questions as $q){
            $i++;
            Session::put('answer_'.$i,$q->answer);
            $html.='<h5 style="font-weight: bold"> Câu ';$html.=$i; $html.=': ';;$html.=$q->question;$html.='</h5>';
                        $html.='<fieldset>
                        <input type="hidden" name="cauhoi_';$html.=$i;$html.='" id="cauhoi_';$html.=$i;$html.='"value="';$html.=$q->id_multiple_choice;$html.='">
                                <div class="radio col-md-12">
                                    <label><input type="radio" value="A" name="group_';$html.=$i;$html.='"  id="group_';$html.=$i;$html.='">

 <span class="text-danger"> A:</span>';$html.= $q->option_a;$html.='

</label>
                                </div>

                                <div class="radio col-md-12">
                                    <label><input type="radio" value="B"name="group_';$html.=$i;$html.='" id="group_';$html.=$i;$html.='"><span class="text-danger"> B:</span>';$html.=$q->option_b;$html.='</label>
                                </div>
                                <div class="radio col-md-12">
                                    <label><input type="radio" value="C" name="group_';$html.=$i;$html.='" id="group_';$html.=$i;$html.='"><span class="text-danger"> C:</span>';$html.=$q->option_c;$html.='</label>
                                </div>
                                <div class="radio col-md-12">
                                    <label><input type="radio" value="D"name="group_';$html.=$i;$html.='" id="group_';$html.=$i;$html.='"><span class="text-danger"> D:</span>';$html.=$q->option_d;$html.='</label>
                                </div>

                         </fieldset>
                                 ';

    }
        $html.='                <center><button type="submit" class="btn btn-warning" id="btnFinish">Kết thúc bài thi</button></center>

                         ';
        return $html;
    }
    public function loadquestionresult(){
        $questions= DB::table("tbl_answer_result")->where('id_customer',Session::get('customer_id'))->orderby('id_answer_result','DESC')->limit(10)->get();
        $html='';
        $id=Customer::where('id_customer',Session::get('customer_id'))->first();
        $code_offer=Offer_customer::where('id_customer',Session::get('customer_id'))->orderby('id_offer_customer','DESC')->first();
        $code=Offer::where('id_offer',$code_offer->id_offer)->first();
if(Session::get('code')!='') {
    $html .= '<div class="row"> <div class="col-md-6"> <h3>Chúc mừng bạn đã đạt <b style="color: #FE980F">';
    $html .= $id->total_yes;
    $html .= ' điểm</b> </h3></div><div class="col-md-6"><a href="http://localhost/shopbitcoin/offer" style="font-size: 16px;font-weight: bold;color:#FE980F;;">Hãy kiểm tra ngay mã voucher của bạn</a></h3><br>
</div></div>';
}else{
    $html .= '<div class="row"> <div class="col-md-12"> <h3>Rất tiếc!! Bạn  đạt <b style="color: #FE980F">';
    $html .= $id->total_yes;
    $html .= ' điểm </b> chưa được nhận mã voucher </h3></div></div>';
}
        $i=0;
        foreach($questions as $qa){
            $q= DB::table("tbl_multiple_choice")->where('id_multiple_choice',$qa->question)->first();

            $i++;

            $html.='
<h5 style="font-weight: bold"> Câu ';$html.=$i; $html.=': ';;$html.=$q->question;$html.='</h5>';
                        $html.='<fieldset>
                        <input type="hidden" name="cauhoi_';$html.=$i;$html.='" id="cauhoi_';$html.=$i;$html.='"value="';$html.=$q->id_multiple_choice;$html.='">
                                <div class="radio col-md-12">
                                    <label><input type="radio"'; if($qa->answer=="A"){ $html.='checked'; } $html.=' value="A" name="group_';$html.=$i;$html.='"  id="group_';$html.=$i;$html.='">

 <span class="text-danger"> A:</span>';$html.= $q->option_a;$html.='

</label>
                                </div>

                                <div class="radio col-md-12">
                                    <label><input type="radio"'; if($qa->answer=="B"){ $html.='checked'; } $html.=' value="B"name="group_';$html.=$i;$html.='" id="group_';$html.=$i;$html.='"><span class="text-danger"> B:</span>';$html.=$q->option_b;$html.='</label>
                                </div>
                                <div class="radio col-md-12">
                                    <label><input type="radio"'; if($qa->answer=="C"){ $html.='checked'; } $html.=' value="C" name="group_';$html.=$i;$html.='" id="group_';$html.=$i;$html.='"><span class="text-danger"> C:</span>';$html.=$q->option_c;$html.='</label>
                                </div>
                                <div class="radio col-md-12">
                                    <label><input type="radio"'; if($qa->answer=="D"){ $html.='checked'; } $html.=' value="D"name="group_';$html.=$i;$html.='" id="group_';$html.=$i;$html.='"><span class="text-danger"> D:</span>';$html.=$q->option_d;$html.='</label>
                                </div>
                                <div class="radio col-md-12">
                                    <label  style="color:#FE980F;"><span> Đáp án :</span>';$html.=$q->answer;$html.='</label>
                                </div>

                         </fieldset>
                                 ';

    }

        return $html;
    }
    public function quickview(Request $request){
            $product_id=$request->product_id;
            $product=Product::find($product_id);

            $gallery=Product_images::where('id_product',$product_id)->get();
            $output['product_gallery']='';
            foreach ($gallery as $g){
                $output['product_gallery'].='<p><img width="100%" src="http://localhost/shopbitcoin/upload/product/gallyery/'.$g->images.'"></p>';

            }
            $output['product_name']=$product->product_name;
            $output['product_id']=$product->code_product;
            $output['product_desc']=$product->product_desc;
            $output['product_quickview_content']=$product->product_content;
            $output['product_price']=number_format($product->product_price,0,',','.');
            $output['product_price_km']=$product->product_price_km;
            $output['product_image']='<p><img width="100%" src="http://localhost/shopbitcoin/upload/product/'.$product->product_images.'"></p>';
            echo json_encode($output);

        }
    public function autocomplet_ajax_search(){
            $product=Product::where('product_status',1)->where('product_name','LIKE','%'.$_GET['query'].'%')->get();
            $html='<ul class="dropdown-menu" style="display: block;position: relative" >';
            foreach ($product as $p){
                $html.='<li><a href="#">'.$p->product_name.'</a> </li>';
            }
            $html.='</ul>';
            echo $html;
        }
    public function autocomplect_tinh(){
            $product=City::where('name_city','LIKE','%'.$_GET['query'].'%')->get();
            $html='<ul class="dropdown-menu"    style="display: block;position: relative" >';
            foreach ($product as $p){
                $html.='<li class="form-control"><a href="#">'.$p->name_city.'</a> </li>';
            }
            $html.='</ul>';
            echo $html;
        }
    public function detail_tags($id){


        $category=Category_product::where('category_status',1)->get();

        $brands=Brands_product::where('brands_status',1)->get();
         $product_tags=Product::where('product_status',1)->where('product_name','LIKE','%'.$id.'%')->get();
       return view('pages.tags',compact('product_tags','id','category','brands'));



    }
}
