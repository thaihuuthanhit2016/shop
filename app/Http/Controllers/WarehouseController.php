<?php
namespace App\Http\Controllers;
date_default_timezone_set("Asia/Bangkok");


use App\Models\Bill_detail;
use App\Models\Category_news;
use App\Models\Category_product;
use App\Models\Category_product_shop;
use App\Models\Shop;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use DB;
use Session;
class WarehouseController extends Controller
{



    public function getAddCategory(){
        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }


       return view('admin.warehouse.addCategory');



    }
    public function getListCategory(){
        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }
        $warehouse=Bill_detail::orderby('id_bill_detail',"DESC")->paginate(10);

       return view('admin.warehouse.listCategory',compact('warehouse'));



    }
    public function postAddCategory(Request $request){
        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $dheck=Warehouse::where('name_warehouse',$request->category_product_name)->first();
        if($dheck==null){

            $category_product=new Warehouse();

            $category_product->name_warehouse=$request->category_product_name;
            $category_product->address=($request->address);
            $category_product->phone=$request->phone;
            $category_product->description=$request->description;
            $category_product->status = 0;

            $category_product->save();


            Session::put('message','Cập nhật thành công');
            return redirect('admin/warehouse/list');

        }else{
            Session::put('message_error','Tên danh mục đã tồn tại !Cập nhật không thành công');
            Session::put('name_category',$request->category_product_name);
            Session::put('category_desc',$request->category_product_desc);
            Session::put('parent',$request->parent);

            return back();
        }
    }
}
