<?php
namespace App\Http\Controllers;
date_default_timezone_set("Asia/Bangkok");



use App\Models\Brands_product;
use App\Models\Brands_product_shop;
use App\Models\Category_product;
use App\Models\Message;
use Illuminate\Http\Request;
use DB;
use Session;
class ChatController extends Controller
{


public function showmessage(Request $request){
 $id=$request->id;
 $id_login=$request->id_login;

    $data=DB::table('messages')
        ->where('incoming_msg_id',$id_login)
        ->where('outgoing_msg_id',$id)
        ->orwhere('outgoing_msg_id',$id_login)
        ->where('incoming_msg_id',$id)
        ->get();

    $html='';

    foreach($data as $d){
         $name_shop=DB::table('tbl_shop')->where('unique_id',$d->outgoing_msg_id)->first();

                   if($d->incoming_msg_id!=$id_login){
         $html.='           <div class="direct-chat-msg">
                        <div class="direct-chat-info clearfix">
                            <span class="direct-chat-name pull-left">';$html.=$name_shop->name_shop;$html.='</span>
                            <span class="direct-chat-timestamp pull-right">';$html.=$d->created_at;$html.='</span>
                        </div>
                        <!-- /.direct-chat-info -->
                        <img class="direct-chat-img" src="https://bootdey.com/img/Content/user_1.jpg" alt="Message User Image"><!-- /.direct-chat-img -->
                        <div class="direct-chat-text">';$html.=
                            $d->msg;$html.='
                        </div>
                        <!-- /.direct-chat-text -->
                    </div>';
                        }else{
                            $html.='<!-- /.direct-chat-msg -->

                    <!-- Message to the right -->
                    <div class="direct-chat-msg right">
                        <div class="direct-chat-info clearfix">
                          <span class="direct-chat-name pull-left">';$html.=$name_shop->name_shop;$html.='</span>
                            <span class="direct-chat-timestamp pull-right">';$html.=$d->created_at;$html.='</span>
                        </div>
                        <!-- /.direct-chat-info -->
                        <img class="direct-chat-img" src="https://bootdey.com/img/Content/user_2.jpg" alt="Message User Image"><!-- /.direct-chat-img -->
                        <div class="direct-chat-text">';$html.=
                        $d->msg;$html.='
                        </div>
                        <!-- /.direct-chat-text -->
                    </div>';
                 }
                }
    echo $html;
}
    public function chat($id){

        $up_=Message::where('outgoing_msg_id',$id)->where('incoming_msg_id','<>',Session::get('unique_id'))->first();
        if($up_!=null){

            $update=Message::find($up_->msg_id);
            $update->viewed=1;
            $update->save();

        }
        $data=DB::table('messages')
            ->where('incoming_msg_id',Session::get('unique_id'))
            ->where('outgoing_msg_id',$id)
            ->orwhere('outgoing_msg_id',Session::get('unique_id'))
            ->where('incoming_msg_id',$id)
            ->get();


        return view('chat.index',compact('data','id'));

    }
    public function postchat($id,Request $request){

        $msg=new Message();
        $msg->incoming_msg_id=Session::get('unique_id');
        $msg->outgoing_msg_id=$id;
        $msg->msg=$request->message;
        $msg->viewed=0;
        $msg->save();



        return back();

    }


    public function listchat(){

        $data=DB::table('tbl_shop')->where('unique_id','<>',Session::get('unique_id'))->get();


        return view('chat.list',compact('data'));

    }

}
