<?php
namespace App\Http\Controllers;

date_default_timezone_set("Asia/Bangkok");


use App\Exports\ExcelExports;
use App\Imports\Imports;
use App\Models\Brands_product;
use App\Models\Brands_product_shop;
use App\Models\Category_news;
use App\Models\Category_product;
use App\Models\Category_product_shop;
use App\Models\News;
use App\Models\Product;
use App\Models\Product_images;
use App\Models\Product_shop;
use App\Models\Shop;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Excel;
use Session;
class NewsController extends Controller
{



    public function getAddCategory(){
        if(!Session::get('admin_id')&&!Session::get('username')){
            return redirect('admin');

        }

        $data=Category_news::get();
        $brands=Brands_product_shop::where('id_shop',Session::get('id_shop_user'))->get();
       return view('admin.news.addCategory',compact('data','brands'));



    }
    public function getEditCategory($id){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $data=Category_news::all();
        $brands=Brands_product::all();
        $product=News::find($id);

       return view('admin.news.editCategory',compact('data','product','brands'));



    }

    public function getListCategory(){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }


        $data=News::orderby('id_news','DESC')->paginate(10);
       return view('admin.news.listCategory',compact('data'));



    }
    public function postAddCategory(Request $request){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }


        $dheck=News::where('product_name',$request->product_name)->first();
        if($dheck==null){
            $category_product=new News();

            $category_product->product_name=$request->product_name;
            $category_product->product_name_slug=slugify($request->product_name);
            $category_product->product_desc=$request->product_desc;
            $category_product->product_content=$request->product_content;
            $category_product->category_id=$request->category_id;
            $category_product->created_at=date('d-m-Y H:i');


            if($request->hasFile('product_images')){
                $file=$request->product_images->getClientOriginalName();
                $rand=generateRandomString(20);
                $day=date('Y-m-d');
                $file_custom=$day.'_'.$rand.'_'.$file;
                $category_product->product_images=$file_custom;
                $request->product_images->move('upload/tintuc/',$file_custom);
            }
                $category_product->product_status = 1;

            $category_product->save();



            Session::put('message','Cập nhật thành công');
            return redirect('admin/news/list');

        }else{
            Session::put('message_error','Tên sản phẩm đã tồn tại !Cập nhật không thành công');
            Session::put('product_name',$request->product_name);
            Session::put('product_desc',$request->product_desc);
            Session::put('product_content',$request->product_content);

            return back();
        }
    }
    public function postEditCategory(Request $request,$id){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){

            return redirect('admin');

        }


            $category_product= News::find($id);

        $category_product->product_name=$request->product_name;
        $category_product->product_name_slug=slugify($request->product_name);
        $category_product->product_desc=$request->product_desc;
        $category_product->product_content=$request->product_content;
        $category_product->category_id=$request->category_id;
        $category_product->created_at=date('d-m-Y H:i');


        if($request->hasFile('product_images')){
            $file=$request->product_images->getClientOriginalName();
            $rand=generateRandomString(20);
            $day=date('Y-m-d');
            $file_custom=$day.'_'.$rand.'_'.$file;
            $category_product->product_images=$file_custom;
            $request->product_images->move('upload/tintuc/',$file_custom);
        }
        $category_product->product_status = 1;

        $category_product->save();

            Session::put('message','Cập nhật thành công');
            return redirect('admin/news/list');



    }

    public function getDelCategory($id){
        if(!Session::get('admin_id')&& !Session::get('id_shop_user')){
            return redirect('admin');

        }

        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();
        if($checkshop->verifyKYC!='Y'){
            Session::put('message_error_kyc','Shop chưa được active!');
            return  back();
        }

        $check=Category_product::where('id_category_product',$id)->first();
        if($check!=null){
            $check=Category_product::where('category_parent',$check->id_category_product)->get();
            if(count($check)==0){

              Category_product::destroy($id);

                Session::put('message_success','Cập nhật thành công');
                return redirect('admin/product/list');

            }else{

                Session::put('message_error','Danh mục vừa xóa có danh mục con. Cập nhật không thành công');
                return redirect('admin/product/list');

            }

        }
//      Category_product::destroy($id);

//        Session::put('message_success','Cập nhật thành công');
//        return redirect('category/list');


    }
    public function getLogout(){

        Session::put('admin_name','');
        Session::put('admin_id','');
        return redirect('admin');



    }

    public function postDashboard(Request $request){
            $email=$request->Email;
            $password=$request->Password;
            $result=DB::table('tbl_admin')->where('admin_email',$email)->where('admin_password',md5($password))->first();

            if($result!=null){
    Session::put('admin_name',$result->admin_name);
    Session::put('admin_id',$result->id_admin);
    return redirect('admin/dashboard');
            }else{
                Session::put('message',"Lỗi tài khoản hoặc mật khẩu chưa đúng");
                return redirect('admin');
            }



    }
    public function export_csv(){

        return Excel::download(new ExcelExports() , 'product.xlsx');

    }
    public function import_csv(Request $request){

        $path = $request->file('file')->getRealPath();
        Excel::import(new Imports(), $path);
        return back();


    }

}
