<?php
namespace App\Http\Controllers;
date_default_timezone_set("Asia/Bangkok");



use App\Models\Brands_product;
use App\Models\Brands_product_shop;
use App\Models\Category_product;
use Illuminate\Http\Request;
use DB;
use Session;
class BrandProductController extends Controller
{



    public function getAddCategory(){
        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }


       return view('admin.brands_product.addCategory');



    }
    public function getEditCategory($id){
        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }
        $aa=Brands_product::find($id);

       return view('admin.brands_product.editCategory',compact('aa'));



    }
    public function getUnactive($id){
        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }
        $data=Brands_product::find($id);
        $data->brands_status=0;
        $data->save();
        Session::put('message_success','Cập nhật thành công');
        return redirect('brands/list');



    }
    public function getActive($id){

        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }
        $data=Brands_product::find($id);
        $data->brands_status=1;
        $data->save();
        Session::put('message_success','Cập nhật thành công');
        return redirect('brands/list');



    }

    public function getListCategory(){

        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }
        $data=Brands_product_shop::where('id_shop',Session::get('id_shop_user'))->paginate(10);
       return view('admin.brands_product.listCategory',compact('data'));



    }
    public function postAddCategory(Request $request){

        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }
        $dheck=Brands_product::where('brands_name',$request->category_product_name)->first();
        if($dheck==null){

            $category_product=new Brands_product();

            $category_product->brands_name=$request->category_product_name;
            $category_product->brands_name_slug=slugify($request->category_product_name);
            $category_product->brands_desc=$request->category_product_desc;
            $category_product->brands_parent=0;
            if($request->category_status!=null) {
                $category_product->brands_status = $request->category_status;
            }else{
                $category_product->brands_status = 0;
            }
            $category_product->save();

            $category_product_shop=new Brands_product_shop();
            $category_product_shop->id_shop=Session::get('id_shop_user');
            $category_product_shop->id_brands_product=$category_product->id_brands_product;

            if($request->category_status!=null) {
                $category_product_shop->status = $request->category_status;
            }else{
                $category_product_shop->status = 0;
            }
            $category_product_shop->save();




            Session::put('message','Cập nhật thành công');
            return redirect('admin/brands/list');

        }else{
            Session::put('message_error','Tên thương hiệu đã tồn tại !Cập nhật không thành công');
            Session::put('name_category',$request->category_product_name);
            Session::put('category_desc',$request->category_product_desc);
            Session::put('parent',$request->parent);

            return back();
        }
    }
    public function postEditCategory(Request $request,$id){

        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $dheck=Brands_product::where('brands_name',$request->category_product_name)->first();
        if($dheck==null){

            $category_product=Brands_product::find($id);

            $category_product->brands_name=$request->category_product_name;
            $category_product->brands_name_slug=slugify($request->category_product_name);
            $category_product->brands_desc=$request->category_product_desc;
            $category_product->brands_parent=0;
            if($request->category_status!=null) {
                $category_product->brands_status = $request->category_status;
            }else{
                $category_product->brands_status = 0;
            }
            $category_product->save();

            Session::put('message','Cập nhật thành công');
            return redirect('admin/brands/list');

        }else{
            return back();
        }




    }

    public function getDelCategory($id){

        if(!Session::get('admin_id')&&!Session::get('id_shop_user')){
            return redirect('admin');

        }

$brands=Brands_product_shop::where('id_brands_product',$id)->where('id_shop',Session::get('id_shop_user'))->first();
        if($brands!=null){
            Brands_product_shop::where('id_brands_product',$id)->delete();
              Brands_product::destroy($id);

                Session::put('message_success','Cập nhật thành công');
                return redirect('admin/brands/list');

        }
    }
//      Category_product::destroy($id);

//        Session::put('message_success','Cập nhật thành công');
//        return redirect('category/list');



    public function getLogout(){

        Session::put('admin_name','');
        Session::put('admin_id','');
        return redirect('admin');



    }

    public function postDashboard(Request $request){
            $email=$request->Email;
            $password=$request->Password;
            $result=DB::table('tbl_admin')->where('admin_email',$email)->where('admin_password',md5($password))->first();

            if($result!=null){
    Session::put('admin_name',$result->admin_name);
    Session::put('admin_id',$result->id_admin);
    return redirect('admin/dashboard');
            }else{
                Session::put('message',"Lỗi tài khoản hoặc mật khẩu chưa đúng");
                return redirect('admin');
            }



    }

}
