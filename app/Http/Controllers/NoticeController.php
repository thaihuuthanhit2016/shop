<?php
namespace App\Http\Controllers;
date_default_timezone_set("Asia/Bangkok");



use App\Models\Brands_product;
use App\Models\Brands_product_shop;
use App\Models\Category_product;
use App\Models\Category_product_shop;
use App\Models\Customer;
use App\Models\Notification;
use App\Models\Product;
use App\Models\Product_images;
use App\Models\Product_shop;
use App\Models\Shop;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Session;
class NoticeController extends Controller
{



    public function getAddCategory(){
        if(!Session::get('admin_id')&&!Session::get('username')){
            return redirect('admin');

        }
$shop=Shop::all();
$customer=Customer::all();
       return view('admin.notice.addCategory',compact('shop','customer'));



    }
    public function getEditCategory($id){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }


        $shop=Shop::all();
        $customer=Customer::all();
        $notice=Notification::find($id);

       return view('admin.notice.editCategory',compact('shop','notice','customer'));



    }

    public function getViewCategory($id){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();
        if($checkshop->verifyKYC!='Y'){
            Session::put('message_error_kyc','Shop chưa được active!');
            return  back();
        }

        $data=Category_product::all();
        $brands=Brands_product::all();
        $product=Notification::where('title_alias',$id)->first();
        $product_up=Notification::find($product->id_notification);
        $product_up->
       return view('admin.notice.viewCategory',compact('data','product','brands'));



    }

    public function getListCategory(){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }


        $data=Notification::orderby('id_notification','DESC')->paginate(10);
       return view('admin.notice.listCategory',compact('data'));



    }
    public function getDelImages($id){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }

        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();
        if($checkshop->verifyKYC!='Y'){
            Session::put('message_error_kyc','Shop chưa được active!');
            return  back();
        }

        Product_images::destroy($id);
        Session::put('message_success','Cập nhật thành công');
        return back();

    }
    public function getUnactive($id){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }

        $data=Category_product::find($id);
        $data->category_status=0;
        $data->save();
        Session::put('message_success','Cập nhật thành công');
       return redirect('category/list');



    }
    public function getActive($id){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $data=Category_product::find($id);
        $data->category_status=1;
        $data->save();
        Session::put('message_success','Cập nhật thành công');
       return redirect('category/list');



    }
    public function postAddCategory(Request $request){
        if(!Session::get('admin_id') &&!Session::get('id_shop_user')){
            return redirect('admin');

        }

        $notice=new Notification();
        $notice->title=$request->title;
        $notice->type=$request->type;
        $notice->content=$request->content;
        $notice->id_shop=$request->id_shop;
        $notice->id_customer=$request->id_customer;
        $notice->title_alias=slugify($request->title);
        $notice->save();
            Session::put('message','Cập nhật thành công');
            return redirect('admin/notice/list');

    }
    public function postEditCategory(Request $request,$id){
        if(!Session::get('admin_id')){
            return redirect('admin');

        }

        $notice=Notification::find($id);
        $notice->title=$request->title;
        $notice->type=$request->type;
        $notice->content=$request->content;
        $notice->id_shop=$request->id_shop;
        $notice->id_customer=$request->id_customer;

        $notice->title_alias=slugify($request->title);
        $notice->save();

            Session::put('message','Cập nhật thành công');
            return redirect('admin/notice/list');



    }

    public function getDelCategory($id){
        if(!Session::get('admin_id')&& !Session::get('id_shop_user')){
            return redirect('admin');

        }

        $checkshop=Shop::where('id_shop_user',Session::get('id_shop_user'))->first();
        if($checkshop->verifyKYC!='Y'){
            Session::put('message_error_kyc','Shop chưa được active!');
            return  back();
        }

        $check=Category_product::where('id_category_product',$id)->first();
        if($check!=null){
            $check=Category_product::where('category_parent',$check->id_category_product)->get();
            if(count($check)==0){

              Category_product::destroy($id);

                Session::put('message_success','Cập nhật thành công');
                return redirect('admin/product/list');

            }else{

                Session::put('message_error','Danh mục vừa xóa có danh mục con. Cập nhật không thành công');
                return redirect('admin/product/list');

            }

        }
//      Category_product::destroy($id);

//        Session::put('message_success','Cập nhật thành công');
//        return redirect('category/list');


    }
    public function getLogout(){

        Session::put('admin_name','');
        Session::put('admin_id','');
        return redirect('admin');



    }

    public function postDashboard(Request $request){
            $email=$request->Email;
            $password=$request->Password;
            $result=DB::table('tbl_admin')->where('admin_email',$email)->where('admin_password',md5($password))->first();

            if($result!=null){
    Session::put('admin_name',$result->admin_name);
    Session::put('admin_id',$result->id_admin);
    return redirect('admin/dashboard');
            }else{
                Session::put('message',"Lỗi tài khoản hoặc mật khẩu chưa đúng");
                return redirect('admin');
            }



    }

}
