<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Feeship extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table="tbl_fee_ship";
    protected $primaryKey  = "Id_fee";
    public function city(){
        return $this->belongsTo('App\Models\City','fee_matp');
    }
    public function province(){
        return $this->belongsTo('App\Models\Province','fee_maqh');
    }
    public function wards(){
        return $this->belongsTo('App\Models\Wards','fee_xaid');
    }
    const UPDATED_AT = null;
    const CREATED_AT = null;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
