<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Category_product extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table="tbl_category_product";
    protected $primaryKey  = "id_category_product";

    const UPDATED_AT = null;
    const CREATED_AT = null;

    public  function  child(){
        return $this->hasMany(Category_product::class,'category_parent','id_category_product');
    }
    public static function  recurrsive($category,$parents =0,$level=1,$listCategory){
        $i=0;
        if(count($category)>0){

            foreach ($category as $c =>$value){
                $i++;
                if($value->category_parent==$parents){
                    $value->level=$level;
                    $listCategory[]=$value;

                    unset($category[$c]);
                    $parent=$value->id_category_product;

                    self::recurrsive($category,$parent,$level+1,$listCategory);
                }
                }
        }


    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
