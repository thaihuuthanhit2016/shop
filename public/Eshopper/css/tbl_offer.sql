-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1:3306
-- Thời gian đã tạo: Th9 06, 2021 lúc 09:54 AM
-- Phiên bản máy phục vụ: 10.4.10-MariaDB
-- Phiên bản PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `elarshoping`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_offer`
--

DROP TABLE IF EXISTS `tbl_offer`;
CREATE TABLE IF NOT EXISTS `tbl_offer` (
  `id_offer` int(11) NOT NULL AUTO_INCREMENT,
  `code_offer` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `percent` int(11) NOT NULL DEFAULT 0,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `number` int(11) NOT NULL,
  `from_date` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `end_date` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id_offer`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_offer`
--

INSERT INTO `tbl_offer` (`id_offer`, `code_offer`, `title`, `percent`, `content`, `number`, `from_date`, `end_date`, `status`, `type`) VALUES
(1, 'Bicoin1', 'Bicoin1', 30, 'Khuyến Mãi 1', 1, '26-07-2021', '28-08-2021', 1, 0),
(3, '03', 'qauy', 15, 'ỵyjj', 1, '14-08-2021', '14-08-2021', 1, 3),
(4, 'khác1', 'Khác', 23, 'Khuyến Mãi 1', 2, '26-07-2021', '28-08-2021', 1, 1),
(5, 'YENLK201830', 'Yenshop', 30, 'jjj', 1, '16-08-2021', '19-08-2021', 1, 2),
(9, 'D9NGkF', 'Quay thưởng', 10, 'Quay thưởng', 1, '30-08-2021', '06-09-2021', 1, 3),
(6, 'quay', 'Quay thưởng', 10, 'Quay thưởng', 1, '29-08-2021', '05-09-2021', 1, 3),
(7, '2k0s69', 'Quay thưởng', 10, 'Quay thưởng', 1, '29-08-2021', '05-09-2021', 1, 3),
(8, 'i54buO', 'Quay thưởng', 10, 'Quay thưởng', 1, '30-08-2021', '06-09-2021', 1, 3),
(10, 'G1IZ6D', 'Quay thưởng', 10, 'Quay thưởng', 1, '30-08-2021', '06-09-2021', 1, 3);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
