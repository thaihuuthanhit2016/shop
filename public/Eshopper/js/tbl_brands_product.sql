-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1:3306
-- Thời gian đã tạo: Th9 07, 2021 lúc 02:05 AM
-- Phiên bản máy phục vụ: 10.4.10-MariaDB
-- Phiên bản PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `elarshoping`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_brands_product`
--

DROP TABLE IF EXISTS `tbl_brands_product`;
CREATE TABLE IF NOT EXISTS `tbl_brands_product` (
  `id_brands_product` int(11) NOT NULL AUTO_INCREMENT,
  `brands_name` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `brands_name_slug` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `brands_status` int(11) NOT NULL,
  `brands_desc` longtext COLLATE utf8_unicode_ci NOT NULL,
  `brands_parent` int(11) NOT NULL,
  PRIMARY KEY (`id_brands_product`),
  UNIQUE KEY `brands_name` (`brands_name`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_brands_product`
--

INSERT INTO `tbl_brands_product` (`id_brands_product`, `brands_name`, `brands_name_slug`, `brands_status`, `brands_desc`, `brands_parent`) VALUES
(4, 'Dell1', 'dell1', 1, 'Dell1', 0),
(5, 'Kinh Đô', 'kinh-do', 1, 'Kinh Đô', 0),
(6, 'Như Lan', 'nhu-lan', 1, 'Như Lan', 0),
(7, 'Cocacola', 'cocacola', 1, 'Cocacola', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
