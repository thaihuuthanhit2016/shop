-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1:3306
-- Thời gian đã tạo: Th9 07, 2021 lúc 02:05 AM
-- Phiên bản máy phục vụ: 10.4.10-MariaDB
-- Phiên bản PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `elarshoping`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_category_product`
--

DROP TABLE IF EXISTS `tbl_category_product`;
CREATE TABLE IF NOT EXISTS `tbl_category_product` (
  `id_category_product` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `category_name_slug` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `category_status` int(11) NOT NULL,
  `category_desc` longtext COLLATE utf8_unicode_ci NOT NULL,
  `category_parent` int(11) NOT NULL,
  PRIMARY KEY (`id_category_product`),
  UNIQUE KEY `category_name` (`category_name`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_category_product`
--

INSERT INTO `tbl_category_product` (`id_category_product`, `category_name`, `category_name_slug`, `category_status`, `category_desc`, `category_parent`) VALUES
(1, 'Kẹo', 'keo', 1, 'Kẹo', 0),
(2, 'X-box PS4', 'x-box-ps4', 1, 'X-box PS4', 1),
(3, 'X-box2 PS4s', 'x-box2-ps4s', 1, 'X-box2 PS4s', 2),
(4, 'Bánh', 'banh', 1, 'Bánh', 0),
(5, 'Nước giải khát', 'nuoc-giai-khat', 1, 'Nước giải khát', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
