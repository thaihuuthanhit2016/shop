<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'facebook' => [
        'client_id' => '210227564450050',  //client face của bạn
        'client_secret' => '564391b65d7f8505a4db96469c565d14',  //client app service face của bạn
        'redirect' => 'http://localhost/shopbitcoin/callback/facebook' //callback trả về
    ],

    'google' => [
        'client_id' => '747433976373-7ra1a6ee4gukh0jlia8igi37k70k9qa4.apps.googleusercontent.com',
        'client_secret' => '0x-wk2WvkhtSEk_2I8QCKr3K',
        'redirect' => 'http://localhost/shopbitcoin/google/callback',
    ],
    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

];
