@extends('admin.master');
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Thông tin shop
                </header>
                <br>
                @include('admin.errors.error')

                <div class="panel-body">
                    <div class="position-center">

                        <form role="form" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                @include('admin.errors.error')
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">T:<i>{{$billdetail->username}}</i></label>

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên Shop:<i>{{$product->name_shop}}</i></label>

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Logo Shop:<img src="{{asset('upload/shop/'.$product->images3)}}" width="100px"></label>

                            </div>
                            <a href="{{asset('admin/shop/approve/'.$product->id_shop_user)}}" type="submit" class="btn btn-info">Duyệt</a>
                            <a href="{{asset('admin/shop/reject/'.$product->id_shop_user)}}" type="submit" class="btn btn-danger">Từ chối</a>

                        </form>
                    </div>

                </div>
            </section>

        </div>

    </div>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        if($item->parent_id==$parent_id){
            echo '<option value="'.$item->id_category_product.'">'.$char.$item->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>
