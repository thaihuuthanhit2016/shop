@extends('admin.master');
@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Sửa Kho
            </header>
            <div class="panel-body">
                <div class="position-center">

                    <form role="form" method="post">
                        @csrf
                        <div class="form-group">
                            @include('admin.errors.error')
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên Kho</label>
                            <input type="text" class="form-control" value="{{$warehouse->name_warehouse}}" name="category_product_name" id="exampleInputEmail1" placeholder="Tên danh mục">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Địa chỉ</label>
                            <input type="text" class="form-control" value="{{$warehouse->address}}" name="address" id="exampleInputEmail1" placeholder="Tên danh mục">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Số điện thoại</label>
                            <input type="text" class="form-control" value="{{$warehouse->phone}}" name="phone" id="exampleInputEmail1" placeholder="Tên danh mục">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Mô tả Kho</label>
                            <textarea name="description" class="form-control" id="exampleInputPassword1" >{{$warehouse->description}}</textarea>
                        </div>
                        <button type="submit" class="btn btn-info">Submit</button>
                    </form>
                </div>

            </div>
        </section>

    </div>

</div>
@endsection
<?php

function showCategories1($categories,$da,$parent_id=0,$char=''){
    $i=0;
    foreach($categories as $key =>$item){
        $i++;
        if($item->parent_id==$parent_id){
            $html='';
            $html.= '<option value="'.$item->id_category_product.'"';if($item->id_category_product==$da->category_parent){ $html.='selected';}$html.=' >'.$char.$item->category_name.'</option>';
            echo $html;
            unset($categories[$key]);
            showCategories1($categories,$item->id_category_product,$char.=' -- ');
        }
    }
}
?>
