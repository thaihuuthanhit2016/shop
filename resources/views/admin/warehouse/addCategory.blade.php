@extends('admin.master');
@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Thêm Kho
            </header>
            <div class="panel-body">
                <div class="position-center">

                    <form role="form" method="post">
                        @csrf
                        <div class="form-group">
                            @include('admin.errors.error')
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên Kho</label>
                            <input type="text" class="form-control" value="@if(Session::get('name_category')!=null) {{Session::get('name_category')}} @endif" name="category_product_name" id="exampleInputEmail1" placeholder="Tên danh mục">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Địa chỉ</label>
                            <input type="text" class="form-control" value="@if(Session::get('name_category')!=null) {{Session::get('name_category')}} @endif" name="address" id="exampleInputEmail1" placeholder="Tên danh mục">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Số điện thoại</label>
                            <input type="text" class="form-control" value="@if(Session::get('name_category')!=null) {{Session::get('name_category')}} @endif" name="phone" id="exampleInputEmail1" placeholder="Tên danh mục">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Mô tả Kho</label>
                            <textarea name="description" class="form-control" id="exampleInputPassword1" >@if(Session::get('category_desc')!=null) {{Session::get('category_desc')}} @endif</textarea>
                        </div>
                        <button type="submit" class="btn btn-info">Submit</button>
                    </form>
                </div>

            </div>
        </section>

    </div>

</div>
@endsection
<?php
//
//function showCategories($categories,$parent_id=0,$char=''){
//    $i=0;
//    foreach($categories as $key =>$item){
//        $i++;
//        $data=DB::table('tbl_category_product')->where('id_category_product',$item->id_category_product)->first();
//        if($item->parent_id==$parent_id){
//            echo '<option value="'.$data->id_category_product.'ss'.$i.'">'.$char.$data->category_name.'</option>';
//            unset($categories[$key]);
//            showCategories($categories,$item->id_category_product,$char=' | --');
//        }
//    }
//}

function showCategories($categories, $parent_id = 0, $char = '')
{
    foreach ($categories as $key => $item)
    {
        $data=DB::table('tbl_category_product')->where('id_category_product',$item->id_category_product)->first();

        // Nếu là chuyên mục con thì hiển thị
        if ($data->category_parent== $parent_id)
        {
            echo '<option value="'.$data->id_category_product.'">';
            echo $char . $data->category_name;
            echo '</option>';

            // Xóa chuyên mục đã lặp
            unset($categories[$key]);

            // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
            showCategories($categories, $data->id_category_product, $char.'-- ');
        }
    }
}
?>
