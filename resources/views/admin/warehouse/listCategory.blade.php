@extends('admin.master');
@section('content')
    <div class="table-agile-info">
        <div class="panel panel-default">
            <div class="panel-heading">
                Danh sách Kho - Bãi - Vận Chuyển
            </div>
            <div class="row w3-res-tb">
                <div class="col-sm-5 m-b-xs">
                    <select class="input-sm form-control w-sm inline v-middle">
                        <option value="0">Bulk action</option>
                        <option value="1">Delete selected</option>
                        <option value="2">Bulk edit</option>
                        <option value="3">Export</option>
                    </select>
                    <button class="btn btn-sm btn-default">Apply</button>
                </div>
                <div class="col-sm-4">
                </div>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input type="text" class="input-sm form-control" placeholder="Search">
                        <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button">Go!</button>
          </span>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                @include('admin.errors.error')
                <table class="table table-striped b-t b-light">
                    <thead>
                    <tr>

                        <th>Mã vận chuyển</th>
                        <th>Mã đơn hàng</th>
                        <th>Khách hàng</th>
                        <th>Ngày tạo</th>

                        <th>Trạng thái</th>
                        <th>COD</th>
                        <th>Tiền COD</th>
                        <th style="width:30px;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($warehouse as $d)
<?Php
$bill=DB::table('')->first();
?>
                    <tr>
<td>{{$d->name_warehouse}}</td>
<td>{{$d->address}}</td>
<td>{{$d->phone	}}</td>
<td>@if($d->status==1) full @else no full @endif</td>

                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <footer class="panel-footer">
                <div class="row">

                    <div class="col-sm-7 text-right text-center-xs">
                        {{$warehouse->links('admin.paginate')}}
                    </div>
                </div>
            </footer>
        </div>
    </div>

@endsection
