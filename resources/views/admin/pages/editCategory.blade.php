@extends('admin.master');
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Sửa Trang Đơn
                </header>
                <div class="panel-body">
                    <div class="position-center">

                        <form role="form" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                @include('admin.errors.error')
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tiêu đề</label>
                                <input type="text" class="form-control" value="{{$product->title}}"  name="product_name" id="exampleInputEmail1" placeholder="Tên ">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Nội dung</label>
                                <textarea name="product_content" class="form-control" id="product_content" >{{$product->content}}</textarea>


                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Loại</label>
                                <select name="type" class="form-control" id="type" >
                                    <option value="0" @if($product->type==0) selected @endif>Chính sách</option>
                                    <option value="1" @if($product->type==1) selected @endif>Hướng dẫn</option>
                                    <option value="2" @if($product->type==2) selected @endif>Khuyến mãi</option>
                                    <option value="3" @if($product->type==3) selected @endif>Dịch vụ</option>
                                </select>


                            </div>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </form>
                    </div>

                </div>
            </section>

        </div>

    </div>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        if($item->parent_id==$parent_id){
            echo '<option value="'.$item->id_category_product.'">'.$char.$item->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>
