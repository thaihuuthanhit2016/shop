@extends('admin.master');
@section('content')
    <div class="table-agile-info">
        <div class="panel panel-default">
            <div class="panel-heading">
                Danh sách danh mục
            </div>
            <div class="row w3-res-tb">
                <div class="col-sm-5 m-b-xs">
                    <select class="input-sm form-control w-sm inline v-middle">
                        <option value="0">Bulk action</option>
                        <option value="1">Delete selected</option>
                        <option value="2">Bulk edit</option>
                        <option value="3">Export</option>
                    </select>
                    <button class="btn btn-sm btn-default">Apply</button>
                </div>
                <div class="col-sm-4">
                </div>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input type="text" class="input-sm form-control" placeholder="Search">
                        <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button">Go!</button>
          </span>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                @include('admin.errors.error')
                <table class="table table-striped b-t b-light">
                    <thead>
                    <tr>
                        <th style="width:20px;">
                            <label class="i-checks m-b-none">
                                <input type="checkbox"><i></i>
                            </label>
                        </th>
                        <th>Tên Danh mục</th>

                        <th>Danh mục Cha</th>
                        <th>Ẩn/Hiện</th>
                        <th style="width:30px;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=0;
                 ?>
                    @foreach($category as $d)
                        <?php
$i++;
                         $category_shop=DB::table('tbl_category_product_shop')->where('id_shop',Session::get('id_shop_user'))->first();
                         if ($category_shop!=null){
                        $category_cha=DB::table('tbl_category_product')->where('id_category_product',$d->category_parent)->first();


                        ?>
                    <tr>
                        <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                        <td>{{$d->category_name}}</td>
                        @if($d->category_parent!=0)
                        <td>{{$category_cha->category_name}}</td>
                        @else
                            <td>none</td>
                        @endif
                        <td>@if($d->category_status==1) Hiện @else Ẩn @endif</td>
                        <td>
                            <a href="{{asset('admin/category/edit/'.$d->id_category_product)}}" class="active" ui-toggle-class=""><i class="fa fa-pencil-square-o text-success text-active"></i></a> <a href="{{asset('category/del/'.$d->id_category_product)}}" onclick="confirm('Bạn có chắc xóa không?')"> <i class="fa fa-times text-danger text"></i></a>
                        </td>


                    </tr>
                        <?php }?>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <footer class="panel-footer">
                <div class="row">

                    <div class="col-sm-7 text-right text-center-xs">
                        {{$category->links('admin.paginate')}}
                    </div>
                </div>
            </footer>

        </div>
    </div>

@endsection
