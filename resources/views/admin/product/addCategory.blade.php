@extends('admin.master');
@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Thêm Sản phẩm
            </header>
            <div class="panel-body">
                <div class="position-center">

                    <form role="form" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            @include('admin.errors.error')
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên Sản phẩm</label>
                            <input type="text" required class="form-control" value="@if(Session::get('product_name')!=null) {{Session::get('product_name')}} @endif" name="product_name" id="exampleInputEmail1" placeholder="Tên ">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mã Sản phẩm</label>
                            <input type="text" required class="form-control" value="@if(Session::get('product_code')!=null) {{Session::get('product_code')}} @endif" name="product_code" id="exampleInputEmail1" placeholder="Tên ">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Số lượng</label>
                            <input type="number" required class="form-control" name="stock" id="exampleInputEmail1" placeholder="Tên ">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Giá Sản phẩm</label>
                            <input type="text" required class="form-control" value="@if(Session::get('product_price')!=null) {{Session::get('product_price')}} @endif" name="product_price" id="price" placeholder="Tên ">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Giá Sản phẩm (KM)</label>
                            <input type="text" class="form-control" value="@if(Session::get('product_price_km')!=null) {{Session::get('product_price_km')}} @endif" name="product_price_km"id="price_km" placeholder="Tên ">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Hình ảnh Sản phẩm</label>
                            <input type="file" required class="form-control  images-preview" name="product_images" onchange="previewFile(this)">
                            <div class="clearfix"></div>
                            <br>
                            <img src="{{asset('upload/avatar/no-image-800x600.png')}}" id="previewimg" >
                            <div class="clearfix"></div>
                            <br>
                        </div>
<style>
    #previewimg{
        width: 200px!important;
        height:  auto!important;

    }
    .none{
        display: none;
    }
</style>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nhóm Hình ảnh Sản phẩm (Tối đa 5 hình)</label>
                            <input type="file" class="form-control images-previews" name="product_images_group[]" multiple onchange="previewFile2(this)">
                            <div class="clearfix"></div>
                            <br>
                            <img class="none previewimg0" >
                            <img class="none previewimg1" >
                            <img class="none previewimg2" >
                            <img class="none previewimg3" >
                            <img class="none previewimg4" >
                            <img class="none previewimg5" >


                            <div class="clearfix"></div>

                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Mô tả Sản phẩm</label>
                            <textarea name="product_desc" required class="form-control" id="exampleInputPassword1" >@if(Session::get('product_desc')!=null) {{Session::get('product_desc')}} @endif</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Nội dung Sản phẩm</label>
                            <textarea name="product_content" class="form-control" id="product_content" >@if(Session::get('product_content')!=null) {{Session::get('product_content')}} @endif</textarea>


                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Tag Sản phẩm</label>
                            <input type="text" value="" class="form-control"  name="product_tags" data-role="tagsinput" placeholder="Add tags" />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Danh mục sản phẩm</label>
                            <select  required class="form-control  input-sm m-bot-15" name="category_id">
                                <option value="0">chon</option>

                            @foreach($data as $d)
                            <?php
                                    $c=DB::table('tbl_category_product')->where('id_category_product',$d->id_category_product)->where('category_parent','<>',0)->first();
                                    if ($c!=null){
                                    ?>
                                <option value="{{$c->id_category_product}}">{{$c->category_name}}</option>
                                <?php }?>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Thương hiệu sản phẩm</label>
                            <select required class="form-control input-sm m-bot-15" name="brands_id">
                                <option value="0">chon</option>

                                @foreach($brands as $d)

                                    <?php
                                    $c=DB::table('tbl_brands_product')->where('id_brands_product',$d->id_brands_product)->first();
                                    ?>
                                    <option value="{{$c->id_brands_product}}">{{$c->brands_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Tình trạng</label>
                            <select class="form-control input-sm m-bot-15" name="product_status">
                                <option value="1">Còn hàng</option>

                                <option value="0">Hết hàng</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Quy Cách</label>
                            <select required class="form-control input-sm m-bot-15" id="spec" name="specifications">
                                <option >Chọn</option>
                                <option value="GOI">Gói</option>

                                <option value="LON">Lon</option>
                                <option value="SIZE">Size</option>
                                <option value="THUNG">Thùng</option>
                                <option value="KG">Kg</option>
                                <option value="HOP">Hộp</option>
                                <option value="CAI">Cái</option>
                            </select>
                        </div>
                        <div id="goi">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Giá Gói Nhỏ</label>
                                <input type="text" id="price_small" class="form-control" value="@if(Session::get('product_price')!=null) {{Session::get('product_price')}} @endif" name="product_price_small" id="exampleInputEmail1" placeholder="Tên ">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Giá Gói Nhỏ (KM)</label>
                                <input type="text" id="price_small_km" class="form-control" value="@if(Session::get('product_price_km')!=null) {{Session::get('product_price_km')}} @endif" name="product_price_small_km" id="exampleInputEmail1" placeholder="Tên ">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Giá Gói Lớn</label>
                                <input type="text" class="form-control" value="@if(Session::get('product_price')!=null) {{Session::get('product_price')}} @endif" name="product_price_big" id="exampleInputEmail1" placeholder="Tên ">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Giá Gói Lớn (KM)</label>
                                <input type="text" class="form-control" value="@if(Session::get('product_price_km')!=null) {{Session::get('product_price_km')}} @endif" name="product_price_big_km" id="exampleInputEmail1" placeholder="Tên ">
                            </div>
                        </div>
                        <div id="thung">
                            <div class="form-group">
                                <input type="checkbox" id="ck_price_small"  value="1"  id="exampleInputEmail1" placeholder="Tên ">
                                <label for="exampleInputEmail1">12 lon</label>

                                <input type="text" id="text_price_small" class="form-control" value="" name="product_price_small_thung" id="exampleInputEmail1">
                            </div>

                            <div class="form-group">
                                <input type="checkbox" id="ck_price_small_km" value="1"  id="exampleInputEmail1" placeholder="Tên ">
                                <label for="exampleInputEmail1">12 lon (KM)</label>
                                <input type="text" id="text_price_small_km" class="form-control" value="" name="product_price_small_km_thung" id="exampleInputEmail1" placeholder="Tên ">
                            </div>
                            <div class="form-group">
                                <input type="checkbox" id="ck_price_big" value="1"  id="exampleInputEmail1" placeholder="Tên ">

                                <label for="exampleInputEmail1">24 lon</label>
                                <input type="text" class="form-control" value="" name="product_price_big" id="text_price_big_thung"  placeholder="Tên ">
                            </div>

                            <div class="form-group">
                                <input type="checkbox" id="ck_price_big_km" value="1"  id="exampleInputEmail1" placeholder="Tên ">

                                <label for="exampleInputEmail1">24 lon (KM)</label>
                                <input type="text" class="form-control" name="product_price_big_km" id="text_price_big_thung_km" placeholder="Tên ">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info">Submit</button>
                    </form>
                </div>

            </div>
        </section>

    </div>

</div>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        if($item->parent_id==$parent_id){
            echo '<option value="'.$item->id_category_product.'">'.$char.$item->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript" src="{{asset('public/admin/js/bootstrap-tagsinput.js')}}"></script>


<script>
    function previewFile(input){
        var file=$('.images-preview').get(0).files[0];
        if(file){
            var  reader=new  FileReader();
            reader.onload=function (){
                $("#previewimg").attr('src',reader.result);
            }
            reader.readAsDataURL(file);
        }
    }
    $(document).ready(function () {
        $("#size").addClass('none');
        $("#goi").addClass('none');
        $("#thung").addClass('none');
        $("#text_price_small").addClass('none');
        $("#text_price_big").addClass('none');
        $("#text_price_small_km").addClass('none');
        $("#text_price_big_km").addClass('none');
        $("#goi").addClass('none');
        var g=$('#spec').val();

        $('#spec').change(function () {
            var g=$('#spec').val();

            if(g=="GOI"){
                $("#goi").removeClass('none')
                $("#size").addClass('none')
                $("#thung").addClass('none')
            }
            if(g=="SIZE"){
                $("#goi").addClass('none')
                $("#thung").addClass('none')
                $("#size").removeClass('none')
            }if(g=="THUNG"){
                $("#goi").addClass('none')
                $("#thung").removeClass('none')
                $("#size").removeClass('none')
            }

        })

        $("#price_small").keyup(function () {
            var price_small=$("#price_small").val();
            $("#price").val(price_small);

        });
        $("#price_small_km").keyup(function () {
            var price_small_km=$("#price_small_km").val();
            $("#price_km").val(price_small_km);

        });
        $("#price_km").keyup(function () {
            var price_km=$("#price_km").val();
            $("#text_price_big_km").val(price_km);
            $("#text_price_big_thung_km").removeClass('none');
            $("#text_price_big_thung_km").val(price_km);
            $("#price_small_km").val(price_km);
            $('#ck_price_big_km').val(this.checked);
            $("#ck_price_big_km").prop('checked', true);

        });
        $("#price").keyup(function () {
            var price= $("#price").val();
            $("#text_price_big").val(price);
            $("#ck_price_big").prop('checked', true);

            $("#text_price_big_thung").removeClass('none');
            $("#text_price_big_thung").val(price);
            $("#thung").removeClass('none');

            $("#price_small").val(price);
            console.log(price);
        });

        $('#ck_price_small').change(function() {
            if(this.checked) {
                $("#text_price_small").removeClass('none');

            }else {
                $("#text_price_small").addClass('none');
            }
        });
        $('#ck_price_small_km').change(function() {
            if(this.checked) {
                $("#text_price_small_km").removeClass('none');

            }else {
                $("#text_price_small_km").addClass('none');
            }
        });
        $('#ck_price_big').change(function() {
            if(this.checked) {
                $("#text_price_big").removeClass('none');

            }else {
                $("#text_price_big").addClass('none');
            }
        });
        $('#ck_price_big_km').change(function() {
            if(this.checked) {
                $("#text_price_big_km").removeClass('none');

            }else {
                $("#text_price_big_km").addClass('none');
            }
        });




    });
</script>

