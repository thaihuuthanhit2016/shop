@extends('admin.master');
@section('content')
    <div class="table-agile-info">
        <div class="panel panel-default">
            <div class="panel-heading">
                Danh sách Sản phẩm
            </div>
<br>
            <div class="table-responsive">
                @include('admin.errors.error')
                <br>
                <br>
<?php
                Session::put('message_error','');
                Session::put('product_code',"");
                Session::put('product_name','');
                Session::put('product_price','');
                Session::put('product_price_km','');
                Session::put('product_desc','');
                Session::put('product_content','');

          ?>
                <br>
                <table id="example" class="display" cellspacing="0" width="100%">

                        <thead>
                        <tr>
                            <th>QR</th>
                            <th>Tên Sản phẩm</th>
                            <th>Hình ảnh</th>

                            <th>Giá sản phẩm</th>
                            <th>Giá sản phẩm (Khuyến mãi)</th>
                            <th>Tình trạng</th>
                            <th>Danh mục</th>
                            <th>Thương hiệu</th>
                            <th>Số lượng tồn</th>
                            <th style="width:100px;">Tùy chọn</th>
                        </tr>
                        </thead>
                        <tbody id="kq">
                        @foreach($data as $c)

                            <?php

                            $d=DB::table('tbl_product')->where('id_product',$c->id_product)->first();
                            $category=DB::table('tbl_category_product')->where('id_category_product',$d->category_id)->first();

                            $brands=DB::table('tbl_brands_product')->where('id_brands_product',$d->brands_id)->first();
                            $depot=DB::table('tbl_depot')->where('id_product',$c->id_product)->first();
                            if($depot->stock==0){
                                $s= DB::table('tbl_product')
                                    ->where('id_product',$c->id_product)
                                    ->first();
                            }
                            ?>
                            <tr style="@if($d->product_status==0) background:red;color: white!important; @endif">
                                <td></td>
                                <td>{{$d->product_name}}@if($d->product_price_km!=0)<sup style="color:red;">Sale</sup>@endif</td>
                                <td><img  src="{{asset('upload/product/'.$d->product_images)}}" width="50px" ></td>
                                <td>{{number_format($d->product_price,0,',','.')}} Đồng</td>
                                <td>{{number_format($d->product_price_km,0,',','.')}} Đồng</td>
                                <td>@if($d->product_status==0) <span style="color:white;">Hết hàng </span> @else<span style="color:green;"> Còn hàng </span>@endif</td>
                                @if($category!=null)
                                    <td>{{$category->category_name}}</td>
                                @else
                                    <td>None</td>

                                @endif
                                <td>{{$brands->brands_name}}</td>
                                <td>{{$depot->stock}}</td>

                                <td>
                                    <a href="{{asset('admin/product/view/'.$d->id_product)}}" class="active" ui-toggle-class=""><i class="fa fa-eye text-success text-active"></i></a>&nbsp;&nbsp;&nbsp;<a href="{{asset('admin/product/edit/'.$d->id_product)}}" class="active" ui-toggle-class=""><i class="fa fa-pencil-square-o text-success text-active"></i></a>&nbsp;&nbsp;&nbsp; <a href="{{asset('admin/product/del/'.$d->id_product)}}" onclick="confirm('Bạn có chắc xóa không?')"> <i class="fa fa-times text-danger text"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
            </div>
            <footer class="panel-footer">

                <form action="{{url('admin/export-csv')}}" method="POST">
                    @csrf

                    <button type="submit"  name="export_csv" class="btn btn-success"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;&nbsp;Export excel</button>
                </form>

            </footer>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src='https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js'></script>
    <script src="{{asset('public/admin/dist_datatable/script.js')}}"></script>

@endsection

<script>
    $(document).ready(function (){
        $('#phantrang').change(function (){
            var phantrang=$(this).val();

            $.ajax({
                url : "{{asset('admin/product/list_/')}}", // gửi ajax đến file result.php
                type : "get", // chọn phương thức gửi là get
                dateType:"text", // dữ liệu trả về dạng text
                data : { // Danh sách các thuộc tính sẽ gửi đi
                    phantrang : $('#phantrang').val()
                },
                success : function (result){
                    // Sau khi gửi và kết quả trả về thành công thì gán nội dung trả về
                    // đó vào thẻ div có id = result
                    $('#kq').html(result);
                }
            });
        });
       $("#search").keyup(function (){
           var search=$("#search").val();
           $.ajax({
               url : "{{asset('admin/product/serachproduct')}}", // gửi ajax đến file result.php
               type : "get", // chọn phương thức gửi là get
               dateType:"text", // dữ liệu trả về dạng text
               data : { // Danh sách các thuộc tính sẽ gửi đi
                   search : $('#search').val()
               },
               success : function (result){
                   // Sau khi gửi và kết quả trả về thành công thì gán nội dung trả về
                   // đó vào thẻ div có id = result
                   $('#kq').html(result);
               }
           });
       }) ;
    });
</script>

