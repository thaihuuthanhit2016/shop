@extends('admin.master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Sửa Sản phẩm
                </header>
                <br>
                @include('admin.errors.error')

                <div class="panel-body">
                    <div class="position-center">

                        <form role="form" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                @include('admin.errors.error')
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên Sản phẩm</label>
                                <input type="text" class="form-control" value="{{$product->product_name}}" name="product_name" id="exampleInputEmail1" placeholder="Tên ">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Giá Sản phẩm</label>
                                <input type="text"  class="form-control" value="{{$product->product_price}}" name="product_price" id="price" placeholder="Tên ">
                            </div>
                            <?php
                                $soluong=DB::table('tbl_depot')->where('id_product',$product->id_product)->first();

                            ?>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Số lượng</label>
                                <input type="text" class="form-control" value="{{$soluong->stock}}" name="stock" id="exampleInputEmail1" placeholder="Tên ">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Giá Sản phẩm (KM)</label>
                                <input type="text" class="form-control" value="{{$product->product_price_km}}" name="product_price_km" id="price_km" placeholder="Tên ">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Hình ảnh Sản phẩm</label>
                                <input type="file" class="form-control" name="product_images">
                            </div>
                            <div class="form-group">

                                <img src="{{asset('upload/product/'.$product->product_images)}}" width="50%">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nhóm Hình ảnh Sản phẩm</label>
                                <input type="file" class="form-control" name="product_images_group[]" multiple>
                            </div>
                    <?php
                            $product_images=DB::table('tbl_product_images_group')->where('id_product',$product->id_product)->get();
                            ?>

                            <div class="form-group">
                                @foreach($product_images as $p)
                                <span>

                                <img src="{{asset('upload/product/gallyery/'.$p->images)}}" width="20%"><sub style="color: grey;font-size: 20px"><a href="{{asset('admin/product/product_images/del/'.$p->id_product_images_group)}}"> <i class="fa fa-times-circle-o"></i></a></sub>
</span>
                                @endforeach
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Mô tả Sản phẩm</label>
                                <textarea name="product_desc" class="form-control" rows="8" id="exampleInputPassword1" >{{$product->product_desc}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Tag Sản phẩm</label>
                                <input type="text"  class="form-control" value="{{$product->product_tag}}"  name="product_tags" data-role="tagsinput"
                                       >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Nội dung Sản phẩm</label>
                                <textarea name="product_content" class="form-control" id="product_content" >{{$product->product_content}}</textarea>


                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Danh mục sản phẩm</label>
                                <select class="form-control input-sm m-bot-15" name="category_id">
                                    <option value="0">chon</option>

                                    @foreach($data as $d)
                                        <?php
                                        $c=DB::table('tbl_category_product')->where('id_category_product',$d->id_category_product)->where('category_parent','<>',0)->first();
                                        if ($c!=null){?>
                                        <option value="{{$d->id_category_product}}" @if($d->id_category_product==$product->category_id) selected @endif>{{$d->category_name}}</option>
                                        <?php }?>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Thương hiệu sản phẩm</label>
                                <select class="form-control input-sm m-bot-15" name="brands_id">
                                    <option value="0">chon</option>

                                    @foreach($brands as $d)
                                        <option value="{{$d->id_brands_product}}" @if($d->id_brands_product==$product->brands_id) selected @endif>{{$d->brands_name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Tình trạng</label>
                                <select class="form-control input-sm m-bot-15" name="product_status">
                                    <option value="1" @if($product->product_status==1) selected @endif>Còn hàng</option>

                                    <option value="0" @if($product->product_status==0) selected @endif>Hết hàng</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Quy Cách</label>
                                <select required class="form-control input-sm m-bot-15" id="spec" name="specifications">
                                    <option >Chọn</option>
                                    <option @if($product->product_specifications=="GOI") selected @endif value="GOI">Gói</option>

                                    <option @if($product->product_specifications=="LON") selected @endif value="LON">Lon</option>
                                    <option value="SIZE"@if($product->product_specifications=="SIZE") selected @endif>Size</option>
                                    <option value="THUNG"@if($product->product_specifications=="THUNG") selected @endif>Thùng</option>
                                    <option value="KG"@if($product->product_specifications=="KG") selected @endif>Kg</option>
                                    <option value="HOP"@if($product->product_specifications=="HOP") selected @endif>Hộp</option>
                                    <option value="CAI"@if($product->product_specifications=="CAI") selected @endif>Cái</option>
                                    <option value="TOUR"@if($product->product_specifications=="TOUR") selected @endif>Tour</option>
                                </select>
                            </div>
                            @if($product->product_specifications=="GOI")
                            <div id="goi">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Giá Gói Nhỏ</label>
                                    <input type="text" id="price_small" class="form-control" value="{{$product_spec->price_small}}" name="product_price_small" id="exampleInputEmail1" placeholder="Tên ">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Giá Gói Nhỏ (KM)</label>
                                    <input type="text" id="price_small_km" class="form-control" value="{{$product_spec->price_small_km}}" name="product_price_small_km" id="exampleInputEmail1" placeholder="Tên ">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Giá Gói Lớn</label>
                                    <input type="text" class="form-control" value="{{$product_spec->price_big}}" name="product_price_big" id="exampleInputEmail1" placeholder="Tên ">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Giá Gói Lớn (KM)</label>
                                    <input type="text" class="form-control" value="{{$product_spec->price_big_km}}" name="product_price_big_km" id="exampleInputEmail1" placeholder="Tên ">
                                </div>
                            </div>
                            @endif
                            @if($product->product_specifications=="THUNG")
                                <div id="thung">
                                    <div class="form-group">
                                        <input type="checkbox" id="ck_price_small" @if($product_spec->price_small!=0) checked @endif value="1"  id="exampleInputEmail1" placeholder="Tên ">
                                        <label for="exampleInputEmail1">12 lon</label>

                                        <input type="text" id="text_price_small" class="form-control" value="{{$product_spec->price_small}}" name="product_price_small_thung" id="exampleInputEmail1">
                                    </div>

                                    <div class="form-group">
                                        <input type="checkbox" id="ck_price_small_km" @if($product_spec->price_small_km!=0) checked @endif value="1"  id="exampleInputEmail1" placeholder="Tên ">
                                        <label for="exampleInputEmail1">12 lon (KM)</label>
                                        <input type="text" id="text_price_small_km" class="form-control" value="{{$product_spec->price_small_km}}" name="product_price_small_km_thung" id="exampleInputEmail1" placeholder="Tên ">
                                    </div>
                                    <div class="form-group">
                                        <input type="checkbox" id="ck_price_big" value="1" @if($product_spec->price_big!=0) checked @endif  id="exampleInputEmail1" placeholder="Tên ">

                                        <label for="exampleInputEmail1">24 lon</label>
                                        <input type="text" class="form-control" value="{{$product_spec->price_big}}" name="product_price_big" id="text_price_big_thung"  placeholder="Tên ">
                                    </div>

                                    <div class="form-group">
                                        <input type="checkbox" id="ck_price_big_km" value="1" @if($product_spec->price_big_km!=0) checked @endif   id="exampleInputEmail1" placeholder="Tên ">

                                        <label for="exampleInputEmail1">24 lon (KM)</label>
                                        <input type="text" class="form-control" name="product_price_big_km" value="{{$product_spec->price_big_km}}" id="text_price_big_thung_km" placeholder="Tên ">
                                    </div>
                                </div>
                            @endif
                            <button type="submit" class="btn btn-info">Submit</button>
                        </form>
                    </div>

                </div>
            </section>

        </div>

    </div>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        if($item->parent_id==$parent_id){
            echo '<option value="'.$item->id_category_product.'">'.$char.$item->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript" src="{{asset('public/admin/js/bootstrap-tagsinput.js')}}"></script>
<script>
    $(document).ready(function () {
        $("#size").addClass('none');
        $('#spec').change(function () {
            var g=$('#spec').val();

            if(g=="GOI"){
                $("#goi").removeClass('none')
                $("#size").addClass('none')
            }
            if(g=="SIZE"){
                $("#goi").addClass('none')
                $("#size").removeClass('none')
            }

        });

        $("#price_small").keyup(function () {
            var price_small=$("#price_small").val();
            $("#price").val(price_small);

        });
        $("#price_small_km").keyup(function () {
            var price_small_km=$("#price_small_km").val();
            $("#price_km").val(price_small_km);

        });
        $("#price_km").keyup(function () {
            var price_km=$("#price_km").val();
            $("#price_small_km").val(price_km);

        });
        $("#price").keyup(function () {
            var price=$("#price").val();
            $("#price_small").val(price);

        });

    });
</script>
