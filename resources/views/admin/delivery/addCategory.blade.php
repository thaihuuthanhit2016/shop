@extends('admin.master')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Thêm Phí vận chuyển
            </header>
            <div class="panel-body">
                <div class="position-center">

                    <form role="form" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            @include('admin.errors.error')
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Chọn Tỉnh/Thành Phố</label>
                            <select class="form-control input-sm m-bot-15 choose city" id="city" name="category_id">
                                <option value="">Chọn Tỉnh/Thành Phố</option>
@foreach($city as $c)
                                    <option value="{{$c->matp}}">{{$c->name_city}}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Chọn Quận Huyện</label>
                            <select class="form-control input-sm m-bot-15 choose province" id="province" name="category_id">
                                <option value="">Chọn Quận Huyện</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Chọn Xã Phường</label>
                            <select class="form-control input-sm m-bot-15 wards" id="wards" name="category_id">
                                <option value="">Chọn Xã Phường</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Phí vận chuyển</label>
                            <input type="number" class="form-control input-sm m-bot-15 fee_ship" name="fee_ship">

                        </div>
                        <button type="button" class="add_delivery btn btn-info">Submit</button>
                    </form>
                </div>

            </div>

            <div id="load_delivery">s</div>

        </section>
    </div>
</div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
$(document).ready(function (){
    fetch_delivery();
    $(document).on('blur','.fee',function (){
        var feeship_id=$(this).data('feeship_id');
        var fee_value=$(this).text();

        $.ajax({
            url:"{{asset('admin/delivery/edit')}}",
            method:'Get',
            data:{feeship_id:feeship_id,fee_value:fee_value},
            success:function (data){

                fetch_delivery();
            }
        });

    });
    function  fetch_delivery(){

        $.ajax({
            url:"{{asset('admin/delivery/listall')}}",
            method:'Get',
            success:function (data){

                $('#load_delivery').html(data);
            }
        });

    }
    $(".add_delivery").click(function (){
        var city =$('.city').val();
        var province =$('.province').val();
        var wards =$('.wards').val();
        var fee_ship =$('.fee_ship').val();

        $.ajax({
            url:"{{asset('admin/delivery/add')}}",
            method:'Get',
            data:{city:city,province:province,wards:wards,fee_ship:fee_ship},
            success:function (data){

                $('#'+result).html(data);
            }
        });
    });
    $(".choose").change(function (){
        var action=$(this).attr('id');
        var ma_id=$(this).val();
        var ma_tp=$(this).val();
        var result='';

        if(action=='city'){
            result="province";
        }else {
            result="wards";
        }

        $.ajax({
            url:"{{asset('admin/delivery/province')}}",
            method:'Get',
            data:{ma_id:ma_id,ma_tp:ma_tp,action:action},
            success:function (data){

                $('#'+result).html(data);
            }
        });
    });});

</script>
