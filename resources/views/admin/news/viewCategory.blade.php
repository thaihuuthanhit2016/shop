@extends('admin.master');
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Chi tiết Sản phẩm
                </header>
                <br>
                @include('admin.errors.error')

                <div class="panel-body">
                    <div class="position-center">

                        <form role="form" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                @include('admin.errors.error')
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên Sản phẩm:<i>{{$product->product_name}}</i></label>

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mã Sản phẩm:{{$product->code_product}} </label>

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Giá Sản phẩm:{{$product->product_price}} VNĐ</label>

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Giá Sản phẩm (KM):{{$product->product_price_km}} VNĐ</label>

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Hình ảnh Sản phẩm</label>
                                <img src="{{asset('upload/product/'.$product->product_images)}}" width="50%">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nhóm Hình ảnh Sản phẩm</label>

                                <?php
                                $product_images=DB::table('tbl_product_images_group')->where('id_product',$product->id_product)->get();
                                ?>

                                <div class="form-group">
                                    @foreach($product_images as $p)
                                        <span>

                                <img src="{{asset('upload/product/gallyery/'.$p->images)}}" width="20%"><sub style="color: grey;font-size: 20px">
</span>
                                    @endforeach
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Mô tả Sản phẩm:{{$product->product_desc}}</label>

                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Nội dung Sản phẩm: {!! $product->product_content !!}</label>


                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Danh mục sản phẩm:
                                        @foreach($data as $d)
                                            @if($d->id_category_product==$product->category_id) {{$d->category_name}} @endif
                                        @endforeach
                                    </label>

                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Thương hiệu sản phẩm:
                                        @foreach($brands as $d)
                                            @if($d->id_brands_product==$product->brands_id) {{$d->brands_name}} @endif
                                        @endforeach
                                    </label>
                            </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>

        </div>

    </div>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        if($item->parent_id==$parent_id){
            echo '<option value="'.$item->id_category_product.'">'.$char.$item->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>
