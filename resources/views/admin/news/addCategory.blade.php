@extends('admin.master');
@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Thêm Tin tức
            </header>
            <div class="panel-body">
                <div class="position-center">

                    <form role="form" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            @include('admin.errors.error')
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên Tin tức</label>
                            <input type="text" class="form-control" value="@if(Session::get('product_name')!=null) {{Session::get('product_name')}} @endif" name="product_name" id="exampleInputEmail1" placeholder="Tên ">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Hình ảnh Tin tức</label>
                            <input type="file" class="form-control" name="product_images">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Mô tả Tin tức</label>
                            <textarea name="product_desc" class="form-control" id="exampleInputPassword1" >@if(Session::get('product_desc')!=null) {{Session::get('product_desc')}} @endif</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Nội dung Tin tức</label>
                            <textarea name="product_content" class="form-control" id="product_content" >@if(Session::get('product_content')!=null) {{Session::get('product_content')}} @endif</textarea>


                            <script>
                                CKEDITOR.replace( 'product_content',
                                    {
                                        filebrowserBrowseUrl : '{{asset("public/admin/ckfinder/ckfinder.html")}}',
                                        filebrowserImageBrowseUrl : '{{asset("public/admin/ckfinder/ckfinder.html?type=Images")}}',
                                        filebrowserFlashBrowseUrl : '{{asset("public/admin/ckfinder/ckfinder.html?type=Flash")}}',
                                        filebrowserUploadUrl : '{{asset("public/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files")}}',
                                        filebrowserImageUploadUrl : '{{asset("public/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images")}}',
                                        filebrowserFlashUploadUrl : '{{asset("public/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash")}}'
                                    });
                            </script>

                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Danh mục Tin tức</label>
                            <select class="form-control input-sm m-bot-15" name="category_id">
                                <option value="0">chon</option>

                            @foreach($data as $c)
                                <option value="{{$c->id_category_news}}">{{$c->category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-info">Submit</button>
                    </form>
                </div>

            </div>
        </section>

    </div>

</div>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        if($item->parent_id==$parent_id){
            echo '<option value="'.$item->id_category_product.'">'.$char.$item->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>
