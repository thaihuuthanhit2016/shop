@extends('admin.master');
@section('content')
    <div class="table-agile-info">
        <div class="panel panel-default">
            <div class="panel-heading">
                Danh sách Rút tiền
            </div>
            <div class="row w3-res-tb">
                <div class="col-sm-5 m-b-xs">
                    <select class="input-sm form-control w-sm inline v-middle">
                        <option value="0">Bulk action</option>
                        <option value="1">Delete selected</option>
                        <option value="2">Bulk edit</option>
                        <option value="3">Export</option>
                    </select>
                    <button class="btn btn-sm btn-default">Apply</button>
                </div>
                <div class="col-sm-4">
                </div>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input type="text" class="input-sm form-control" placeholder="Search">
                        <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button">Go!</button>
          </span>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                @include('admin.errors.error')
<?php
                Session::put('message_error','');
                Session::put('product_code',"");
                Session::put('product_name','');
                Session::put('product_price','');
                Session::put('product_price_km','');
                Session::put('product_desc','');
                Session::put('product_content','');

          ?>      <table class="table table-striped b-t b-light">
                    <thead>
                    <tr>
                        <th style="width:20px;">
                            <label class="i-checks m-b-none">
                                <input type="checkbox"><i></i>
                            </label>
                        </th>

                        <th>Tài khoản</th>
                        <th>Số tiền vnđ</th>
                        <th>Số tiền coin</th>
                        <th>Mã giao dịch</th>

                        <th>Trạng thái</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($shop as $d)

                    <tr>
                        <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                        <td>{{$d->address}}</td>
                        <td>{{number_format($d->amount_vnd,0,',','.')}} VNĐ</td>
                        <td>{{number_format($d->amount_coin,0,',','.')}} BITCOINSJC</td>
                        <td>{{$d->code_transfer}}</td>
                        <td>@if($d->status==0) Chưa duyệt @else Đã duyệt @endif</td>
                        @if(Session::get('admin_id'))
                        <td><a href="{{asset('admin/withraw/edit/'.$d->id_card_user)}}"><i class="fa fa-pencil"></i> </a></td>
                        @endif
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <footer class="panel-footer">
                <div class="row">

                    <div class="col-sm-7 text-right text-center-xs">
                        {{$shop->links('admin.paginate')}}
                    </div>
                </div>
            </footer>
        </div>
    </div>

@endsection
