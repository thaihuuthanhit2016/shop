@extends('admin.master');
@section('content')
    <div class="table-agile-info">
        <div class="panel panel-default">
            <div class="panel-heading">
                Danh sách bình luận
            </div>
            <div class="row w3-res-tb">
                <div class="wrapper">
                    <section class="users">
                        <header>
                            <div class="content">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="box_chat">
                                        <div class="col-md-6">
                                            <div class="img_">
                                                <img src="{{asset('upload/avatar/user.jpg')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">

                                            <p>HƯu tahnh</p>
                                            <span>Active now</span>
                                        </div>
                                        <div class="clearfix"></div>
                                        <br>
                                        <hr style="background: #fe980F;height: 1px;width: 70%;">
                                        <div class="users-list">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>


                        </header>
                    </section>
                </div>
            </div>
        </div>
        @endsection
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script>
            $(document).ready(function (){
                loadUser();
            });

          function loadUser() {
              $.ajax({
                  url: "{{asset('admin/loaduser')}}", // gửi ajax đến file result.php
                  type: "get", // chọn phương thức gửi là get
                  dateType: "text",
                  success: function (result) {
                      // Sau khi gửi và kết quả trả về thành công thì gán nội dung trả về
                      // đó vào thẻ div có id = result
                      $('.users-list').html(result);
                  }

              });
          }
        </script>
