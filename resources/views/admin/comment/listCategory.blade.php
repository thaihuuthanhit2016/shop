@extends('admin.master');
@section('content')
    <div class="table-agile-info">
        <div class="panel panel-default">
            <div class="panel-heading">
                Danh sách bình luận
            </div>
            <div class="row w3-res-tb">
                <div class="col-sm-5 m-b-xs">
                    <select class="input-sm form-control w-sm inline v-middle">
                        <option value="0">Bulk action</option>
                        <option value="1">Delete selected</option>
                        <option value="2">Bulk edit</option>
                        <option value="3">Export</option>
                    </select>
                    <button class="btn btn-sm btn-default">Apply</button>
                </div>
                <div class="col-sm-4">
                </div>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input type="text" class="input-sm form-control" placeholder="Search">
                        <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button">Go!</button>
          </span>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                @include('admin.errors.error')
<?php
                Session::put('message_error','');
                Session::put('product_code',"");
                Session::put('product_name','');
                Session::put('product_price','');
                Session::put('product_price_km','');
                Session::put('product_desc','');
                Session::put('product_content','');

          ?>      <table class="table table-striped b-t b-light">
                    <thead>
                    <tr>
                        <th style="width:20px;">
                            <label class="i-checks m-b-none">
                                <input type="checkbox"><i></i>
                            </label>
                        </th>
                        <th>Tên </th>
                        <th>Nội dung bình luận</th>
                        <th>Tin tức</th>

                        <th style="width:100px;">Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($comment as $d)
                <?php
                $news=DB::table('tbl_news')->where('id_news',$d->comment_news_id)->first();
                ?>
                    <tr>
                        <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                        <td>{{$d->comment_name}}</td>
                        <td>{{$d->comment}}</td>


                        <td>{{$news->product_name}}</td>
                        @if($d->comment_status==1)
                        <td>
                            <a href="{{asset('admin/comment/rejectComment/'.$d->id_comments)}}" class="active" ui-toggle-class=""><i class="fa fa-thumbs-up"style="color:green;" aria-hidden="true"></i>
                            </a>
                        </td>
                        @else
                        <td>
                            <a href="{{asset('admin/comment/approveComment/'.$d->id_comments)}}" class="active" ui-toggle-class=""><i class="fa fa-thumbs-down"style="color:red;" aria-hidden="true"></i>
                            </a>
                        </td>
                        @endif
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <footer class="panel-footer">
                <div class="row">

                    <div class="col-sm-7 text-right text-center-xs">
                        {{$comment->links('admin.paginate')}}
                    </div>
                </div>
            </footer>
        </div>
    </div>

@endsection
