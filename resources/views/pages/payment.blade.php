<!DOCTYPE html>
<html lang="en">
@include('template.header.head_cart')
<body>
@include('template.header.cart')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="{{asset('/')}}">Trang chủ</a></li>
                <li class="active">Thanh toán</li>
            </ol>
        </div>
        <!--/breadcrums-->
        <div class="container">
            <div class="table-responsive cart_info">
                <table class="table table-condensed">
                    <thead>
                    <tr class="cart_menu">
                        <td class="image">Sản phẩm</td>
                        <td class="description"></td>
                        <td class="price">Giá</td>
                        <td class="quantity">Số lượng</td>
                        <td class="total">Tổng tiền</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=0;
                    ?>
                    @foreach($content as $c)
                        <?php
                        $i++;
                        $check_product=DB::table('tbl_product')->where('id_product',$c->id)->first();
                        //                    dd($check_product);
                        ?>
                        <tr>
                            <td class="cart_product">
                                <a href="{{asset('detail/'.$check_product->product_name_slug)}}"><img src="{{asset('upload/product/'.$c->options->image)}}" width="70px" alt=""></a>
                            </td>
                            <td class="cart_description">
                                <h4><a href="">{{$c->name}}</a></h4>
                                <p>Mã sản phẩm: {{$check_product->code_product}}</p>
                            </td>
                            <td class="cart_price">
                                <p>{{number_format($c->price,0,',','.')}} VNĐ</p>
                            </td>
                            <td class="cart_quantity">
                                <form method="post" action="{{asset('updatecart')}}">
                                    @csrf
                                    <div class="cart_quantity_button">
                                        <label>{{$c->qty}}</label>
                                    </div>
                                </form>
                            </td>
                            <td class="cart_total">
                                <p class="cart_total_price result_{{$i}}"><?php $total=($c->price*$c->qty);echo number_format($total,0,',','.');?> VNĐ</p>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <a data-toggle="modal" data-target="#myModal" style="cursor: pointer;color: #FE980F">Mã giảm giá</a>
            <div class="clearfix"></div>
            <br>

            <?php

            if(Session::get('id_offer')!='') {
                $total=round(Cart::subtotal())*1000;
                $percent=Session::get('id_offer')/100;
                $percent=($total*$percent);
                $total_percent=$total-$percent;

            }else{

                $total_percent=Session::get('feeship');
            }

 Session::put('total_percent',$total_percent);?>
        </div>

        <h4>Chọn hình thức thanh toán</h4>
        <br>
        <form method="post" >
            @csrf
            <div class="payment-optionsss" id="check_payment">

                  <span><label>
                  <input type="radio"name="check_option" checked value="3"> BitCoinSJC
                  </label></span>
                &nbsp;&nbsp;&nbsp;&nbsp;
                  <span><label>
                  <input type="radio"name="check_option"  value="1"> Chuyển khoản
                  </label></span>
                &nbsp;&nbsp;&nbsp;&nbsp;
                  <span><label>
                  <input type="radio"name="check_option"  value="4"> Nhận tiền mặt
                  </label></span>
                &nbsp;&nbsp;&nbsp;&nbsp;
                  <span><label>
                  <input type="radio" id='radio_paypal'name="check_option"  value="2"> PayPal
                  </label></span>

                <div class="clearfix"></div><br>
                <div class="col-md-12" id="bitcoin">
                <label><h4>Tạm tính:<span id="fee_payment3" style="color:#FE980F;">
                            <?php
                                $fee_ship=Session::get('fee');
                                $fee_ship_=$fee_ship*800/20000;
                            $total=($c->price*$c->qty*800/20000);
                            ?>
                            <?php echo number_format($total,0,',','.');?> BITCOINSJC</span></h4></label><br>

                    </label><label><h4>Phí ship:<span id="fee_payment3" style="color:#FE980F;">
                            <?php
                                $fee_ship=Session::get('fee');
                                $fee_ship_=$fee_ship*800/20000;

                            ?>
                            {{number_format($fee_ship_,0,',','.')}} BITCOINSJC</span></h4></label><br>
                    <label><h4>Số coin giảm giá:<span id="fee_payment3" style="color:#FE980F;">
                            <?php
                                $magiamgia=Session::get('id_offer');

                                $fee_ship=Session::get('fee');
                                $fee_ship_=$fee_ship*800/20000;
                                $magiamgia=$magiamgia/100;

                                $total_=$total*$magiamgia;
                                $total_=$total_*800/20000;
                                ?>
                                {{number_format($total_,0,',','.')}} BITCOINSJC</span></h4></label><br>


                    <label><h4>Thành tiền: <span id="total_payment3" style="color:#FE980F;">
                                                       <?php
                                $total=Session::get('total_percent');
                                $coin=40;


                                $vnd_to_usd=$total/23000;
                                $total_coin=($total*800)/20000;
                                echo number_format($total_coin,0,',','.');
                                ?> BITCOINSJC</span></h4></label><br>
                </div>
                <div class="col-md-12 none" id="ck_tm">
                    <label><h4>Tạm tính:<span id="fee_payment3" style="color:#FE980F;">
                            <?php
                                $fee_ship=Session::get('fee');
                                $fee_ship_=$fee_ship*800/20000;
                                $total=($c->price*$c->qty);
                                ?>
                                <?php echo number_format($total,0,',','.');?> VNĐ</span></h4></label><br>

                    </label>
                <label><h4>Phí ship:<span id="fee_payment3" style="color:#FE980F;">
                            <?php
                            $magiamgia=Session::get('id_offer');

                                $fee_ship=Session::get('fee');
                                $fee_ship_=$fee_ship*800/20000;

                            ?>
                                {{number_format($fee_ship,0,',','.')}} VNĐ</span></h4></label><br>
<label><h4>Số tiền giảm giá:<span id="fee_payment3" style="color:#FE980F;">
                            <?php
                            $magiamgia=Session::get('id_offer');

                                $fee_ship=Session::get('fee');
                                $fee_ship_=$fee_ship*800/20000;
            $magiamgia=$magiamgia/100;

            $total_=$total*$magiamgia;

                            ?>
                                {{number_format($total_,0,',','.')}} VNĐ</span></h4></label><br>

                    <label><h4>Thành tiền:<span id="total_payment3" style="color:#FE980F;">
                                                 <?php
                                $total=Session::get('total_percent');

                                if(Session::get('id_offer')!=''){
                                $magiamgia=$magiamgia/100;
                                $total=$total-$total_+$magiamgia;

                                }else{

                                    $total=$total;

                                }
                                $vnd_to_usd=$total/23000;
                                $total_coin=($total*800)/20000;
                                echo number_format($total,0,',','.');
                                ?> VNĐ</span></h4></label><br>
                </div>
                <div class="clearfix"></div><br>
                <div class="col-md-4">
                    <input class="form-control" type="text" value="{{$check_account->customer_email}}" readonly><br>
                    <input class="form-control" placeholder="số điện thoại" type="text" value="{{$check_account->customer_phone}}"><br>
                    <input class="form-control" placeholder="địa chỉ" type="text" value="{{$check_account->customer_address}}"><br>

                    <textarea name="note" class="form-control" placeholder="Ghi chú đơn hàng ..."></textarea>

                </div>
                <div class="clearfix"></div><br>
            </div>
            <div class="col-md-3">
<a href="{{asset('/')}}" class="btn-primary btn">Tiếp tục mua sắm</a>&nbsp;&nbsp;&nbsp;
            </div>
            <div class="col-md-3">
                <input type="submit" id='dathang'class="btn-primary btn" value="Đặt hàng">
                <?php
                $vnd_to_usd=$total/23000;
                ?>
                <div id="paypal-button" style="margin-top: 16px;" class="none"></div>
                <input type="hidden"  id="vnd_to_usd" value="{{round($vnd_to_usd,2)}}">
            </div>
            <div class="clearfix"></div>
            <br>
        </form>
    </div>
</section>
<!--/#cart_items-->
<!--/#do_action-->
@include('template.footer')
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color:#fe980F;">Mã giảm giá</h4>
            </div>
    @if(count($offer)>0)
            @foreach($offer as $o)
                <?php
                $offer_content=DB::table('tbl_offer')->where('id_offer',$o->id_offer)->first();
                ?>
                <div class="col-md-4">
                    <div class="box" style="margin-bottom: 15px">
                        <div class="row">
                            <form method="post" action="{{asset('doOffer')}}">
                                @csrf
                                <div  class="col-md-12">

                                    <div class="title_1">{{mysubstr($offer_content->content,20)}}<b style="float:right;color:#FE980F;margin-right: 5px">Giảm {{$offer_content->percent}} %</b></div>
                                    <div class="title_1">HSD: <span style="color:red;">Từ ngày {{$offer_content->from_date}} đến ngày {{$offer_content->end_date}}</span></div>
                                    <input class="btn btn-primary"type="submit" style="width: 100%;text-align: center" value="Sử dụng">
                                    <input type="hidden" name="id_offer" value="{{$offer_content->id_offer}}">

                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            @endforeach
    @else



            <div class="col-md-12">
               <p>Hiện tại bạn không có mã giảm giá nào</p>
            </div>

@endif            <div class="clearfix"></div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>


    <style>
        .active {
            color: #FE980F!important;
            border-bottom-color: transparent;
        }
        .none{
            display: none;
        }
        .box{

            box-shadow: 3px -2px 7px #888888;
        }
        .title_1{
            padding-left: 8px;
            padding-top: 5px;
        }
        .modal-header,.modal-footer{
            border-top:none!important;border-bottom:none!important;



        }
    </style>
<script>

    $("#total_payment3").removeClass('none');
    $("#total_payment2").addClass('none');
    $("#total_payment1").addClass('none');
    $("#total_payment4").addClass('none');
    $('#check_payment').on('change', function() {
        var so=$('input[name=check_option]:checked', '#check_payment').val();
        if(so==1){
            $("#total_payment1").removeClass('none');
            $("#total_payment2").addClass('none');
            $("#total_payment3").addClass('none');
            $("#total_payment4").addClass('none');
            $("#dathang").removeClass('none');
            $("#paypal-button").addClass('none');
            $("#bitcoin").addClass('none');
            $("#ck_tm").removeClass('none');

        }
        if(so==2){
            $("#total_payment2").removeClass('none');
            $("#paypal-button").removeClass('none');
            $("#total_payment1").addClass('none');
            $("#total_payment3").addClass('none');
            $("#total_payment4").addClass('none');
            $("#dathang").addClass('none');
            $("#bitcoin").addClass('none');
            $("#ck_tm").removeClass('none');


        }
        if(so==3){
            $("#bitcoin").removeClass('none');
            $("#ck_tm").addClass('none');

            $("#total_payment3").removeClass('none');
            $("#total_payment2").addClass('none');
            $("#total_payment1").addClass('none');
            $("#total_payment4").addClass('none');
            $("#dathang").removeClass('none');
            $("#paypal-button").addClass('none');

        }
        if(so==4){
            $("#total_payment4").removeClass('none');
            $("#total_payment2").addClass('none');
            $("#total_payment3").addClass('none');
            $("#total_payment1").addClass('none');
            $("#dathang").removeClass('none');
            $("#paypal-button").addClass('none');
            $("#bitcoin").addClass('none');
            $("#ck_tm").removeClass('none');


        }
    });

</script>
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>

    <script>
        var usd=document.getElementById('vnd_to_usd');
        paypal.Button.render({
            // Configure environment
            env: 'sandbox',
            client: {
                sandbox: 'AZcIyRwptBsu4DfYfkXz-k_oNWknekcTjs4MgsA7XoBTrsFVta2IH8TVy9GiOCLBWzdgBGHtIUUNuIlg',
                production: 'demo_production_client_id'
            },
            // Customize button (optional)
            locale: 'en_US',
            style: {
                size: 'small',
                color: 'gold',
                shape: 'pill',
            },

            // Enable Pay Now checkout flow (optional)
            commit: true,

            // Set up a payment
            payment: function(data, actions) {
                return actions.payment.create({
                    transactions: [{
                        amount: {
                            total: '$(usd)',
                            currency: 'USD'
                        }
                    }]
                });
            },
            // Execute the payment
            onAuthorize: function(data, actions) {
                return actions.payment.execute().then(function() {
                    // Show a confirmation message to the buyer
                    window.alert('Thank you for your purchase!');
                });
            }
        }, '#paypal-button');

    </script>
