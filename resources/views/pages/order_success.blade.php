<!DOCTYPE html>
<html lang="en">
@include('template.header.head_cart')

<body>
@include('template.header.cart')
<section id="cart_items">
    <div class="container">

            <h4>Cám ơn bạn đã đặt hàng ! Chúng tôi đang xử lý đơn hàng của bạn !</h4>
            <h5>Mã đơn hàng:{{$bill->code_bill}}</h5>
            <a class="btn btn-primary">Tiếp tục mua sắm</a>
        <div class="clearfix"></div>
        <br>
    </div>
</section> <!--/#cart_items-->

@include('template.footer')
