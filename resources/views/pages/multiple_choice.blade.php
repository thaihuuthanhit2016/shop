@extends('layout')
@section('content')
    <div class="features_items"><!--features_items-->
        <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading">Tham gia bài kiểm tra</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 text-right">

                                <button id='btnStart' type="button" class="btn btn-success" name="">Bắt đầu</button>
                            </div>
                        </div>
                        <div id="ten-countdown"class="none" ></div>

                        <form method="post">
                            @csrf

                        <div id="question" class="row" style="margin-left: 10px">
                            </div>
                        </form>

                        <div id="result"></div>
                        </div>
                </div>
            </div>
        </div>
    </div>

@endsection
<style>
    .title-oo{
        padding: 10px;
        color: white;
        background: #FE980F;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<style>
    .none{
        display: none!important;}
</style>
<script>
    $(document).ready(function (){

        $("#btnFinish").click(function (){
            $('#btnFinish').addClass('none');
            $('#btnStart').removeClass('none');
        });
        $("#btnStart").click(function (){
            GetQuestion();
            countdown( "ten-countdown", 1, 0 );

            $('#btnFinish').removeClass('none');
            $('#ten-countdown').removeClass('none');
            $('#btnStart').addClass('none');

        });
        $('#btnFinish').addClass('none');

    });

    function CheckResult(){
       $('#question div h5').each(function (k,v){
           let id=$(v).attr('id');
           console.log(id);
       })
    }
    function GetQuestion(){
        $.ajax({
            url:'{{asset("loadquestion")}}',
            type:'get',
            success:function (data){
               $("#question").html(data);
            }
        });
    }
    function countdown( elementName, minutes, seconds )
    {
        var element, endTime, hours, mins, msLeft, time;

        function twoDigits( n )
        {
            return (n <= 9 ? "0" + n : n);
        }

        function updateTimer()
        {
            msLeft = endTime - (+new Date);
            if ( msLeft < 1000 ) {
                $.ajax({
                    url : "{{asset('multiple_choices')}}", // gửi ajax đến file result.php
                    type : "get", // chọn phương thức gửi là get
                    dateType:"text", // dữ liệu trả về dạng text
                    data : { // Danh sách các thuộc tính sẽ gửi đi
                        cauhoi_1 : $('#cauhoi_1').val(),
                        cauhoi_2 : $('#cauhoi_2').val(),
                        cauhoi_3 : $('#cauhoi_3').val(),
                        cauhoi_4 : $('#cauhoi_4').val(),
                        cauhoi_5 : $('#cauhoi_5').val(),
                        cauhoi_6 : $('#cauhoi_6').val(),
                        cauhoi_7 : $('#cauhoi_7').val(),
                        cauhoi_8 : $('#cauhoi_8').val(),
                        cauhoi_9: $('#cauhoi_9').val(),
                        cauhoi_10 : $('#cauhoi_10').val(),

                        group_1 : $('input[name="group_1"]:checked').val(),
                        group_2 : $('input[name="group_2"]:checked').val(),
                        group_3 : $('input[name="group_3"]:checked').val(),
                        group_4 : $('input[name="group_4"]:checked').val(),
                        group_5 : $('input[name="group_5"]:checked').val(),
                        group_6 : $('input[name="group_6"]:checked').val(),
                        group_7 : $('input[name="group_7"]:checked').val(),
                        group_8 : $('input[name="group_8"]:checked').val(),
                        group_9 : $('input[name="group_9"]:checked').val(),
                        group_10 : $('input[name="group_10"]:checked').val(),
                    },
                    success : function (result){
                        // Sau khi gửi và kết quả trả về thành công thì gán nội dung trả về
                        // đó vào thẻ div có id = result
             window.location.href='http://localhost/shopbitcoin/view-result'
                    }
                });
            } else {
                time = new Date( msLeft );
                hours = time.getUTCHours();
                mins = time.getUTCMinutes();
                element.innerHTML = (hours ? hours + ':' + twoDigits( mins ) : mins) + ':' + twoDigits( time.getUTCSeconds() );
                setTimeout( updateTimer, time.getUTCMilliseconds() + 500 );
            }
        }

        element = document.getElementById( elementName );
        endTime = (+new Date) + 1000 * (60*minutes + seconds) + 500;
        updateTimer();
    }

</script>
<style>
    div #ten-countdown{
        /* text-align: center; */
        border: 5px solid #004853;
        display:inline;
        padding: 5px;
        color: #004853;
        font-family: Verdana, sans-serif, Arial;
        font-size: 40px;
        font-weight: bold;
        text-decoration: none;

        position: fixed;
        top: 76px;
        right: 90px;
    }

</style>
