<!DOCTYPE html>
<html lang="en">
@include('template.header.head_cart')

<body>
@include('template.header.cart')
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
@include('pages.left_menu')
            </div>

            <div class="col-sm-9">
                    <h3 class="title">DANH SÁCH SẢN PHẨM ĐÃ XEM</h3>
                <br>
                <h4>Có <b style="color:#FE980F;">{{count($product_viewed)}}</b> sản phẩm đã xem</h4>
                <div class="table-responsive cart_info">
                    <table class="table table-condensed" border="1">
                        <thead>
                        <?php
                        $addressvi="lGAJDaThG28Y7UtGTU7jJ1apRLczA2PfdUUSw1";
                        ?>
                            @foreach($product_viewed as $d_b)

                                <?Php
                                $product=DB::table('tbl_product')->where('id_product',$d_b->id_product)->first();
                                ?>
                        <tr class="cart_menu">
                            <td class="image" style="width: 100px;">Ảnh Sản phẩm:<br><b><img src="{{asset('upload/product/'.$product->product_images)}}" width="100px"></b></td>
                            <td class="image" style="width: 100px;">Mã Sản phẩm:<br><b> {{$product->code_product}}</b></td>

                            <td class="price" style="width: 120px">Tên sản phẩm: <br> <b>{{$product->product_name}}@if($product->product_price_km!=0)<sup style="color: #FE980F">Sale</sup>@endif</b></td>
                            <td class="quantity" style="width: 40px">Giá:<br><b> @if($product->product_price_km!=0){{number_format($product->product_price_km,0,',','.')}} @else {{number_format($product->product_price,0,',','.')}} @endif VNĐ</b></td>
                            <td class="total" style="width: 80px;"> <a style="color:#FE980F;" href="{{asset('add-to-cart/'.$product->id_product)}}">Mua ngay</a></td>


                        </tr>

                            @endforeach
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@include('template.footer')
<style>
    .active{
        color: #FE980F!important;
    }
    td{
        text-align: center;
    }
</style>

<script>
    $(document).ready(function (){
        $('#viewed_producut').addClass('active');
    });
</script>
