<!DOCTYPE html>
<html lang="en">
@include('template.header.head_cart')

<body>
@include('template.header.cart')
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Quản lý giao dịch</h2>
                    <div class="panel-group category-products" id="accordian"><!--category-productsr-->

                        <div class="panel panel-default">

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title" style="margin-bottom: 10px"><a href="{{asset('order/')}}"><i class="fa fa-list-alt" style="color:#FE980F;"></i> &nbsp;&nbsp;Đơn hàng</a></h4>
                                        <h4 class="panel-title" style="margin-bottom: 10px"><a href="{{asset('wallet/')}}"><i class="fa fa-user"  style="color:#FE980F;"></i>&nbsp;&nbsp;&nbsp;Ví BitConSJC</a></h4>
                                        <h4 class="panel-title" style="margin-bottom: 10px"><a href="{{asset('wishlist/')}}"><i class="fa fa-heart"  style="color:#FE980F;"></i>&nbsp;&nbsp;Sản Phẩm yêu thích</a></h4>
                                    </div>
                                </div>


                        </div>

                    </div>
                    <h2>Quản lý tài khoản</h2>
                    <div class="panel-group category-products" id="accordian"><!--category-productsr-->

                        <div class="panel panel-default">

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title " style="margin-bottom: 10px"><a href="{{asset('account/')}}" class="active"><i class="fa fa-edit" style="color:#FE980F;"></i> &nbsp;&nbsp;Thông tin tài khoản</a></h4>
                                        <h4 class="panel-title" style="margin-bottom: 10px"><a href="{{asset('offer')}}"><i class="fa fa-user"  style="color:#FE980F;"></i>&nbsp;&nbsp;&nbsp;Ưu đãi của tôi</a></h4>
                                    </div>
                                </div>


                        </div>

                    </div><!--/category-products-->
                </div>
            </div>

            <div class="col-sm-6">
                    <h3 class="title">THAY ĐỔI MẬT KHẨU</h3>
                <br>
                <div class="clearfix"></div>
                @include('admin.errors.error')
                <div class="clearfix"></div>
                <br>
                <form method="post" >
                    @csrf
                    <div class="form-group">
                        <input readonly class="form-control" value="{{$account->customer_email}}" >
                        <input type="hidden" class="form-control"name="id_customer" value="{{$account->id_customer}}" >
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="c_pass" placeholder="Mật khẩu hiện tại">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="n_pass" placeholder="Mật khẩu mới" >
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="r_pass" placeholder="Nhập lạimật khẩu mới" >
                    </div>

                <div class="form-group">
                    <input type="submit" class="btn-primary btn" value="Cập nhật" >
                </div>
            </form>

            </div>
        </div>
    </div>
</section>

@include('template.footer')
<style>
    .active{
        color: #FE980F!important;
    }
</style>
