@extends('layout')
@section('content')
@section('title',$brands_id->brands_name)
    <div class="features_items"><!--features_items-->
        <h2 class="title text-center">Thương hiệu {{$brands_id->brands_name}}</h2>
        @foreach($brands_product as $p_n)
            <?php
            $pro=DB::table('tbl_product_shop')->where('id_product',$p_n->id_product)->first();

            $pro_shop=DB::table('tbl_shop')->where('id_shop_user',$pro->id_shop)->first();

            ?>
        <div class="col-sm-4">
            <div class="product-image-wrapper">
                <div class="single-products">
                    <div class="productinfo text-center">
                        <a href="{{asset('detail/'.$p_n->product_name_slug)}}" ><img src="{{asset('upload/product/'.$p_n->product_images)}}" alt="" />
                            @if($p_n->product_price_km!=0)
                                <h4 style="    text-decoration: line-through;color: #ccc">{{number_format($p_n->product_price,0,',','.')}} VNĐ</h4>
                                <h2 style="color:#FE980F;">{{number_format($p_n->product_price_km,0,',','.')}} VNĐ <sup style="color:red;font-size: 15px">-<?php
                                        echo 100-ceil($p_n->product_price_km*100/$p_n->product_price).'%';
                                        ?></sup></h2>
                            @else
                                <div class="price_pro1">

                                <h2 style="color:#FE980F;">{{number_format($p_n->product_price,0,',','.')}} VNĐ</h2>
                                </div>
                            @endif
                            <p>{{mysubstr($p_n->product_name,60)}}</p>
                        </a>
                        <div class="shop_info"><a href="{{asset('shop/'.$pro_shop->id_shop_user)}}"> <i class="fa fa-bank"></i> <b>{{$pro_shop->name_shop}}</b></a></div>

                        <a href="{{asset('add-to-cart/'.$p_n->id_product)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ hàng</a>
                    </div>
                </div>
            </div>

            <div class="choose">
                <ul class="nav nav-pills nav-justified">
                    <li><a href="#"><i class="fa fa-plus-square"></i>Thêm vào yêu thích</a></li>
                    <li><a href="#"><i class="fa fa-plus-square"></i>Thêm vào so sánh</a></li>
                </ul>
            </div>
        </div>
        @endforeach
    </div><!--features_items-->

{{--    <div class="category-tab"><!--category-tab-->--}}
{{--        <div class="col-sm-12">--}}
{{--            <ul class="nav nav-tabs">--}}
{{--                <li class="active"><a href="#tshirt" data-toggle="tab">T-Shirt</a></li>--}}
{{--                <li><a href="#blazers" data-toggle="tab">Blazers</a></li>--}}
{{--                <li><a href="#sunglass" data-toggle="tab">Sunglass</a></li>--}}
{{--                <li><a href="#kids" data-toggle="tab">Kids</a></li>--}}
{{--                <li><a href="#poloshirt" data-toggle="tab">Polo shirt</a></li>--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--        <div class="tab-content">--}}
{{--            <div class="tab-pane fade active in" id="tshirt" >--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery1.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery2.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery3.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery4.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="tab-pane fade" id="blazers" >--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery4.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery3.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery2.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery1.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="tab-pane fade" id="sunglass" >--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery3.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery4.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery1.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery2.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="tab-pane fade" id="kids" >--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery1.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery2.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery3.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery4.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="tab-pane fade" id="poloshirt" >--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery2.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery4.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery3.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery1.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div><!--/category-tab-->--}}

{{--    <div class="recommended_items"><!--recommended_items-->--}}
{{--        <h2 class="title text-center">recommended items</h2>--}}

{{--        <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">--}}
{{--            <div class="carousel-inner">--}}
{{--                <div class="item active">--}}
{{--                    <div class="col-sm-4">--}}
{{--                        <div class="product-image-wrapper">--}}
{{--                            <div class="single-products">--}}
{{--                                <div class="productinfo text-center">--}}
{{--                                    <img src="{{asset('public/Eshopper/images/home/recommend1.jpg')}}" alt="" />--}}
{{--                                    <h2>$56</h2>--}}
{{--                                    <p>Easy Polo Black Edition</p>--}}
{{--                                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-4">--}}
{{--                        <div class="product-image-wrapper">--}}
{{--                            <div class="single-products">--}}
{{--                                <div class="productinfo text-center">--}}
{{--                                    <img src="{{asset('public/Eshopper/images/home/recommend2.jpg')}}" alt="" />--}}
{{--                                    <h2>$56</h2>--}}
{{--                                    <p>Easy Polo Black Edition</p>--}}
{{--                                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-4">--}}
{{--                        <div class="product-image-wrapper">--}}
{{--                            <div class="single-products">--}}
{{--                                <div class="productinfo text-center">--}}
{{--                                    <img src="{{asset('public/Eshopper/images/home/recommend3.jpg')}}" alt="" />--}}
{{--                                    <h2>$56</h2>--}}
{{--                                    <p>Easy Polo Black Edition</p>--}}
{{--                                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="item">--}}
{{--                    <div class="col-sm-4">--}}
{{--                        <div class="product-image-wrapper">--}}
{{--                            <div class="single-products">--}}
{{--                                <div class="productinfo text-center">--}}
{{--                                    <img src="{{asset('public/Eshopper/images/home/recommend1.jpg')}}" alt="" />--}}
{{--                                    <h2>$56</h2>--}}
{{--                                    <p>Easy Polo Black Edition</p>--}}
{{--                                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-4">--}}
{{--                        <div class="product-image-wrapper">--}}
{{--                            <div class="single-products">--}}
{{--                                <div class="productinfo text-center">--}}
{{--                                    <img src="{{asset('public/Eshopper/images/home/recommend2.jpg')}}" alt="" />--}}
{{--                                    <h2>$56</h2>--}}
{{--                                    <p>Easy Polo Black Edition</p>--}}
{{--                                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-4">--}}
{{--                        <div class="product-image-wrapper">--}}
{{--                            <div class="single-products">--}}
{{--                                <div class="productinfo text-center">--}}
{{--                                    <img src="{{asset('public/Eshopper/images/home/recommend3.jpg')}}" alt="" />--}}
{{--                                    <h2>$56</h2>--}}
{{--                                    <p>Easy Polo Black Edition</p>--}}
{{--                                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">--}}
{{--                <i class="fa fa-angle-left"></i>--}}
{{--            </a>--}}
{{--            <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">--}}
{{--                <i class="fa fa-angle-right"></i>--}}
{{--            </a>--}}
{{--        </div>--}}
{{--    </div><!--/recommended_items-->--}}



@endsection
