@extends('layout')
@section('content')
    <div class="features_items"><!--features_items-->
        <div id="calendar"></div>

    <link href="{{asset('public/Eshopper/css/main.min.css')}}" rel='stylesheet' />
    <script src="{{asset('public/Eshopper/js/main.min.js')}}"></script>

    <script>

        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {
                selectable: true,
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,timeGridDay'
                },
                events:"{{asset('test')}}",
                select: function(info) {
                    alert('selected ' + info.startStr);
                }
            });

            calendar.render();
        });

    </script>
@endsection
