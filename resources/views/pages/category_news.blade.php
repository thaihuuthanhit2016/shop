@extends('layout_news')
@section('content')
    <div class="features_items"><!--features_items-->
        <h2 class="title text-center">Danh mục {{$category_news->category_name}}</h2>
        @foreach($category_product as $p_n)
            <?php

            ?>

                <div class="col-sm-12">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo ">
                                <a href="{{asset('detail-news/'.$p_n->product_name_slug)}}" >
                                    <div class="col-md-3">
                                        <img src="{{asset('upload/tintuc/'.$p_n->product_images)}}" alt=""width="100%" />
                                    </div>
                                    <div class="col-md-9">
                                        <p style="font-weight:bold;margin-top: 10px;">{{mysubstr($p_n->product_name,180)}}</p>
                                        <span style="color: black">{{mysubstr($p_n->product_desc,200) }}</span>
                                    </div>
                                </a>

                            </div>
                        </div>
                    </div>

                </div>
            <hr style="background: #FE980F;width: 100%;height: 1px;">

        @endforeach
    </div><!--features_items-->

    {{$category_product->links('admin.paginate')}}
{{--    <div class="category-tab"><!--category-tab-->--}}
{{--        <div class="col-sm-12">--}}
{{--            <ul class="nav nav-tabs">--}}
{{--                <li class="active"><a href="#tshirt" data-toggle="tab">T-Shirt</a></li>--}}
{{--                <li><a href="#blazers" data-toggle="tab">Blazers</a></li>--}}
{{--                <li><a href="#sunglass" data-toggle="tab">Sunglass</a></li>--}}
{{--                <li><a href="#kids" data-toggle="tab">Kids</a></li>--}}
{{--                <li><a href="#poloshirt" data-toggle="tab">Polo shirt</a></li>--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--        <div class="tab-content">--}}
{{--            <div class="tab-pane fade active in" id="tshirt" >--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery1.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery2.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery3.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery4.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="tab-pane fade" id="blazers" >--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery4.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery3.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery2.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery1.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="tab-pane fade" id="sunglass" >--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery3.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery4.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery1.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery2.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="tab-pane fade" id="kids" >--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery1.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery2.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery3.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery4.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="tab-pane fade" id="poloshirt" >--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery2.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery4.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery3.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-3">--}}
{{--                    <div class="product-image-wrapper">--}}
{{--                        <div class="single-products">--}}
{{--                            <div class="productinfo text-center">--}}
{{--                                <img src="{{asset('public/Eshopper/images/home/gallery1.jpg')}}" alt="" />--}}
{{--                                <h2>$56</h2>--}}
{{--                                <p>Easy Polo Black Edition</p>--}}
{{--                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div><!--/category-tab-->--}}

{{--    <div class="recommended_items"><!--recommended_items-->--}}
{{--        <h2 class="title text-center">recommended items</h2>--}}

{{--        <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">--}}
{{--            <div class="carousel-inner">--}}
{{--                <div class="item active">--}}
{{--                    <div class="col-sm-4">--}}
{{--                        <div class="product-image-wrapper">--}}
{{--                            <div class="single-products">--}}
{{--                                <div class="productinfo text-center">--}}
{{--                                    <img src="{{asset('public/Eshopper/images/home/recommend1.jpg')}}" alt="" />--}}
{{--                                    <h2>$56</h2>--}}
{{--                                    <p>Easy Polo Black Edition</p>--}}
{{--                                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-4">--}}
{{--                        <div class="product-image-wrapper">--}}
{{--                            <div class="single-products">--}}
{{--                                <div class="productinfo text-center">--}}
{{--                                    <img src="{{asset('public/Eshopper/images/home/recommend2.jpg')}}" alt="" />--}}
{{--                                    <h2>$56</h2>--}}
{{--                                    <p>Easy Polo Black Edition</p>--}}
{{--                                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-4">--}}
{{--                        <div class="product-image-wrapper">--}}
{{--                            <div class="single-products">--}}
{{--                                <div class="productinfo text-center">--}}
{{--                                    <img src="{{asset('public/Eshopper/images/home/recommend3.jpg')}}" alt="" />--}}
{{--                                    <h2>$56</h2>--}}
{{--                                    <p>Easy Polo Black Edition</p>--}}
{{--                                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="item">--}}
{{--                    <div class="col-sm-4">--}}
{{--                        <div class="product-image-wrapper">--}}
{{--                            <div class="single-products">--}}
{{--                                <div class="productinfo text-center">--}}
{{--                                    <img src="{{asset('public/Eshopper/images/home/recommend1.jpg')}}" alt="" />--}}
{{--                                    <h2>$56</h2>--}}
{{--                                    <p>Easy Polo Black Edition</p>--}}
{{--                                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-4">--}}
{{--                        <div class="product-image-wrapper">--}}
{{--                            <div class="single-products">--}}
{{--                                <div class="productinfo text-center">--}}
{{--                                    <img src="{{asset('public/Eshopper/images/home/recommend2.jpg')}}" alt="" />--}}
{{--                                    <h2>$56</h2>--}}
{{--                                    <p>Easy Polo Black Edition</p>--}}
{{--                                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-4">--}}
{{--                        <div class="product-image-wrapper">--}}
{{--                            <div class="single-products">--}}
{{--                                <div class="productinfo text-center">--}}
{{--                                    <img src="{{asset('public/Eshopper/images/home/recommend3.jpg')}}" alt="" />--}}
{{--                                    <h2>$56</h2>--}}
{{--                                    <p>Easy Polo Black Edition</p>--}}
{{--                                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">--}}
{{--                <i class="fa fa-angle-left"></i>--}}
{{--            </a>--}}
{{--            <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">--}}
{{--                <i class="fa fa-angle-right"></i>--}}
{{--            </a>--}}
{{--        </div>--}}
{{--    </div><!--/recommended_items-->--}}



@endsection
