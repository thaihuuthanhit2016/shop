<!DOCTYPE html>
<html lang="en">
@include('template.header.head_cart')

<body>
@include('template.header.cart')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="{{asset('/')}}">Trang chủ</a></li>
                <li class="active">Giỏ hàng</li>
            </ol>
        </div>
        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                <tr class="cart_menu">
                    <td class="image">Sản phẩm</td>
                    <td class="description"></td>
                    <td class="price">Giá</td>
                    <td class="quantity">Số lượng</td>
                    <td class="total">Tổng tiền</td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                <?php
                $i=0;
                ?>
                @foreach($content as $c)
                    <?php
                        $i++;
                    $check_product=DB::table('tbl_product')->where('id_product',$c->id)->first();
                    $depot=DB::table('tbl_depot')->where('id_product',$c->id)->first();

                    ?>
                    <tr>
                        <td class="cart_product">
                            <a href="{{asset('detail/'.$check_product->product_name_slug)}}"><img src="{{asset('upload/product/'.$c->options->image)}}" width="70px" alt=""></a>
                        </td>
                        <td class="cart_description">
                            <h4><a href="">{{$c->name}}</a></h4>
                            <p>Mã sản phẩm: {{$check_product->code_product}}</p>
                        </td>
                        <td class="cart_price">
                            <p>{{number_format($c->price,0,',','.')}} VNĐ</p>
                        </td>
                        <td class="cart_quantity">
                            <form method="post" action="{{asset('updatecart')}}">
                                @csrf
                                <div class="cart_quantity_button">

                                    <input  class="cart_quantity_input qty_{{$i}}" type="number" style="
    height: 32px;" name="quantity" value="{{$c->qty}}" autocomplete="off" min="1" max="{{$depot->stock}}" size="2">&nbsp;&nbsp;
                                    <input  class="cart_quantity_input id_product_{{$i}}" type="hidden" name="id" value="{{$c->rowId}}"  min="1" size="2">
                                    <input  class="btn-primary btn"style="margin-top: 0px" type="submit"  value="Update">

                                </div>
                            </form>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price result_{{$i}}">{{number_format(Session::get('total'),0,',','.')}} VNĐ
                            </p>
                        </td>
                        <td class="cart_delete">
                            <a class="cart_quantity_delete" href="{{asset('delCart/'.$c->rowId)}}"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>

</section> <!--/#cart_items-->

<section id="do_action">
    <div class="container">
        <div class="row">

            <div class="col-sm-6">
                <div class="total_area">
                    <ul>
                        <li>Tạm tính <span id="subtotal">{{Cart::subtotal()}} VNĐ</span></li>

                        <form role="form" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                @include('admin.errors.error')
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Chọn Tỉnh/Thành Phố</label>
                                <select class="form-control input-sm m-bot-15 choose city" id="city" name="category_id">
                                    <option value="">Chọn Tỉnh/Thành Phố</option>
                                    @foreach($city as $c)
                                        <option value="{{$c->matp}}">{{$c->name_city}}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Chọn Quận Huyện</label>
                                <select class="form-control input-sm m-bot-15 choose province" id="province" name="category_id">
                                    <option value="">Chọn Quận Huyện</option>

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Chọn Xã Phường</label>
                                <select class="form-control input-sm m-bot-15 wards" id="wards" name="category_id">
                                    <option value="">Chọn Xã Phường</option>

                                </select>
                            </div>
                            <li>Phí vận chuyển <span style="font-weight: bold" id="data"> 0 VNĐ</span> </li>

                        </form>
                        <li>Thành tiền <span id="total">

                                    {{Cart::subtotal()}} VNĐ</span></li>

                    </ul>
                    <a class="btn btn-default check_out" href="{{asset('payment')}}">Thanh toán </a>
                </div>
            </div>
        </div>
    </div>
</section><!--/#do_action-->

@include('template.footer')
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>

                @foreach($offer as $o)
                    <?php
                    $offer_content=DB::table('tbl_offer')->where('id_offer',$o->id_offer)->first();
                    ?>
                    <div class="col-md-4">
                        <div class="box" style="margin-bottom: 15px">
                            <div class="row">
                                <form method="post" action="{{asset('doOffer')}}">
                                    @csrf
                                <div  class="col-md-12">

                                    <div class="title_1">{{mysubstr($offer_content->content,20)}}<b style="float:right;color:#FE980F;margin-right: 5px">Giảm {{$offer_content->percent}} %</b></div>
                                    <div class="title_1">HSD: <span style="color:red;">Từ ngày {{$offer_content->from_date}} đến ngày {{$offer_content->end_date}}</span></div>
                                    <input class="btn btn-primary"type="submit" style="width: 100%;text-align: center" value="Sử dụng">
                                    <input type="hidden" name="id_offer" value="{{$offer_content->id_offer}}">

                                </div>
                                </form>
                            </div>

                        </div>
                    </div>
                @endforeach
            <div class="clearfix"></div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>


    <style>
        .active {
            color: #FE980F!important;
            border-bottom-color: transparent;
        }
        .none{
            display: none;
        }
        .box{

            box-shadow: 3px -2px 7px #888888;
        }
        .title_1{
            padding-left: 8px;
            padding-top: 5px;
        }
        .modal-header,.modal-footer{
            border-top:none!important;border-bottom:none!important;



        }
    </style>
<script>
    $(document).ready(function (){
    $(".choose").change(function (){
        var action=$(this).attr('id');
        var ma_id=$(this).val();
        var ma_tp=$(this).val();
        var result='';

        if(action=='city'){
            result="province";
        }else {
            result="wards";
        }

        $.ajax({
            url:"{{asset('admin/delivery/province')}}",
            method:'Get',
            data:{ma_id:ma_id,ma_tp:ma_tp,action:action},
            success:function (data){

                $('#'+result).html(data);
            }
        });
    });
    $('#wards').change(function (){
        var wards=$(this).val();

        $.ajax({
            url:"{{asset('admin/delivery/fee')}}",
            method:'Get',
            data:{wards:wards},
            success:function (data){
                var tach=data.split('-');
                $('#data').html(tach[1]);
                $('#total').html(tach[0]);
            }
        });
    });
    });
</script>
