<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
@include('template.header.head_cart')
<body>
@include('template.header.cart')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="{{asset('/')}}">Trang chủ</a></li>
                <li class="active">Thanh toán Card</li>
            </ol>
        </div>
        <!--/breadcrums-->
        <div class="container">
            <div class="col-md-6">
                <h3 style="text-align: center;color: #fe980F">Thông tin chuyển khoản thanh toán</h3>
                <p>Tên Tài khoản:<b style="color:#FE980F;">Thái Hữu Thạnh</b></p>
                <p>Số Tài khoản:<b style="color:#FE980F;">241000729</b></p>
                <p>Tên Ngân hàng :<b style="color:#FE980F;">ACB</b></p>
                <hr style="background: #FE980F;color:#FE980F;width: 100%;height: 1px;">
                <p>Tên Tài khoản:<b style="color:#FE980F;">Thái Hữu Thạnh</b></p>
                <p>Số Tài khoản:<b style="color:#FE980F;">0911000032291</b></p>
                <p>Tên Ngân hàng :<b style="color:#FE980F;">VCB</b></p>
                <p>Nội dung chuyển khoản :<b style="color:#FE980F;">Payment code_transfer: xxxxx
                    </b></p>

            </div>
            <div class="col-md-6">
                <h3 style="text-align: center;color: #fe980F">Thông tin thẻ cào</h3>

                @if(Session::get('message_update')!=null)
                    <p class="alert-success alert">{{Session::get('message_update')}}</p>
                    <?php
                    Session::put('message_update', "");
                    ?>
                @endif
                @if(Session::get('err_update')!=null)
                    <p class="alert-info alert">{{Session::get('err_update')}}</p>
                @endif
                <p class="title_1">Seri: <b style="color:#FE980F;">{{$card->seri}}</b></p>
                <p class="title_1">Code: <b style="color:#FE980F;"> {{$card->code_card}}</b></p>
                <p class="title_1">USD: <b style="color:#FE980F;"> $ {{number_format($card->usd,0,',','.')}}</b></p>
                <p class="title_1">VNĐ: <b style="color:#FE980F;">  {{number_format($card->usd*200000,0,',','.')}} VNĐ</b></p>
                <p class="title_1">Coin:  <b style="color:#FE980F;">{{number_format($card->coin,0,',','.')}}</b></p>
                <p class="title_1">Code transfer:  <b style="color:blue;">@if($card_data!=null){{$card_data->code_transfer}} @else <b style="color: red">None</b>@endif</b></p>
                @if(Session::get('address_url')!=null)
                    <p class="title_1">Address:  <b style="color:black;">{{Session::get('address_url')}}</b></p>
                @else
                    <form method="get" action="{{asset('payment/buycard/'.$card->id_card)}}">
                    <p class="title_1">Address wallet:  <input type="text" name="address_coin" id="address_wallet"  class="form-control">
                        <input type="hidden" name="address_coin_back" id="address_wallet_back"  class="form-control">

                    </p>
                    <p class="title_1" style="cursor: pointer;color:#FE980F " id="check" >Kiểm tra ví</p>

                    <p id="kq" style="color:red;"></p>

                    <p id="kq2" style="color:blue;"></p>
                    <p class="title_1">  <input class="btn btn-primary" id="lll" disabled="true" type="submit" value="Send" ></p>
                    </form>
                @endif
            </div>

            <div class="col-md-12">
                <p style="color: #fe980F;font-size: 15px;font-weight: bold">Lưu ý: Tài khoản của bạn sẽ tự động nạp vào ví coin theo địa chỉ: <b style="color:black;font-size: 18px;"> {{Session::get('address_url')}}</b> sau khi khách hàng đã thanh toán</p>
            </div>
            <?php
                Session::put('address_url','');
            ?>
        </div>
    </div>

</section>
<!--/#cart_items-->
<!--/#do_action-->
@include('template.footer')


    <style>
        .active {
            color: #FE980F!important;
            border-bottom-color: transparent;
        }
        .none{
            display: none;
        }
        .box{

            box-shadow: 3px -2px 7px #888888;
        }
        .title_1{
            padding-left: 8px;
            padding-top: 5px;
        }
        .modal-header,.modal-footer{
            border-top:none!important;border-bottom:none!important;



        }
    </style>
<script>

    $("#total_payment3").removeClass('none');
    $("#total_payment2").addClass('none');
    $("#total_payment1").addClass('none');
    $("#total_payment4").addClass('none');
    $('#check_payment').on('change', function() {
        var so=$('input[name=check_option]:checked', '#check_payment').val();
        if(so==1){
            $("#total_payment1").removeClass('none');
            $("#total_payment2").addClass('none');
            $("#total_payment3").addClass('none');
            $("#total_payment4").addClass('none');
        }
        if(so==2){
            $("#total_payment2").removeClass('none');
            $("#total_payment1").addClass('none');
            $("#total_payment3").addClass('none');
            $("#total_payment4").addClass('none');
        }
        if(so==3){
            $("#total_payment3").removeClass('none');
            $("#total_payment2").addClass('none');
            $("#total_payment1").addClass('none');
            $("#total_payment4").addClass('none');

        }
        if(so==4){
            $("#total_payment4").removeClass('none');
            $("#total_payment2").addClass('none');
            $("#total_payment3").addClass('none');
            $("#total_payment1").addClass('none');
        }
    });
</script>
<script>
    $('#check').click(function (){
    var  address_wallet=$("#address_wallet").val();
    var  coin_deposit=$("#coin_deposit").val();
    if(address_wallet==''){
    var error='<div class="alert alert-danger">You must enter the address BitcoinSJC</div>'
    $('#kq').html(error);
    return false;
    }
    if(coin_deposit==''){
    var error='<div class="alert alert-danger">You must enter the number of coins to deposit</div>'
    $('#kq').html(error);
    return false;
    }
    $.ajax({
    url : "http://localhost/shopbitcoin/apigetallcoin.php?address="+address_wallet, // gửi ajax đến file result.php
    type : "get", // chọn phương thức gửi là get
    dateType:"text", // dữ liệu trả về dạng text

    success : function (result){
    // Sau khi gửi và kết quả trả về thành công thì gán nội dung trả về
    // đó vào thẻ div có id = result
    var tach=result.split('wallet:');
    var tach1=tach[0].split('coin: ');

    var tach2=tach1[1].split('<br />');

    if(tach2[0]!='false'){
        var error='<div class="alert alert-success">Verified your wallet </div>';
        $('#kq').html(error);
        $('#address_wallet_back').html('ok');
        $('#lll').prop('disabled', false);

        var tach=result.split('wallet');
    var tach1=tach[0].split('coin: ');
    var tach2=tach1[1].split('<br />');

    }else{
    var error='<div class="alert alert-danger">Your wallet address does not exist </div>';

    $('#kq').html(error);
    }
    }
    });
    });
</script>
