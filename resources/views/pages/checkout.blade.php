<!DOCTYPE html>
<html lang="en">
@include('template.header.head_cart')
<body>
@include('template.header.cart')
<!--/header-->
<form method="post">
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="{{asset('/')}}">Trang chủ</a></li>
                <li class="active">Thanh toán</li>
            </ol>
        </div><!--/breadcrums-->

        <div class="shopper-informations">
            <div class="row">

                <div class="col-sm-8 clearfix">
                    <div class="bill-to">
                        <p>Thông tin Đơn hàng</p>
                        <div class="form-one">
                            <input class="form-control" type="text" value="{{$check_account->customer_name}}" readonly><br>
                                <input class="form-control" type="text" value="{{$check_account->customer_email}}" readonly><br>
                                <input class="form-control" type="text" value="{{$check_account->customer_phone}}"><br>
                                <input class="form-control" type="text" value="{{$check_account->customer_address}}"><br>
                                <input type="hidden" name="total" value="{{Cart::subtotal()}}">
                                <textarea name="note" class="form-control">Ghi chú đơn hàng</textarea>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="review-payment">
            <h2>Xem lai giỏ hàng và thanh toán</h2>
        </div>
        <section id="cart_items">
            <div class="container">
                <div class="table-responsive cart_info">
                    <table class="table table-condensed">
                        <thead>
                        <tr class="cart_menu">
                            <td class="image">Sản phẩm</td>
                            <td class="description"></td>
                            <td class="price">Giá</td>
                            <td class="quantity">Số lượng</td>
                            <td class="total">Tổng tiền</td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i=0;
                        ?>
                        @foreach($content as $c)
                            <?php
                            $i++;
                            $check_product=DB::table('tbl_product')->where('id_product',$c->id)->first();
                            //                    dd($check_product);
                            ?>
                            <tr>
                                <td class="cart_product">
                                    <a href="{{asset('detail/'.$check_product->product_name_slug)}}"><img src="{{asset('upload/product/'.$c->options->image)}}" width="70px" alt=""></a>
                                </td>
                                <td class="cart_description">
                                    <h4><a href="">{{$c->name}}</a></h4>
                                    <p>Mã sản phẩm: {{$check_product->code_product}}</p>
                                </td>
                                <td class="cart_price">
                                    <p>{{number_format($c->price,0,',','.')}} VNĐ</p>
                                </td>
                                <td class="cart_quantity">
                                    <form method="post" action="{{asset('updatecart')}}">
                                        @csrf
                                        <div class="cart_quantity_button">

                                         <label>{{$c->qty}}</label>

                                        </div>
                                    </form>
                                </td>
                                <td class="cart_total">
                                    <p class="cart_total_price result_{{$i}}"><?php $total=($c->price*$c->qty);echo number_format($total,0,',','.');?> VNĐ</p>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </section> <!--/#cart_items-->

        <section id="do_action">
            <div class="container">
                <div class="row">

                    <div class="col-sm-6">
                        <div class="total_area">
                            <ul>
                                <li>Tạm tính <span id="subtotal">{{Cart::subtotal()}} VNĐ</span></li>
                                <li>Phí vận chuyển <span>Free</span></li>
                                <li>Thành tiền <span><b>{{Cart::subtotal()}} VNĐ</b></span></li>
                            </ul>
                            <input type="submit" class="btn btn-default check_out" value="Thanh toán">
                        </div>
                    </div>
                </div>
            </div>
        </section><!--/#do_action-->

        </div>
</section> <!--/#cart_items-->
</form>
@include('template.footer')
