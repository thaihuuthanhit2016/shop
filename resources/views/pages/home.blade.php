@extends('layout')
@section('content')
@section('title',$config->title_website)
    <div class="category-tab shop-details-tab"><!--category-tab-->
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li><a href="#tag" data-toggle="tab"><i class="fa fa-list"></i></a></li>
                <li class="active"><a href="#reviews" data-toggle="tab"><i class="fa fa-th"></i></a></li>
            </ul>
        </div>
        <div class="tab-content">

            <div class="tab-pane fade" id="tag" >
                <div class="clearfix"></div>
                <h2 class="title text-center">Sản phẩm mới</h2>


                    @foreach($product_new as $p_n)
                        <?php
                        $pro=DB::table('tbl_product_shop')->where('id_product',$p_n->id_product)->first();

                        $pro_shop=DB::table('tbl_shop')->where('id_shop_user',$pro->id_shop)->first();
                        $pro_specifications=DB::table('tbl_product_specifications')->where('id_product',5)->first();
                        ?>

                <div class="col-sm-12">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo ">
                                <a href="{{asset('detail/'.$p_n->product_name_slug)}}">
                                    <div class="col-md-3">
                                        <img src="{{asset('upload/product/'.$p_n->product_images)}}" alt="" width="100%">
                                    </div>
                                </a>
                                <a href="{{asset('detail/'.$p_n->product_name_slug)}}">

                                    <div class="col-md-9">
                                        @if($pro_specifications==null)
                                            @if($p_n->product_price_km!=0)
                                                <h4 style="    text-decoration: line-through;color: #ccc">{{number_format($p_n->product_price,0,',','.')}} VNĐ</h4>
                                                <h2 style="color:#FE980F;">{{number_format($p_n->product_price_km,0,',','.')}} VNĐ <sup style="color:red;font-size: 15px">-<?php
                                                        echo 100-ceil($p_n->product_price_km*100/$p_n->product_price).'%';
                                                        ?></sup></h2>
                                            @else
                                                <h2 style="color:#FE980F;">{{number_format($p_n->product_price,0,',','.')}} VNĐ</h2>
                                            @endif
                                        @else

                                            @if($pro_specifications->price_small_km!=0)
                                            <h4 style="    text-decoration: line-through;color: #ccc">{{number_format($pro_specifications->price_small,0,',','.')}} VNĐ</h4>
                                            <h2 style="color:#FE980F;">{{number_format($pro_specifications->price_small_km,0,',','.')}} VNĐ <sup style="color:red;font-size: 15px">-<?php
                                                    echo 100-ceil($pro_specifications->price_small_km*100/$pro_specifications->price_small).'%';
                                                    ?></sup></h2>
                                            @else
                                                <h2 style="color:#FE980F;">{{number_format($pro_specifications->price_small,0,',','.')}} VNĐ</h2>
                                            @endif
                                        @endif

                                            <p>{{mysubstr($p_n->product_name,35)}}</p>

                                <div class="shop_info"><a href="{{asset('shop/'.$pro_shop->id_shop_user)}}"> <i class="fa fa-bank"></i> <b>{{$pro_shop->name_shop}}</b></a></div>

                            </div>
                                </a>

                            </div>
                        </div>
                    </div>

                </div>
                @endforeach
                <div class="clearfix"></div>
                <br>
            </div>

            <div class="tab-pane fade active in" id="reviews" >
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Sản phẩm mới</h2>
                    @include('admin.errors.error')

                    <div class="clearfix"></div>

                    <?php
                    $i=0;
                    ?>
                        @foreach($product_new as $p_n)
                            <?php
                            $i++;
                            $pro=DB::table('tbl_product_shop')->where('id_product',$p_n->id_product)->first();

                            $pro_shop=DB::table('tbl_shop')->where('id_shop_user',$pro->id_shop)->first();

                            ?>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">

                                            <a href="{{asset('detail/'.$p_n->product_name_slug)}}" ><img src="{{asset('upload/product/'.$p_n->product_images)}}" alt="" />
                                                @if($p_n->product_price_km!=0)
                                                    <h4 style="    text-decoration: line-through;color: #ccc">{{number_format($p_n->product_price,0,',','.')}} VNĐ</h4>
                                                    <h2 style="color:#FE980F;">{{number_format($p_n->product_price_km,0,',','.')}} VNĐ <sup style="color:red;font-size: 15px">-<?php
                                                            echo 100-ceil($p_n->product_price_km*100/$p_n->product_price).'%';
                                                            ?></sup></h2>
                                                @else
                                                    <h2 style="color:#FE980F;margin-top: 53px;">{{number_format($p_n->product_price,0,',','.')}} VNĐ</h2>
                                                @endif
                                            </a>
                                            <p>{{mysubstr($p_n->product_name,35)}}</p>
                                            </a>
                                            <div class="shop_info"><a href="{{asset('shop/'.$pro_shop->id_shop_user)}}"> <i class="fa fa-bank"></i> <b>{{$pro_shop->name_shop}}</b></a></div>
                                            <a href="{{asset('add-to-cart/'.$p_n->id_product)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ hàng</a>

                                            {{--                        <a style="cursor: pointer" class="btn btn-default xemnhanh" data-product_id="{{$p_n->id_product}}" data-toggle="modal" data-target="#xemnhanh_{{$i}}"><i class="fa fa-eye"></i>Xem nhanh</a>--}}
                                        </div>
                                    </div>
                                </div>



                                <?php

                                $product_new_cash=DB::table('tbl_product')->where('product_status',1)->where('category_id',4)->orderby('product_name','ASC')->paginate();

                                ?>


                            </div>
@if($i==3||$i==6||$i==9)

                                    <br>
                                    <div class="clearfix"></div>
                                @endif
                                    @endforeach

                </div>
            </div>

        </div>
    </div><!--/category-tab-->





@endsection
<style>
    a.tags_style{
        margin: 3px 2px;
        border: 1px solid;
        height: auto;
        background: #428bca;
        color: #FFFFFF;padding: 0px;
        border-radius: 5px;
        padding: 3px;
    }
    a.tags_style:hover{
        background: black;
    }

    .category-tab ul {
     background: white!important;
    }
    .category-tab ul li a {
        font-size: 10px!important;

    }

    .features_items {
        overflow: visible!important;
    }
    .choose ul li a {
        color: #B3AFA8;
        font-family: 'Roboto', sans-serif;
        font-size: 13px;
        padding-left: 0;
        padding-right: 0;
    }
    .nav-tabs>li {
        float: right!important;
        margin-bottom: -1px;
    }

    h3.title-oo {
        background: #FE980F;
        color: white;
        padding: 12px;
    }
</style>
