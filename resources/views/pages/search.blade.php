@extends('layout')
@section('content')
    <div class="features_items"><!--features_items-->
        <h2 class="title text-center">Kết quả tìm kiếm</h2>
        @foreach($product_new as $p_n)
            <div class="col-sm-4">
                <div class="product-image-wrapper">
                    <div class="single-products">
                        <div class="productinfo text-center">
                            <a href="{{asset('detail/'.$p_n->product_name_slug)}}" ><img src="{{asset('upload/product/'.$p_n->product_images)}}" alt="" />
                                <h2>{{number_format($p_n->product_price,0,',','.')}} VNĐ</h2>
                                <p>{{$p_n->product_name}}</p>
                            </a>
                            <a href="{{asset('add-to-cart/'.$p_n->id_product)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ hàng</a>
                        </div>
                    </div>
                </div>

                <div class="choose">
                    <ul class="nav nav-pills nav-justified">
                        <li><a href="{{asset('wishlist/'.$p_n->id_product)}}"><i class="fa fa-plus-square"></i>Thêm vào yêu thích</a></li>
                        <li><a href="#"><i class="fa fa-plus-square"></i>Thêm vào so sánh</a></li>
                    </ul>
                </div>
            </div>
        @endforeach
    </div><!--features_items-->


@endsection
