@extends('layout_news')
@section('content')
    <style>
        .baiviet ul li{
            padding: 2px;
            font-size: 16px;
            list-style-type: decimal-leading-zero;
        }
        .baiviet ul li a{
            color: #000;
        }
        .baiviet ul li a:hover{
            color: #fe980F;
        }
        .mucluc h1{
            font-size: 20px;
            color: brown;
        }

    </style>
    <div class="product-details"><!--product-details-->
        <div class="col-sm-12">
            <div class="product-information"><!--/product-information-->
                <h2 style="color: #FE980F">{{$detail_new->product_name}}</h2>
            </div>
            <div class="content"style="text-align: justify">{!! $detail_new->product_content !!}</div>

            <!--/product-information-->
        </div>
        <input type="hidden" id="news_id" value="{{$detail_new->id_news}}">
        <div class="fb-share-button" data-href="{{asset('detail-news/'.$detail_new->product_name_slug)}}" data-layout="button_count" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{asset('detail-news/'.$detail_new->product_name_slug)}}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
        <div class="clearfix"></div>
    <br>
        <div class="col-md-12" style="float:left;;">
<?php
            $tach=explode(' ',$detail_new->created_at);

            ?>
            <p><span><i class="fa fa-user"></i>Admin</span> &nbsp;&nbsp;
            <span href=""><i class="fa fa-clock-o"></i>{{$tach[0]}} </span>&nbsp;&nbsp;
            <span href=""><i class="fa fa-calendar-o"></i>{{$tach[1]}} </span></p >
        <div class="row">
            <div id="show_comment" ></div>
        </div>
        </div>
        <h3>Bình luận của bạn</h3>
        <div  id="notice"></div>
        <div class="col-md-12">
        <form action="#">
										<span>
											<input type="text" id="name" class="form-control" placeholder="Your Name"/><br>
										</span>
            <textarea name="" class="form-control" id="id_message" placeholder="message" ></textarea><br>
            <button type="button" class="btn btn-primary " id="send_comment">
                Submit
            </button>
        </form></div>

    </div><!--/product-details-->


    <div class="recommended_items"><!--recommended_items-->
        <h2 class="title text-center">Tin tức liên quan</h2>

        <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    @foreach($product_goiy as $p)
                    <div class="col-sm-4">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    <img src="{{asset('upload/tintuc/'.$p->product_images)}}" alt="" />

                                    <p style="font-weight:bold;margin-top: 10px;">{{$p->product_name}}</p>
                                    <span style="color: black">{{mysubstr($p->product_desc,50) }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>

            </div>
            <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                <i class="fa fa-angle-left"></i>
            </a>
            <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                <i class="fa fa-angle-right"></i>
            </a>
        </div>
    </div><!--/recommended_items-->



@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
