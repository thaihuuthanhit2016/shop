<!DOCTYPE html>
<html lang="en">
@include('template.header.head_cart')

<body>
@include('template.header.cart')

<style>
    .title_chat{
        font-weight: bold;
        font-size: 17px;
    }
    .box_chat_friends .box img{
        border-radius: 50%;
        border: 1px solid #ccc;
        float: left;
        margin-right: 20px;

        margin-top: 20px;
    }
    .box_chat_friends .box{
        padding: 10px;

        height: 127px;
        border: white;
        margin-bottom: 20px;
    }
    .box_chat_friends .box .title_chat1,.box_chat_friends .box .title_chat2s{
        margin-top: 22px;

    }
    .point{
        float: right;
        width: 10px;
        height: 10px;
        border-radius: 50%;
        background: green;

        margin-top: -32px;
    }
.box_message p{
    background: #cccccc;
    color: #000;
    border-radius: 20px;
    padding: 10px;
    text-align: justify;
}.box_message2 p{
    background: blue;
    color: white;
    border-radius: 20px;
    padding: 10px;
    float: right;
        text-align: justify;
}
    .box_chat_friends{
        width: 100%;
        height: 500px;
        overflow: scroll;

    }
    .chat .top img{
        float: left;
        border-radius: 50%;
        border: 1px solid #ccc;
        margin-right: 10px;
    }
</style>
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="{{asset('/')}}">Trang chủ</a></li>
                <li class="active">Chat</li>
            </ol>
        </div>
        <div class="col-md-12">
            <div class="col-md-3">
                <div class="left_chat">
                    <p class="title_chat">Chat</p>
                    <div class="box_chat_friends">
                        <div class="box">
                            <img src="{{asset('upload/avatar/avatar.jpg')}}"width="60px" height="60px" >
                            <p class="title_chat1">huu thanh</p>
                            <p class="title_chat2">huu thanh</p>

                                <p class="point"></p>

                        </div>
                        <div class="box">
                            <img src="{{asset('upload/avatar/avatar.jpg')}}"width="60px" height="60px" >
                            <p class="title_chat1">huu thanh</p>
                            <p class="title_chat2">huu thanh</p>

                                <p class="point"></p>

                        </div>
                        <div class="box">
                            <img src="{{asset('upload/avatar/avatar.jpg')}}"width="60px" height="60px" >
                            <p class="title_chat1">huu thanh</p>
                            <p class="title_chat2">huu thanh</p>

                                <p class="point"></p>

                        </div>
                        <div class="box">
                            <img src="{{asset('upload/avatar/avatar.jpg')}}"width="60px" height="60px" >
                            <p class="title_chat1">huu thanh</p>
                            <p class="title_chat2">huu thanh</p>

                                <p class="point"></p>

                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="chat">
                    <div class="top">
                        <img src="{{asset('upload/avatar/avatar.jpg')}}"width="60px" height="60px" >
                        <p class="title_chat1">huu thanh</p>
                        <p class="title_chat2">huu thanh</p>

                    </div>
                    <div class="content">
                        <div class="box_message">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

                            </p>
                        </div>
                        <div class="box_message2">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</section> <!--/#cart_items-->


@include('template.footer')
