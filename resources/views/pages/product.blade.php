@extends('layout')
@section('title','Shop')

@section('content')
    <div class="features_items"><!--features_items-->
        <h2 class="title text-center">Sản phẩm </h2>
        @include('admin.errors.error')
        <br>
        <div class="clearfix"></div>
        <br>
        <?php
        $i=0;
        ?>
        @foreach($category as $ca)
<?php

            $ca_con=DB::table('tbl_category_product')->where('category_parent',$ca->id_category_product)->first();
if ($ca_con!=null){
            $product=DB::table('tbl_product')->where('category_id',$ca_con->id_category_product)->get();
}
            ?>
        <div><h3  class="title-oo">{{$ca->category_name}}</h3></div>
    @foreach($product as $p_n)

            <?php
        $i++;

        $pro=DB::table('tbl_product_shop')->where('id_product',$p_n->id_product)->first();
            $pro_shop=DB::table('tbl_shop')->where('id_shop_user',$pro->id_shop)->first();
            ?>

            <div class="col-sm-4">
            <div class="product-image-wrapper">
                <div class="single-products">
                    <div class="productinfo text-center">

                        <a href="{{asset('detail/'.$p_n->product_name_slug)}}" ><img src="{{asset('upload/product/'.$p_n->product_images)}}" alt="" />
                        @if($p_n->product_price_km!=0)
                            <h4 style="    text-decoration: line-through;color: #ccc">{{number_format($p_n->product_price,0,',','.')}} VNĐ</h4>
                            <h2 style="color:#FE980F;">{{number_format($p_n->product_price_km,0,',','.')}} VNĐ</h2>
                            @else
                                <div class="price_pro">

                                <h2 style="color:#FE980F;">{{number_format($p_n->product_price,0,',','.')}} VNĐ</h2>
                                </div>
                            @endif
                        </a>
                        <p>{{mysubstr($p_n->product_name,35)}}</p>
                        </a>
                        <div class="shop_info"><a href="{{asset('shop/'.$pro_shop->id_shop_user)}}"> <i class="fa fa-bank"></i> <b>{{$pro_shop->name_shop}}</b></a></div>
                        <a href="{{asset('add-to-cart/'.$p_n->id_product)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ hàng</a>
                    </div>
                </div>
            </div>

            <div class="choose">
                <ul class="nav nav-pills nav-justified">
                    <li><a href="{{asset('wishlist/'.$p_n->id_product)}}"><i class="fa fa-plus-square"></i>Thêm vào yêu thích</a></li>
                    <li><a href="{{asset('compare/'.$p_n->id_product)}}"><i class="fa fa-plus-square"></i>So sánh</a></li>
                </ul>
            </div>
        </div>

    @endforeach




    <div class="clearfix"></div>
        <br>
        @endforeach
    </div><!--features_items-->


<?php echo $i?>
@endsection
<style>
    .title-oo{
        padding: 10px;
        color: white;
        background: #FE980F;
    }
</style>
