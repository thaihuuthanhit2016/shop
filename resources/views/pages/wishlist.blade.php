<!DOCTYPE html>
<html lang="en">
@include('template.header.head_cart')

<body>
@include('template.header.cart')
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
@include('pages.left_menu')            </div>

            <div class="col-sm-9">
                    <h3 class="title">DANH SÁCH SẢN PHẨM TRONG WISHLIST CỦA BẠN</h3>
                <br>
                <div class="table-responsive cart_info">
                    <table class="table table-condensed">
                        <thead>
                        @foreach($wishlist as $b)
                               <?Php
                                $product=DB::table('tbl_product')->where('id_product',$b->id_product)->first();
                                ?>
                        <tr class="cart_menu">
                            <td class="price">Tên sản phẩm: <b>{{$product->product_name}}</b> &nbsp;&nbsp;<img src="{{asset('upload/product/'.$product->product_images)}}" width="50px"></td>
                            <td class="quantity">Giá:<b> @if($product->product_price_km!=0){{number_format($product->product_price_km,0,',','.')}} @else {{number_format($product->product_price,0,',','.')}} @endif VNĐ</b></td>
                            <td class="price">Tình trạng : @if($product->product_status==0) Tạm hết hàng @else Còn hàng @endif</td>
                            <td class="total"> <a style="color:#FE980F;" href="{{asset('add-to-cart/'.$product->id_product)}}">Mua ngay</a></td>
                            <td></td>
                        </tr>
                        @endforeach
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>

@include('template.footer')
<style>
    .active{
        color: #FE980F!important;
    }
</style>

<script>
    $(document).ready(function (){
        $('#wishlist').addClass('active');
    });
</script>
