@extends('mail.inbox')
@section('content')
    <section id="main-content">
        <section class="wrapper">
            <div class="table-agile-info">
                <div class="panel panel-default">

                    <section class="panel">
                        <header class="panel-heading" style="color: #FE980F">
                            Thư đã gửi
                        </header>
                        <br>
              <center>          @include('admin.errors.error')</center>
                    <div class="row w3-res-tb">
                        <div class="col-sm-5 m-b-xs">
                            <select class="input-sm form-control w-sm inline v-middle">
                                <option value="0">Bulk action</option>
                                <option value="1">Delete selected</option>
                                <option value="2">Bulk edit</option>
                                <option value="3">Export</option>
                            </select>
                            <button class="btn btn-sm btn-default">Apply</button>
                        </div>
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input type="text" class="input-sm form-control" placeholder="Search">
                                <span class="input-group-btn">
                           <button class="btn btn-sm btn-default" type="button">Go!</button>
                           </span>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped b-t b-light">
                            <tbody>
                            @if(count($mailsend)>0)
                                @foreach($mailsend as $m)
                                    <tr>
                                        <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                                        <td style=""> <a style="color: black" href="{{asset('admin/viewed/'.$m->id_mail)}}">{{$m->subject}}</a></td>
                                        <td style=""> <a style="color: black"  href="{{asset('admin/viewed/'.$m->id_mail)}}"><span class="text-ellipsis">{{$m->created_at}}</span></a></td>
                                        <td style=""> <a style="color: black"  href="{{asset('admin/viewed/'.$m->id_mail)}}"><span class="text-ellipsis">{{$m->updated_at}}</span></a></td>

                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td>Không có thư nào !!!</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-sm-7 text-right text-center-xs">
                                {{$mailsend->links('admin.paginate')}}
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        </section>
        <!-- footer -->
        <div class="footer">
            <div class="wthree-copyright">
                <p>© 2017 Visitors. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>
            </div>
        </div>
        <!-- / footer -->
    </section>
@endsection
