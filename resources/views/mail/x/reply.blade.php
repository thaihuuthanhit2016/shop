    @extends('mail.inbox')
    @section('content')
    <section id="main-content">
        <section class="wrapper">
            <div class="table-agile-info">
                <div class="panel panel-default">
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <header class="panel-heading" style="color: #FE980F">
                                    Soạn thư trả lời <b>{{$view->subject}}</b>
                                </header>
                                <div class="panel-body">
                                    <div class="position-center">
                                        <form role="form" method="post" enctype="multipart/form-data">
                                            @csrf

                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Tiêu đề</label>
                                                <input type="text" readonly class="form-control" value="{{$view->subject}}" id="exampleInputPassword1" placeholder="Password">
                                                <input type="hidden" readonly class="form-control" value="{{$view->id_user}}" name="id_user" id="exampleInputPassword1" placeholder="Password">
                                                <input type="hidden" readonly class="form-control" value="{{$view->subject}}" name="subject" id="exampleInputPassword1" placeholder="Password">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputFile">Nội dung thư trả lời</label>
                                                <textarea name="content" rows="10" id="content"></textarea>
                                            </div>
                                            <script>
                                                CKEDITOR.replace( 'content',
                                                    {
                                                        filebrowserBrowseUrl : '{{asset("public/admin/ckfinder/ckfinder.html")}}',
                                                        filebrowserImageBrowseUrl : '{{asset("public/admin/ckfinder/ckfinder.html?type=Images")}}',
                                                        filebrowserFlashBrowseUrl : '{{asset("public/admin/ckfinder/ckfinder.html?type=Flash")}}',
                                                        filebrowserUploadUrl : '{{asset("public/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files")}}',
                                                        filebrowserImageUploadUrl : '{{asset("public/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images")}}',
                                                        filebrowserFlashUploadUrl : '{{asset("public/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash")}}'
                                                    });
                                            </script>

                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Tệp đính kèm</label>
                                                <input type="file" class="form-control" name="file[]" multiple id="exampleInputPassword1" placeholder="Password">
                                            </div>
                                            <button type="submit" class="btn btn-info">Gửi</button>
                                        </form>
                                    </div>

                                </div>
                            </section>

                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- footer -->
        <div class="footer">
            <div class="wthree-copyright">
                <p>© 2017 Visitors. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>
            </div>
        </div>
        <!-- / footer -->
    </section>
@endsection
