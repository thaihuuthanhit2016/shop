@extends('mail.inbox')
@section('content')
    <section id="main-content">
        <section class="wrapper">
            <div class="table-agile-info">
                <div class="panel panel-default">

                    <section class="panel">
                        <header class="panel-heading" style="color: #FE980F">
                            NỘI DUNG CHI TIẾT THƯ
                        </header>
                    <div class="table-responsive" style="margin-left: 10px;margin-top: 5px">
                        <div class="form-group">
                            <p for="usr">Tiêu đề:<span style="font-weight: bold">{{ $view->subject}}</span></p>

                        </div>
                            <div class="form-group">
                            <div for="usr">{!! $view->content!!}</div>

                        </div>

                            <div class="form-group">
                                <?php
                                    $mail=DB::table('mail_file')->where('id_mail',$view->id_mail)->get();
                                ?>
                                @if(count($mail)>0)
@foreach($mail as $i_m)
                                <div class="col-md-4" >
                            <div for="usr"><img src="{{asset('upload/mail/'.$i_m->images)}}" width="300px"></div>
                                </div>
                                        @endforeach
                                    @endif
                        </div>
<div class="clearfix"></div>
                        <br><br>
                        @if($view->id_user!=Session::get('id_shop_user'))
                            <div class="form-group">
                                <div for="usr"><a  style="color:#FE980F;" href="{{asset('admin/reply/'.$view->id_mail)}}">Trả lời thư</a></div>

                        </div>
                        @endif
                    </div>
                    </section>
                </div>

            </div>
        </section>
        <!-- footer -->
        <div class="footer">
            <div class="wthree-copyright">
                <p>© 2017 Visitors. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>
            </div>
        </div>
        <!-- / footer -->
    </section>
@endsection
