<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a href="index.html">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="{{asset('admin/writenew')}}">
                        <i class="fa fa-book"></i>
                        <span>Soạn thư</span>
                    </a>
                </li>

                <?php
                $mailinbox=DB::table('mail')->where('to_id_user',Session::get('id_shop_user'))->where('viewed',0)->get();

                ?>
                <li class="sub-menu">
                    <a href="{{asset('admin/email')}}">
                        <i class="fa fa-book"></i>
                        <span>Hộp thư đến <sup style="color: #FE980F">({{count($mailinbox)}})</sup></span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="{{asset('admin/send')}}">
                        <i class="fa fa-book"></i>
                        <span>Hộp thư gửi </span>
                    </a>
                </li>
                <li>
                    <a href="fontawesome.html">
                        <i class="fa fa-bullhorn"></i>
                        <span>Font awesome </span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- sidebar menu end-->
    </div>
</aside>
