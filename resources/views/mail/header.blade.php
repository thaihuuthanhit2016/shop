<header class="header fixed-top clearfix">
    <!--logo start-->
    <div class="brand">
        <a href="index.html" class="logo">
            VISITORS
        </a>
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars"></div>
        </div>
    </div>
    <!--logo end-->
    <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        @if(!Session::get('admin_id'))
            <ul class="nav top-menu">
                <!-- settings start -->
                <!-- settings end -->
                <!-- inbox dropdown start-->
                <?php
                $mail=DB::table('mail')->where('viewed',0)->orderby('id_mail','DESC')->limit(5)->get();
                $mail_=DB::table('mail')->where('viewed',0)->orderby('id_mail','DESC')->get();
                ?>
                <li id="header_inbox_bar" class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="fa fa-envelope-o"></i>
                        <span class="badge bg-important">{{count($mail_)}}</span>
                    </a>
                    <ul class="dropdown-menu extended inbox">
                        <li>
                            <p class="red">Bạn có <b>{{count($mail_)}}</b> thư mới</p>
                        </li>
                        @foreach($mail as $m)
                            <li>
                                <a href="{{asset('admin/viewed/'.$m->id_mail)}}">
                                    <span class="photo"><img alt="avatar" src="{{asset('public/admin/images/3.png')}}"></span>
                                    <span class="subject">{{$m->title}}
                        <span class="from">Từ hệ thống </span>
                        <span class="time">Mới</span>
                        </span>
                                    <span class="message">
                        {{mysubstr($m->content,30)}}
                        </span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <!-- inbox dropdown end -->
                <!-- notification dropdown start-->
                <?php
                $notification=DB::table('tbl_notification')->where('type',0)->orderby('id_notification','DESC')->limit(5)->get();
                ?>
                <li id="header_notification_bar" style="cursor: pointer" class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle">
                        <i class="fa fa-bell-o"></i>
                        <span class="badge bg-warning">{{count($notification)}}</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li>
                            <p>Thông báo</p>
                        </li>
                        @foreach($notification as $n)
                            <li>
                                <div class="alert alert-info clearfix">
                                    <span class="alert-icon"><i class="fa fa-bolt"></i></span>
                                    <div class="noti-info">
                                        <a href="{{asset('admin/notice/view/'.$n->title_alias)}}"> {{$n->title}}</a>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <!-- notification dropdown end -->
            </ul>
    @endif        <!--  notification end -->
    </div>
    <div class="top-nav clearfix">
        <!--search & user info start-->
        <ul class="nav pull-right top-menu">
            <li>
                <input type="text" class="form-control search" placeholder=" Search">
            </li>
            <!-- user login dropdown start-->
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <img alt="" src="{{asset('public/admin/images/2.png')}}">
                    <span class="username">{{Session::get('username')}}</span>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu extended logout">
                    <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
                    <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                    <li><a href="login.html"><i class="fa fa-key"></i> Log Out</a></li>
                </ul>
            </li>
            <!-- user login dropdown end -->
        </ul>
        <!--search & user info end-->
    </div>
</header>
