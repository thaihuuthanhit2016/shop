@extends('admin.master');
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Sửa slider
                </header>
                <div class="panel-body">
                    <div class="position-center">

                        <form role="form" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                @include('admin.errors.error')
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tiêu đề</label>
                                <input type="text"  value="{{$product->title}}" class="form-control" name="title" id="exampleInputEmail1" placeholder="Tên ">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Link</label>
                                <input type="text"  value="{{$product->link}}" class="form-control" name="link" id="exampleInputEmail1" placeholder="Tên ">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Hình ảnh </label>
                                <input type="file" class="form-control" name="images">
                            </div>
                            <div class="form-group">
                                <img src="{{asset('upload/slider/'.$product->images)}}" width="200px">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Loại</label>
                                <select class="form-control" name="loai" >
                                    <option value="1" @if($product->type==1) selected @endif>Banner</option>
                                    <option value="0" @if($product->type==0) selected @endif>Slider</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-info">Submit</button>
                        </form>
                    </div>

                </div>
            </section>

        </div>

    </div>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        if($item->parent_id==$parent_id){
            echo '<option value="'.$item->id_category_product.'">'.$char.$item->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>
