@extends('admin.master');
@section('content')
    <div class="table-agile-info">
        <div class="panel panel-default">
            <div class="panel-heading">
                Danh sách Sản phẩm
            </div>
            <div class="row w3-res-tb">
                <div class="col-sm-5 m-b-xs">
                    <select class="input-sm form-control w-sm inline v-middle">
                        <option value="0">Bulk action</option>
                        <option value="1">Delete selected</option>
                        <option value="2">Bulk edit</option>
                        <option value="3">Export</option>
                    </select>
                    <button class="btn btn-sm btn-default">Apply</button>
                </div>
                <div class="col-sm-4">
                </div>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input type="text" class="input-sm form-control" placeholder="Search">
                        <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button">Go!</button>
          </span>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                @include('admin.errors.error')
<?php
                Session::put('message_error','');
                Session::put('product_code',"");
                Session::put('product_name','');
                Session::put('product_price','');
                Session::put('product_price_km','');
                Session::put('product_desc','');
                Session::put('product_content','');

          ?>      <table class="table table-striped b-t b-light">
                    <thead>
                    <tr>
                        <th style="width:20px;">
                            <label class="i-checks m-b-none">
                                <input type="checkbox"><i></i>
                            </label>
                        </th>
                        <th>Tên Sản phẩm</th>
                        <th>Hình ảnh</th>

                        <th>Giá sản phẩm</th>
                        <th>Giá sản phẩm (Khuyến mãi)</th>
                        <th>Tình trạng</th>
                        <th>Danh mục</th>
                        <th>Thương hiệu</th>
                        <th style="width:100px;">Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $c)

<?php

$d=DB::table('tbl_product')->where('id_product',$c->id_product)->first();
$category=DB::table('tbl_category_product')->where('id_category_product',$d->category_id)->first();
$brands=DB::table('tbl_brands_product')->where('id_brands_product',$d->brands_id)->first();
?>
                    <tr>
                        <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                        <td>{{$d->product_name}}@if($d->product_price_km!=0)<sup style="color:red;">Sale</sup>@endif</td>
                        <td><img  src="{{asset('upload/product/'.$d->product_images)}}" width="50px" ></td>
                        <td>{{$d->product_price}}</td>
                        <td>{{$d->product_price_km}}</td>
                        <td>@if($d->product_status==0) <span style="color:red;">Hết hàng </span> @else<span style="color:green;"> Còn hàng </span>@endif</td>
                        <td>{{$category->category_name}}</td>
                        <td>{{$brands->brands_name}}</td>

                        <td>
                            <a href="{{asset('admin/product/view/'.$d->id_product)}}" class="active" ui-toggle-class=""><i class="fa fa-eye text-success text-active"></i></a>&nbsp;&nbsp;&nbsp;<a href="{{asset('admin/product/edit/'.$d->id_product)}}" class="active" ui-toggle-class=""><i class="fa fa-pencil-square-o text-success text-active"></i></a>&nbsp;&nbsp;&nbsp; <a href="{{asset('admin/product/del/'.$d->id_product)}}" onclick="confirm('Bạn có chắc xóa không?')"> <i class="fa fa-times text-danger text"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <footer class="panel-footer">
                <div class="row">

                    <div class="col-sm-7 text-right text-center-xs">
                        {{$data->links('admin.paginate')}}
                    </div>
                </div>
            </footer>
        </div>
    </div>

@endsection
