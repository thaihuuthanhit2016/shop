@extends('admin.master');
@section('content')
    <div class="table-agile-info">
        <div class="panel panel-default">
            <div class="panel-heading">
                Danh sách Shop
            </div>
            <div class="row w3-res-tb">
                <div class="col-sm-5 m-b-xs">
                    <select class="input-sm form-control w-sm inline v-middle">
                        <option value="0">Bulk action</option>
                        <option value="1">Delete selected</option>
                        <option value="2">Bulk edit</option>
                        <option value="3">Export</option>
                    </select>
                    <button class="btn btn-sm btn-default">Apply</button>
                </div>
                <div class="col-sm-4">
                </div>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input type="text" class="input-sm form-control" placeholder="Search">
                        <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button">Go!</button>
          </span>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                @include('admin.errors.error')
<?php
                Session::put('message_error','');
                Session::put('product_code',"");
                Session::put('product_name','');
                Session::put('product_price','');
                Session::put('product_price_km','');
                Session::put('product_desc','');
                Session::put('product_content','');

          ?>      <table class="table table-striped b-t b-light">
                    <thead>
                    <tr>
                        <th style="width:20px;">
                            <label class="i-checks m-b-none">
                                <input type="checkbox"><i></i>
                            </label>
                        </th>
                        <th>Tên shop</th>
                        <th>Tài khoản</th>
                        <th>CMND</th>

                        <th>Logo</th>
                        <th>Trạng thái</th>
                        <th style="width:100px;">Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $d)

                    <tr>
                        <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                        <td>{{$d->name_shop}}</td>
                        <td>{{$d->username}}</td>
                        @if($d->verifyKYC!="N")
                        <td><a target="_blank" href="{{asset('upload/shop/'.$d->images1)}}"> <img  src="{{asset('upload/shop/'.$d->images1)}}" width="50px" ></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{asset('upload/shop/'.$d->images2)}}" target="_blank" > <img  src="{{asset('upload/shop/'.$d->images2)}}" width="50px" ></a></td>

                            <td><a href="{{asset('upload/shop/'.$d->images3)}}" target="_blank" > <img  src="{{asset('upload/shop/'.$d->images3)}}" width="50px" ></a></td>
@else

                            <td><img  src="{{asset('upload/shop/cmnd_mt.jpg')}}" width="50px" >&nbsp;&nbsp;&nbsp;&nbsp;<img  src="{{asset('upload/shop/cmnd_ms.jpg')}}" width="50px" ></td>

                            <td><img  src="{{asset('upload/shop/logo.png')}}" width="50px" ></td>
                        @endif
                        <td>@if($d->verifyKYC=='Y') Hoạt động @elseif($d->verifyKYC=='P') Chờ duyệt @else Chưa hoạt động @endif</td>

                        <td>
                            <a href="{{asset('admin/shop/view/'.$d->id_shop_user)}}" class="active" ui-toggle-class=""><i class="fa fa-eye text-success text-active"></i></a>&nbsp;&nbsp;&nbsp;       <a href="{{asset('admin/shop/loginofshop/'.$d->id_shop_user)}}" class="active" ui-toggle-class=""><i class="fa fa-sign-in text-success text-active"></i></a>&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <footer class="panel-footer">
                <div class="row">

                    <div class="col-sm-7 text-right text-center-xs">
                        {{$data->links('admin.paginate')}}
                    </div>
                </div>
            </footer>
        </div>
    </div>

@endsection
