@extends('admin.master');
@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Cập nhật
            </header>
            <div class="panel-body">
                <div class="position-center">

                    <form role="form" method="post">
                        @csrf
                        <div class="form-group">
                            @include('admin.errors.error')
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Trạng thái</label>
                            <select class="form-control input-sm m-bot-15" name="status">

                                <option value="1">Đã nạp</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-info">Submit</button>
                    </form>
                </div>

            </div>
        </section>

    </div>

</div>
@endsection
<?php

function showCategories1($categories,$da,$parent_id=0,$char=''){
    $i=0;
    foreach($categories as $key =>$item){
        $i++;
        if($item->parent_id==$parent_id){
            $html='';
            $html.= '<option value="'.$item->id_category_product.'"';if($item->id_category_product==$da->category_parent){ $html.='selected';}$html.=' >'.$char.$item->category_name.'</option>';
            echo $html;
            unset($categories[$key]);
            showCategories1($categories,$item->id_category_product,$char.=' -- ');
        }
    }
}
?>
