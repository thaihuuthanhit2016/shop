@extends('admin.master');
@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Thêm Sản phẩm
            </header>
            <div class="panel-body">
                <div class="position-center">

                    <form role="form" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            @include('admin.errors.error')
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên Sản phẩm</label>
                            <input type="text" class="form-control" value="@if(Session::get('product_name')!=null) {{Session::get('product_name')}} @endif" name="product_name" id="exampleInputEmail1" placeholder="Tên ">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mã Sản phẩm</label>
                            <input type="text" class="form-control" value="@if(Session::get('product_code')!=null) {{Session::get('product_code')}} @endif" name="product_code" id="exampleInputEmail1" placeholder="Tên ">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Số lượng</label>
                            <input type="text" class="form-control" name="stock" id="exampleInputEmail1" placeholder="Tên ">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Giá Sản phẩm</label>
                            <input type="text" class="form-control" value="@if(Session::get('product_price')!=null) {{Session::get('product_price')}} @endif" name="product_price" id="exampleInputEmail1" placeholder="Tên ">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Giá Sản phẩm (KM)</label>
                            <input type="text" class="form-control" value="@if(Session::get('product_price_km')!=null) {{Session::get('product_price_km')}} @endif" name="product_price_km" id="exampleInputEmail1" placeholder="Tên ">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Hình ảnh Sản phẩm</label>
                            <input type="file" class="form-control  images-preview" name="product_images" onchange="previewFile(this)">
                            <div class="clearfix"></div>
                            <br>
                            <img src="{{asset('upload/avatar/no-image-800x600.png')}}" id="previewimg" >
                            <div class="clearfix"></div>
                            <br>
                        </div>
<style>
    #previewimg{
        width: 200px!important;
        height:  auto!important;

    }
    .none{
        display: none;
    }
</style>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nhóm Hình ảnh Sản phẩm</label>
                            <input type="file" class="form-control images-previews" name="product_images_group[]" multiple onchange="previewFile2(this)">
                            <div class="clearfix"></div>
                            <br>
                            <img class="none previewimg0" >
                            <img class="none previewimg1" >
                            <img class="none previewimg2" >
                            <img class="none previewimg3" >
                            <img class="none previewimg4" >
                            <img class="none previewimg5" >


                            <div class="clearfix"></div>

                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Mô tả Sản phẩm</label>
                            <textarea name="product_desc" class="form-control" id="exampleInputPassword1" >@if(Session::get('product_desc')!=null) {{Session::get('product_desc')}} @endif</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Nội dung Sản phẩm</label>
                            <textarea name="product_content" class="form-control" id="product_content" >@if(Session::get('product_content')!=null) {{Session::get('product_content')}} @endif</textarea>


                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Tag Sản phẩm</label>
                            <input type="text" value="" class="form-control"  name="product_tags" data-role="tagsinput" placeholder="Add tags" />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Danh mục sản phẩm</label>
                            <select class="form-control input-sm m-bot-15" name="category_id">
                                <option value="0">chon</option>

                            @foreach($data as $d)
                            <?php
                                    $c=DB::table('tbl_category_product')->where('id_category_product',$d->id_category_product)->first();
                                    ?>
                                <option value="{{$c->id_category_product}}">{{$c->category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Thương hiệu sản phẩm</label>
                            <select class="form-control input-sm m-bot-15" name="brands_id">
                                <option value="0">chon</option>

                                @foreach($brands as $d)

                                    <?php
                                    $c=DB::table('tbl_brands_product')->where('id_brands_product',$d->id_brands_product)->first();
                                    ?>
                                    <option value="{{$c->id_brands_product}}">{{$c->brands_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Tình trạng</label>
                            <select class="form-control input-sm m-bot-15" name="product_status">
                                <option value="1">Còn hàng</option>

                                <option value="0">Hết hàng</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-info">Submit</button>
                    </form>
                </div>

            </div>
        </section>

    </div>

</div>
@endsection
<?php

function showCategories($categories,$parent_id=0,$char=''){
    foreach($categories as $key =>$item){
        if($item->parent_id==$parent_id){
            echo '<option value="'.$item->id_category_product.'">'.$char.$item->category_name.'</option>';
            unset($categories[$key]);
            showCategories($categories,$item->id_category_product,$char=' | --');
        }
    }
}
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript" src="{{asset('public/admin/js/bootstrap-tagsinput.js')}}"></script>


<script>
    function previewFile(input){
        var file=$('.images-preview').get(0).files[0];
        if(file){
            var  reader=new  FileReader();
            reader.onload=function (){
                $("#previewimg").attr('src',reader.result);
            }
            reader.readAsDataURL(file);
        }
    }
</script>
