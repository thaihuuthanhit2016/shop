<!--A Design by W3layouts
   Author: W3layout
   Author URL: http://w3layouts.com
   License: Creative Commons Attribution 3.0 Unported
   License URL: http://creativecommons.org/licenses/by/3.0/
   -->
<!DOCTYPE html>
<head>
    <title>Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Visitors Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
      Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- bootstrap-css -->
    <link rel="stylesheet" href="{{asset('public/admin/css/bootstrap.min.css')}}" >
{{--    <link rel="stylesheet" href="{{asset('public/Eshopper/css/style_chat.css')}}" >--}}
    <!-- //bootstrap-css -->
    <!-- Custom CSS -->
    <link href="{{asset('public/admin/css/bootstrap-tagsinput.css')}}" rel='stylesheet' type='text/css' />
    <link href="{{asset('public/admin/css/style.css')}}" rel='stylesheet' type='text/css' />
    <link href="{{asset('public/admin/css/style-responsive.css')}}" rel="stylesheet"/>
    <!-- font CSS -->
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.9.0/bootstrap-table.min.css'>
    <link rel='stylesheet' href='https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css'>
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <!-- font-awesome icons -->
    <link rel="stylesheet" href="{{asset('public/admin/css/font.css')}}" type="text/css"/>
    <link href="{{asset('public/admin/css/font-awesome.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('public/admin/css/morris.css')}}" type="text/css"/>
    <!-- calendar -->
    <link rel="stylesheet" href="{{asset('public/admin/css/monthly.css')}}">
    <link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css'>
    <!-- //calendar -->
    <!-- //font-awesome icons -->
    <script src="{{asset('public/admin/js/jquery2.0.3.min.js')}}"></script>
    <script src="{{asset('public/admin/js/raphael-min.js')}}"></script>
    <script src="{{asset('public/admin/js/morris.js')}}"></script>
    <script src="{{asset('public/admin/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('public/admin/ckfinder/ckfinder.js')}}"></script>
</head>
<body>
<section id="container">
    <!--header start-->
    <header class="header fixed-top clearfix" style="margin-top: -6px; ">
        <!--logo start-->
        <div class="brand">
            <a href="#" class="logo">
                VISITORS
            </a>
            <div class="sidebar-toggle-box">
                <div class="fa fa-bars"></div>
            </div>
        </div>
        <!--logo end-->
        <div class="nav notify-row" id="top_menu">
            <!--  notification start -->
            @if(!Session::get('admin_id'))
                <ul class="nav top-menu">
                    <!-- settings start -->
                    <!-- settings end -->
                    <!-- inbox dropdown start-->
                    <?php
                    $mail=DB::table('mail')->where('viewed',0)->orderby('id_mail','DESC')->limit(5)->get();
                    $mail_=DB::table('mail')->where('viewed',0)->orderby('id_mail','DESC')->get();
                    ?>
                    <li id="header_inbox_bar" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i class="fa fa-envelope-o"></i>
                            <span class="badge bg-important">{{count($mail_)}}</span>
                        </a>
                        <ul class="dropdown-menu extended inbox">
                            <li>
                                <p class="red">Bạn có <b>{{count($mail_)}}</b> thư mới</p>
                            </li>
                            @foreach($mail as $m)
                                <li>
                                    <a href="{{asset('admin/viewed/'.$m->title_alias)}}">
                                        <span class="photo"><img alt="avatar" src="{{asset('public/admin/images/3.png')}}"></span>
                                        <span class="subject">{{$m->title}}
                        <span class="from">Từ hệ thống </span>
                        <span class="time">Mới</span>
                        </span>
                                        <span class="message">
                        {{mysubstr($m->content,30)}}
                        </span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    <!-- inbox dropdown end -->
                    <!-- notification dropdown start-->
                    <?php
                    $notification=DB::table('tbl_notification')->where('type',0)->orderby('id_notification','DESC')->limit(5)->get();
                    ?>
                    <li id="header_notification_bar" style="cursor: pointer" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle">
                            <i class="fa fa-bell-o"></i>
                            <span class="badge bg-warning">{{count($notification)}}</span>
                        </a>
                        <ul class="dropdown-menu extended notification">
                            <li>
                                <p>Thông báo</p>
                            </li>
                            @foreach($notification as $n)
                                <li>
                                    <div class="alert alert-info clearfix">
                                        <span class="alert-icon"><i class="fa fa-bolt"></i></span>
                                        <div class="noti-info">
                                            <a href="{{asset('admin/notice/view/'.$n->title_alias)}}"> {{$n->title}}</a>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    <!-- notification dropdown end -->
                </ul>
        @endif
        <!--  notification end -->
        </div>
        <div class="top-nav clearfix">
            <!--search & user info start-->
            <ul class="nav pull-right top-menu">
                <li>
                    <input type="text" class="form-control search" placeholder=" Search">
                </li>
                <!-- user login dropdown start-->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <img alt="" src="{{asset('public/admin/images/2.png')}}">
                        <span class="username">@if(Session::get('admin_name')!=''){{Session::get('admin_name')}}@else {{Session::get('username')}} @endif</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <li><a href="@if(Session::get('username')!=null) {{asset('shop/info')}} @endif"><i class=" fa fa-suitcase"></i>Thông tin</a></li>
                        <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                        <li><a href="{{asset('logout')}}"><i class="fa fa-key"></i> Log Out</a></li>
                    </ul>
                </li>
                <!-- user login dropdown end -->
            </ul>
            <!--search & user info end-->
        </div>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <aside>
        <div id="sidebar" class="nav-collapse">
            <!-- sidebar menu start-->
            <div class="leftside-navigation">
                <ul class="sidebar-menu demo" id="nav-accordion">
                    <li>
                        <a class="active" href="{{asset('admin/dashboard')}}">
                            <i class="fa fa-dashboard"></i>
                            <span>Tổng quan</span>
                        </a>
                    </li>
                    <li class="sub-menu">
                        <a href="javascript:;">
                            <i class="fa fa-book"></i>
                            <span>Danh mục Sản phẩm</span>
                        </a>
                        <ul class="sub">
                            <li><a href="{{asset('/admin/category/add')}}">Thêm</a></li>
                            <li><a href="{{asset('/admin/category/list')}}">Danh sách</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="javascript:;">
                            <i class="fa fa-book"></i>
                            <span>Hỗ trợ</span>
                        </a>
                        <ul class="sub">
                            <li><a href="http://localhost/shopbitcoin/chat/login.php" target="_blank">Hỗ trợ</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="javascript:;">
                            <i class="fa fa-book"></i>
                            <span>Khuyến mãi</span>
                        </a>
                        <ul class="sub">
                            <li><a href="{{asset('/admin/offer/add')}}">Thêm</a></li>
                            <li><a href="{{asset('/admin/offer/list')}}">Danh sách</a></li>
                        </ul>
                    </li>
                    <?php
                    $mailinbox=DB::table('mail')->where('to_id_user',Session::get('id_shop_user'))->where('viewed',0)->get();
                    $bill=DB::table('tbl_bill_detail')->select('id_bill')->groupBy('id_bill')->get();
                    $j=DB::table('tbl_bill')->where('status',0)->get();
                    $i=0;
                    foreach ($bill as $b){

                        $bill=DB::table('tbl_bill')->where('id_bill',$b->id_bill)->where('status',0)->first();
                        if($bill!=null){
                            $bill_detail=DB::table('tbl_bill_detail')->where('id_bill',$bill->id_bill)->first();
                            $shop=DB::table('tbl_product_shop')->where('id_product',$bill_detail->id_product)->first();
                            if($shop->id_shop==Session::get('id_shop_user')){

                                $i++;

                            }
                        }
                    }

                    ?>
                    <li class="sub-menu">
                        <a href="javascript:;">
                            <i class="fa fa-book"></i>
                            <span>Email <sup style="color: #FE980F">({{count($mailinbox)}})</sup></span>
                        </a>
                        <ul class="sub">
                            <li><a href="{{asset('/admin/email')}}">Mail của tôi</a></li>
                            <li><a href="{{asset('/admin/offer/list')}}">Danh sách</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="javascript:;">
                            <i class="fa fa-book"></i>
                            <span>Hiệu Sản phẩm</span>
                        </a>
                        <ul class="sub">
                            <li><a href="{{asset('admin/brands/add')}}">Thêm</a></li>
                            <li><a href="{{asset('admin/brands/list')}}">Danh sách</a></li>
                        </ul>
                    </li>
                    @if(Session::get('admin_id'))
                        <li class="sub-menu">
                            <a href="javascript:;">
                                <i class="fa fa-book"></i>
                                <span>Slider - Banner</span>
                            </a>
                            <ul class="sub">
                                <li><a href="{{asset('admin/slider/add')}}">Thêm</a></li>
                                <li><a href="{{asset('admin/slider/list')}}">Danh sách</a></li>
                            </ul>
                        </li>
                        <li class="sub-menu">
                            <a href="javascript:;">
                                <i class="fa fa-book"></i>
                                <span>Trắc nghiệm</span>
                            </a>
                            <ul class="sub">
                                <li><a href="{{asset('admin/multiple-choice/add')}}">Thêm</a></li>
                                <li><a href="{{asset('admin/multiple-choice/list')}}">Danh sách</a></li>
                            </ul>
                        </li>
                        <li class="sub-menu">
                            <a href="javascript:;">
                                <i class="fa fa-book"></i>
                                <span>Trang </span>
                            </a>
                            <ul class="sub">
                                <li><a href="{{asset('admin/pages/add')}}">Thêm</a></li>
                                <li><a href="{{asset('admin/pages/list')}}">Danh sách</a></li>
                            </ul>
                        </li>
                    @endif
                    <li class="sub-menu">
                        <a href="javascript:;">
                            <i class="fa fa-book"></i>
                            <span>Sản phẩm</span>
                        </a>
                        <ul class="sub">
                            <li><a href="{{asset('admin/product/add')}}">Thêm</a></li>
                            <li><a href="{{asset('admin/product/list')}}">Danh sách</a></li>
                        </ul>
                    </li>
                    @if(Session::get('admin_id'))
                        <li class="sub-menu">
                            <a href="javascript:;">
                                <i class="fa fa-book"></i>
                                <span>Shop</span>
                            </a>
                            <ul class="sub">
                                <li><a href="{{asset('admin/shop/list')}}">Danh sách</a></li>
                            </ul>
                        </li>
                        <li class="sub-menu">
                            <a href="javascript:;">
                                <i class="fa fa-book"></i>
                                <span>Danh mục Tin tức</span>
                            </a>
                            <ul class="sub">
                                <li><a href="{{asset('admin/category-news/add')}}">Thêm</a></li>
                                <li><a href="{{asset('admin/category-news/list')}}">Danh sách</a></li>
                            </ul>
                        </li>
                        <li class="sub-menu">
                            <a href="javascript:;">
                                <i class="fa fa-book"></i>
                                <span>Bình luận</span>
                            </a>
                            <ul class="sub">
                                <li><a href="{{asset('admin/comment/list')}}">Danh sách</a></li>
                            </ul>
                        </li>
                        <li class="sub-menu">
                            <a href="javascript:;">
                                <i class="fa fa-book"></i>
                                <span>Tin tức</span>
                            </a>
                            <ul class="sub">
                                <li><a href="{{asset('admin/news/add')}}">Thêm</a></li>

                                <li><a href="{{asset('admin/news/list')}}">Danh sách</a></li>
                            </ul>
                        </li>
                    @endif
                    <?php
                    ?>
                    @if(Session::get('id_shop_user'))
                        <li class="sub-menu">
                            <a href="javascript:;">
                                <i class="fa fa-book"></i>
                                <span>Đơn hàng <sup style="color: #FE980F">({{$i}})</sup></span></span>
                            </a>
                            <ul class="sub">
                                <li><a href="{{asset('admin/order/list')}}">Danh sách</a></li>
                            </ul>
                        </li>
                        <li class="sub-menu">
                            <a href="http://localhost/shopbitcoin/laravel-filemanager?type=Images&CKEditor=product_content&CKEditorFuncNum=3&langCode=vi" target="_blank">
                                <i class="fa fa-book"></i>
                                <span>Thư viện ảnh</span>
                            </a>
                        </li>
                    @endif
                    @if(Session::get('admin_id'))
                        <li class="sub-menu">
                            <a href="javascript:;">
                                <i class="fa fa-book"></i>
                                <span>Đơn hàng <sup style="color: #FE980F">({{count($j)}})</sup></span></span>
                            </a>
                            <ul class="sub">
                                <li><a href="{{asset('admin/order/list')}}">Danh sách</a></li>
                            </ul>
                        </li>
                    @endif
                    @if(Session::get('admin_id'))
                        <li class="sub-menu">
                            <a href="javascript:;">
                                <i class="fa fa-book"></i>
                                <span>Notice</span>
                            </a>
                            <ul class="sub">
                                <li><a href="{{asset('admin/notice/add')}}">Thêm</a></li>
                                <li><a href="{{asset('admin/notice/list')}}">Danh sách</a></li>
                            </ul>
                        </li>
                        <li class="sub-menu">
                            <a href="javascript:;">
                                <i class="fa fa-book"></i>
                                <span>Phí vận chuyển</span>
                            </a>
                            <ul class="sub">
                                <li><a href="{{asset('admin/delivery/add')}}">Thêm</a></li>
                                <li><a href="{{asset('admin/delivery/list')}}">Danh sách</a></li>
                            </ul>
                        </li>
                        <?php
                        $user_deposit=DB::table('tbl_card_user')->where('status',0)->get();

                        ?>
                        <li class="sub-menu">
                            <a href="javascript:;">
                                <i class="fa fa-book"></i>
                                <span>Desposit <sup style="color:red;">{{count($user_deposit)}}</sup></span>
                            </a>
                            <ul class="sub">
                                <li><a href="{{asset('admin/card/list')}}">Danh sách</a></li>
                            </ul>
                        </li>
                    @endif
<?php

                    if(Session::get('admin_id')){
                        $shop_deposit=DB::table('tbl_card_user')->where('status',0)->get();

                    }else{
                        $shop_deposit=DB::table('tbl_card_user')->where('id_shop',Session::get('id_shop_user'))->where('status',0)->get();
                    }
                    ?>
                    <li class="sub-menu">
                        <a href="javascript:;">
                            <i class="fa fa-book"></i>
                            <span>@if(Session::get('admin_id')) Shop @endif Rút tiền @if(Session::get('admin_id'))
<sup style="color:red;">{{count($shop_deposit)}}</sup>@endif</span>
                        </a>
                        <ul class="sub">
                            @if(!Session::get('admin_id'))
                            <li><a href="{{asset('/admin/withraw/add')}}">Thêm</a></li>
                            @endif
                            <li><a href="{{asset('/admin/withraw/list')}}">Danh sách</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- sidebar menu end-->
        </div>
    </aside>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            @yield('content')
        </section>
        <!-- footer -->
        <div class="footer">
            <div class="wthree-copyright">
                <p>© 2017 Visitors. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>
            </div>
        </div>
        <!-- / footer -->
    </section>
    <!--main content end-->
</section>
<script src="{{asset('public/admin/js/bootstrap.js')}}"></script>
<script src="{{asset('public/admin/js/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{asset('public/admin/js/scripts.js')}}"></script>
<script src="{{asset('public/admin/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('public/admin/js/jquery.nicescroll.js')}}"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="{{asset('public/admin/js/flot-chart/excanvas.min.js')}}"></script><![endif]-->
<script src="{{asset('public/admin/js/jquery.scrollTo.js')}}"></script>
<!-- morris JavaScript -->
<script>
    $(document).ready(function() {
        //BOX BUTTON SHOW AND CLOSE
        jQuery('.small-graph-box').hover(function() {
            jQuery(this).find('.box-button').fadeIn('fast');
        }, function() {
            jQuery(this).find('.box-button').fadeOut('fast');
        });
        jQuery('.small-graph-box .box-close').click(function() {
            jQuery(this).closest('.small-graph-box').fadeOut(200);
            return false;
        });

        //CHARTS
        function gd(year, day, month) {
            return new Date(year, month - 1, day).getTime();
        }

        graphArea2 = Morris.Area({
            element: 'hero-area',
            padding: 10,
            behaveLikeLine: true,
            gridEnabled: false,
            gridLineColor: '#dddddd',
            axes: true,
            resize: true,
            smooth:true,
            pointSize: 0,
            lineWidth: 0,
            fillOpacity:0.85,
            data: [
                {period: '2015 Q1', iphone: 2668, ipad: null, itouch: 2649},
                {period: '2015 Q2', iphone: 15780, ipad: 13799, itouch: 12051},
                {period: '2015 Q3', iphone: 12920, ipad: 10975, itouch: 9910},
                {period: '2015 Q4', iphone: 8770, ipad: 6600, itouch: 6695},
                {period: '2016 Q1', iphone: 10820, ipad: 10924, itouch: 12300},
                {period: '2016 Q2', iphone: 9680, ipad: 9010, itouch: 7891},
                {period: '2016 Q3', iphone: 4830, ipad: 3805, itouch: 1598},
                {period: '2016 Q4', iphone: 15083, ipad: 8977, itouch: 5185},
                {period: '2017 Q1', iphone: 10697, ipad: 4470, itouch: 2038},

            ],
            lineColors:['#eb6f6f','#926383','#eb6f6f'],
            xkey: 'period',
            redraw: true,
            ykeys: ['iphone', 'ipad', 'itouch'],
            labels: ['All Visitors', 'Returning Visitors', 'Unique Visitors'],
            pointSize: 2,
            hideHover: 'auto',
            resize: true
        });


    });
</script>
<!-- calendar -->
<script type="text/javascript" src="{{asset('public/admin/js/monthly.js')}}"></script>
<script type="text/javascript">
    $(window).load( function() {
    });

</script>

<script>
 var option={
     filebrowserImageBrowseUrl:'http://localhost/shopbitcoin/laravel-filemanager?type=Images',
     filebrowserImageUploadUrl:'http://localhost/shopbitcoin/laravel-filemanager/upload?type=Images&_token=',
     filebrowserBrowseUrl:'http://localhost/shopbitcoin/laravel-filemanager/upload?type=Files',
     filebrowserUploadUrl:'http://localhost/shopbitcoin/laravel-filemanager/upload?type=Files&_token='

 };
 CKEDITOR.replace('product_content',option);
    $('#check').click(function (){
        var  address_wallet=$("#address_wallet33").val();

        var  coin_deposit=$("#coin_deposit").val();
        if(address_wallet==''){
            var error='<div class="alert alert-danger">You must enter the address BitcoinSJC</div>'
            $('#kq').html(error);
            return false;
        }
        if(coin_deposit==''){
            var error='<div class="alert alert-danger">You must enter the number of coins to deposit</div>'
            $('#kq').html(error);
            return false;
        }
        $.ajax({
            url : "http://localhost/shopbitcoin/apigetallcoin.php?address="+address_wallet, // gửi ajax đến file result.php
            type : "get", // chọn phương thức gửi là get
            dateType:"text", // dữ liệu trả về dạng text

            beforeSend: function () {

                $("#check").addClass('none');
                $("#check2").removeClass('none');
            },
            success : function (result){
                // Sau khi gửi và kết quả trả về thành công thì gán nội dung trả về
                // đó vào thẻ div có id = result
                var tach=result.split('wallet:');
                var tach1=tach[0].split('coin: ');

                var tach2=tach1[1].split('<br />');

                if(tach2[0]!='false'){
                    var error='<div class="alert alert-success">Verified your wallet </div>';
                    $('#kq').html(error);
                    $('#address_wallet_back').html('ok');
                    $('#lll').prop('disabled', false);

                    var tach=result.split('wallet');
                    var tach1=tach[0].split('coin: ');
                    var tach2=tach1[1].split('<br />');

                }else{
                    var error='<div class="alert alert-danger">Your wallet address does not exist </div>';

                    $('#kq').html(error);
                }
            },
            complete: function () {
                $("#check").removeClass('none');
                $("#check2").addClass('none');
            },
        });
    });
</script>

</body>
</html>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src='https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.0/morris.min.js'></script>
<!-- //calendar -->
<script>
    $(document).ready(function (){


        chart30daysorder();
        var chart=new Morris.Bar({
            element:'myfirstchat',
            parseTime:false,
            hideHover:"auto",
            xkey:'period',
            pointStrokeColors:['black'],
            ykeys:['order','sales','profit','quantity'],
            labels:['đơn hàng','doanh số','lợi nhuận','số lương']

        });
<?php
        $product_shop=DB::table('tbl_product_shop')->where('id_shop',Session::get('id_shop_user'))->get();
        if(Session::get('admin_id')) {
            $account = DB::table('tbl_customer')->where('verifyKYC', 'Y')->get()->count();
            $account_u = DB::table('tbl_customer')->get()->count();
            $account_p = DB::table('tbl_customer')->get()->count();
            $account_r = DB::table('tbl_customer')->get()->count();
        }else{

            $account =0;
            $account_u = 0;
            $account_p = 0;
            $account_r = 0;
        }


        $product=0;
        foreach ($product_shop as $p_s) {
            $product++;
        }

        ?>
        @if(!Session::get('admin_id'))
        var donut=Morris.Donut({
            element: 'donut',
            resize:true,
            color:[
                '#a8328e'
            ],
            data:[
                {label:"Sản phẩm", value:{{$product}}}
            ]
        });
        @endif
        @if(Session::get('admin_id'))

        var donut1=Morris.Donut({
            element: 'donut1',
            resize:true,
            color:[
                '#ea0610','#0c13a3','#1addad','#ea0610',
            ],
            data:[
                {label:"Tài khoản đã KYC", value:{{$account}}},
                {label:"Tài khoản chưa KYC", value:{{$account_u}}},
                {label:"Tài khoản chờ duyệt KYC", value:{{$account_p}}},
                {label:"Tài khoản từ chối KYC", value:{{$account_r}}}
            ]
        });
        @endif
        $('div svg text').css({
            "font-family" : "'Roboto",

        });

        function chart30daysorder(){
            $.ajax({
                url:"{{asset('admin/30day')}}",
                method:'Get',
                dataType:'JSON',
                success:function (data){
                    chart.setData(data);

                }
            });
        }

        $('.dashboard-filter').change(function (){
            var dashboard_value=$(this).val();

            $.ajax({
                url: "{{asset('admin/dashboard-filter')}}",
                method: "get",
                dataType: "JSON",
                data: {dashboard_value: dashboard_value},
                success: function (data) {
                    chart.setData(data);

                }
            });
        });
        $('#btn-dashboard-filter').click(function (){

            var from_date=$('#datepicker').val();
            var to_date=$('#datepicker2').val();

            $.ajax({
                url:"{{asset('admin/filter-by-date')}}",
                method:'get',
                dataType:"JSON",
                data:{from_date:from_date,to_date:to_date},
                success:function (data){
                    chart.setData(data);

                }
            })
        });
        $(function (){
            $("#datepicker").datepicker({
                prevText:"Tháng trước",
                nextText:"Tháng sau",
                dateFormat: 'yy-mm-dd',
                dayNamesMin:['Thứ 2','Thứ 3','Thứ 4','Thứ 5','Thứ 6','Thứ 7','Chủ nhật'],
                duration:'slow'

            });
            $("#datepicker2").datepicker({
                prevText:"Tháng trước",
                nextText:"Tháng sau",
                dateFormat: 'yy-mm-dd' ,
                dayNamesMin:['Thứ 2','Thứ 3','Thứ 4','Thứ 5','Thứ 6','Thứ 7','Chủ nhật'],
                duration:'slow'

            });
        });
    });


</script>

<script src="{{asset('public/admin/dist_datatable/script.js')}}"></script>
