@extends('admin.master');
@section('content')
    <div class="table-agile-info">
        <div class="panel panel-default">
            <div class="panel-heading">
                Danh sách danh mục
            </div>
            <div class="row w3-res-tb">
                <div class="col-sm-5 m-b-xs">
                    <select class="input-sm form-control w-sm inline v-middle">
                        <option value="0">Bulk action</option>
                        <option value="1">Delete selected</option>
                        <option value="2">Bulk edit</option>
                        <option value="3">Export</option>
                    </select>
                    <button class="btn btn-sm btn-default">Apply</button>
                </div>
                <div class="col-sm-4">
                </div>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input type="text" class="input-sm form-control" placeholder="Search">
                        <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button">Go!</button>
          </span>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                @include('admin.errors.error')
                <table class="table table-striped b-t b-light">
                    <thead>
                    <tr>
                        <th style="width:20px;">
                            <label class="i-checks m-b-none">
                                <input type="checkbox"><i></i>
                            </label>
                        </th>
                        <th>Tên Danh mục</th>

                        <th>Danh mục Cha</th>
                        <th>Ẩn/Hiện</th>
                        <th style="width:30px;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($category_shop as $d)
                        <?php

                         $cap=DB::table('tbl_category_product')->where('id_category_product',$d->id_category_product)->first();
                         $cap_=DB::table('tbl_category_product')->where('id_category_product',$cap->category_parent)->first();

                        ?>
                    <tr>
                        <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                        @if($cap_!=null)
                        <td>{{$cap_->category_name}}</td>

                        <td><span class="text-ellipsis">{{$cap_->category_name}}</span></td>
                        @else
                            <td>{{$cap->category_name}}</td>

                            <td><span class="text-ellipsis">None</span></td>
                        @endif
                        @if($cap_!=null)

                        <td>
                            @if($cap_->category_status==1)
                                <a href="{{asset('admin/category/unactive/'.$cap->id_category_product)}}"> <span class="fa fa-thumbs-up"style="color: green;font-size: 20px"></span></a>
                            @else
                                <a href="{{asset('admin/category/active/'.$cap->id_category_product)}}"> <span class="fa fa-thumbs-down" style="color: red;font-size: 20px"></span></a>
                            @endif
                        </td>
                        @else

                            <td>
                                @if($cap->category_status==1)
                                    <a href="{{asset('admin/category/unactive/'.$cap->id_category_product)}}"> <span class="fa fa-thumbs-up"style="color: green;font-size: 20px"></span></a>
                                @else
                                    <a href="{{asset('admin/category/active/'.$cap->id_category_product)}}"> <span class="fa fa-thumbs-down" style="color: red;font-size: 20px"></span></a>
                                @endif
                            </td>
                        @endif
                        <td>
                            <a href="{{asset('admin/category/edit/'.$cap->id_category_product)}}" class="active" ui-toggle-class=""><i class="fa fa-pencil-square-o text-success text-active"></i></a> <a href="{{asset('category/del/'.$cap->id_category_product)}}" onclick="confirm('Bạn có chắc xóa không?')"> <i class="fa fa-times text-danger text"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <footer class="panel-footer">
                <div class="row">

                    <div class="col-sm-7 text-right text-center-xs">
                        {{$category_shop->links('admin.paginate')}}
                    </div>
                </div>
            </footer>
        </div>
    </div>

@endsection
