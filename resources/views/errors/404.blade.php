<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="{{asset('public/Eshopper/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="{{asset('public/Eshopper/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/main.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/responsive.css')}}" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="{{asset('public/Eshopper/js/html5shiv.js')}}"></script>
    <script src="{{asset('public/Eshopper/js/respond.min.js')}}"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{asset('public/Eshopper/images/ico/favicon.ico')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('public/Eshopper/images/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('public/Eshopper/images/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('public/Eshopper/images/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('public/Eshopper/images/ico/apple-touch-icon-57-precomposed.png')}}">
</head><!--/head-->

<body>
<header id="header"><!--header-->
    <div class="header_top"><!--header_top-->
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                            <li><a href="#"><i class="fa fa-envelope"></i> info@domain.com</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="social-icons pull-right">
                        <ul class="nav navbar-nav">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header_top-->

    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="index.html"><img src="{{asset('public/Eshopper/images/home/logo.png')}}" alt="" /></a>
                    </div>

                </div>
                <div class="col-sm-8">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">@if(Session::get('customer_id')!='')
                                <li><a href="{{asset('account')}}"><i class="fa fa-user"></i> Tài khoản</a></li>

                                <li><a href="#"><i class="fa fa-star"></i> Yêu thích</a></li>
                            @endif
                            <li><a href="{{asset('checkout')}}"><i class="fa fa-crosshairs"></i> Thanh toán</a></li>
                            <li><a href="{{asset('cart')}}"><i class="fa fa-shopping-cart"></i> Giỏ hàng @if(count(Cart::content())>0)<sup style="color:red;">({{count(Cart::content())}})</sup>@endif</a></li>
                            @if(!Session::get('customer_id'))
                                <li><a href="{{asset('login')}}"><i class="fa fa-lock"></i> Đăng nhập</a></li>
                            @else
                                <li><a href="{{asset('logoutacount')}}"><i class="fa fa-lock"></i> Đăng xuất</a></li>
                            @endif                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="{{asset('/')}}" class="active">Trang chủ</a></li>
                            <li class="dropdown"><a href="{{asset('product')}}">Sản phẩm</a>

                            </li>
                            <li class="dropdown"><a href="{{asset('news')}}">Tin tức</a>

                            </li>
                            <li><a href="{{asset('contact')}}">Liên hệ</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="search_box pull-right">
                        <form method="post" action="{{asset('search')}}">
                            @csrf
                            <input  type="text" name="keyword" placeholder="Tìm kiếm sản phẩm"/>
                            <input  style="margin-top: 0px;color:white;"type="submit" class="btn btn-primary" value="Tìm"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-bottom-->
</header><!--/header-->

<section id="slider"><!--slider-->
    <div id="slider-carousel" class="carousel slide" data-ride="carousel">

        <?php
        $i=0;
        $slider =DB::table('tbl_slider')->where('status',1)->where('type',0)->get();

        $category=DB::table('tbl_category_product')->where('category_status',1)->get();

        $brands=DB::table('tbl_brands_product')->where('brands_status',1)->get();
        ?>

        <ol class="carousel-indicators">
            @for($j=0;$j<count($slider);$j++)
                <li data-target="#slider-carousel" data-slide-to="{{$j}}" class="@if($j==1) active @endif"></li>
            @endfor
        </ol>
        <div class="carousel-inner">
            @foreach($slider as $s)
                <?php $i++;?>
                <div class="item @if($i==1)active @endif" style="padding-left: 0px;">
                    <div class="col-sm-12" style="padding-left: 0px !important;padding-right: 0px !important;">
                        <img src="{{asset('upload/slider/'.$s->images)}}"   alt="" width="100%" height="300px"/>
                    </div>
                </div>
            @endforeach
        </div>

        <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
            <i class="fa fa-angle-left"></i>
        </a>
        <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
            <i class="fa fa-angle-right"></i>
        </a>
    </div>
</section><!--/slider-->

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Danh mục</h2>
                    <div class="panel-group category-products" id="accordian"><!--category-productsr-->

                        <div class="panel panel-default">
                            @foreach($category as $c)
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><a href="{{asset('category/'.$c->category_name_slug)}}">{{$c->category_name}}</a></h4>
                                    </div>
                                </div>

                            @endforeach
                        </div>

                    </div><!--/category-products-->

                    <div class="brands_products"><!--brands_products-->
                        <h2>Thương hiệu sản phẩm</h2>
                        <div class="brands-name">
                            <ul class="nav nav-pills nav-stacked">
                                @foreach($brands as $b)
                                    <?php
                                    $product=DB::table('tbl_product')->where('brands_id',$b->id_brands_product)->get();

                                    ?>
                                    <li><a href="{{asset('brands/'.$b->brands_name_slug)}}"> <span class="pull-right">({{count($product)}})</span>{{$b->brands_name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div><!--/brands_products-->

                    <div class="price-range"><!--price-range-->
                        <h2>Price Range</h2>
                        <div class="well text-center">
                            <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
                            <b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>
                        </div>
                    </div><!--/price-range-->

                    <div class="shipping text-center"><!--shipping-->
                        <img src="{{asset('public/Eshopper/images/home/shipping.jpg')}}" alt="" />
                    </div><!--/shipping-->

                </div>
            </div>

            <div class="col-sm-9">

                            <center>       <div class="error-template">
                                <h1>
                                    Oops!</h1>
                                <h2>
                                    404 Not Found</h2>
                                <div class="error-details">
                                    Sorry, an error has occured, Requested page not found!
                                </div>
                                <div class="error-actions">
                                    <a href="{{asset('/')}}" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span></a>
                                </div>
                            </div>
                            </center>



                </div>
            </div>
        </div>
    </div>
</section>

<footer id="footer"><!--Footer-->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="companyinfo">
                        <h2><span>e</span>-shopper</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor</p>
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="col-sm-3">
                        <div class="video-gallery text-center">
                            <a href="#">
                                <div class="iframe-img">
                                    <img src="{{asset('public/Eshopper/images/home/iframe1.png')}}" alt="" />
                                </div>
                                <div class="overlay-icon">
                                    <i class="fa fa-play-circle-o"></i>
                                </div>
                            </a>
                            <p>Circle of Hands</p>
                            <h2>24 DEC 2014</h2>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="video-gallery text-center">
                            <a href="#">
                                <div class="iframe-img">
                                    <img src="{{asset('public/Eshopper/images/home/iframe2.png')}}" alt="" />
                                </div>
                                <div class="overlay-icon">
                                    <i class="fa fa-play-circle-o"></i>
                                </div>
                            </a>
                            <p>Circle of Hands</p>
                            <h2>24 DEC 2014</h2>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="video-gallery text-center">
                            <a href="#">
                                <div class="iframe-img">
                                    <img src="{{asset('public/Eshopper/images/home/iframe3.png')}}" alt="" />
                                </div>
                                <div class="overlay-icon">
                                    <i class="fa fa-play-circle-o"></i>
                                </div>
                            </a>
                            <p>Circle of Hands</p>
                            <h2>24 DEC 2014</h2>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="video-gallery text-center">
                            <a href="#">
                                <div class="iframe-img">
                                    <img src="{{asset('public/Eshopper/images/home/iframe4.png')}}" alt="" />
                                </div>
                                <div class="overlay-icon">
                                    <i class="fa fa-play-circle-o"></i>
                                </div>
                            </a>
                            <p>Circle of Hands</p>
                            <h2>24 DEC 2014</h2>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="address">
                        <img src="{{asset('public/Eshopper/images/home/map.png')}}" alt="" />
                        <p>505 S Atlantic Ave Virginia Beach, VA(Virginia)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-widget">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>Service</h2>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">Online Help</a></li>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Order Status</a></li>
                            <li><a href="#">Change Location</a></li>
                            <li><a href="#">FAQ’s</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>Quock Shop</h2>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">T-Shirt</a></li>
                            <li><a href="#">Mens</a></li>
                            <li><a href="#">Womens</a></li>
                            <li><a href="#">Gift Cards</a></li>
                            <li><a href="#">Shoes</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>Policies</h2>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">Terms of Use</a></li>
                            <li><a href="#">Privecy Policy</a></li>
                            <li><a href="#">Refund Policy</a></li>
                            <li><a href="#">Billing System</a></li>
                            <li><a href="#">Ticket System</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>About Shopper</h2>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">Company Information</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Store Location</a></li>
                            <li><a href="#">Affillate Program</a></li>
                            <li><a href="#">Copyright</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 col-sm-offset-1">
                    <div class="single-widget">
                        <h2>About Shopper</h2>
                        <form action="#" class="searchform">
                            <input type="text" placeholder="Your email address" />
                            <button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
                            <p>Get the most recent updates from <br />our site and be updated your self...</p>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">Copyright © 2018 BitCoinSJC Inc. All rights reserved.</p>
                <p class="pull-right">Designed by BitCoinSJC and Sponsored by BitCoinSJC</p>
            </div>
        </div>
    </div>

</footer><!--/Footer-->



<script src="{{asset('public/Eshopper/js/jquery.js')}}"></script>
<script src="{{asset('public/Eshopper/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/Eshopper/js/jquery.scrollUp.min.js')}}"></script>
<script src="{{asset('public/Eshopper/js/price-range.js')}}"></script>
<script src="{{asset('public/Eshopper/js/jquery.prettyPhoto.js')}}"></script>
<script src="{{asset('public/Eshopper/js/main.js')}}"></script>
</body>
</html>
<script>
    function  remove_background(product_id){
        for(var count=1;count<=5;count++){
            $("#"+product_id+'-'+count).css('color','#ccc');
        }
    }
    $(document).on('mouseenter','.rating',function (){
        var index =$(this).data('index');
        var product_id=$(this).data('product_id');

        remove_background(product_id);
        for(var count=1;count<=index;count++){
            $("#"+product_id+'-'+count).css('color','#ffcc00');
        }
    });
    $(document).on('mouseleave','.rating',function (){
        var index =$(this).data('index');
        var product_id=$(this).data('product_id');
        var rating=$(this).data('rating');
        remove_background(product_id);
        for(var count=1;count<=rating;count++){
            $("#"+product_id+'-'+count).css('color','#ffcc00');
        }
    });
    $(document).on('click','.rating',function (){
        var index =$(this).data('index');
        var product_id=$(this).data('product_id');
        $.ajax({
            url:"{{asset('rating')}}",
            method:'get',
            data:{index:index,product_id:product_id},
            success:function (data){
                if(data=="done"){
                    alert("Bạn đã đánh giá "+index+" trên 5");
                }else {
                    alert("Lỗi đánh giá");
                }
            }
        })
    });

</script>
