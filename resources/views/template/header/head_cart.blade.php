<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="{{asset('public/Eshopper/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="{{asset('public/Eshopper/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/main.css')}}" rel="stylesheet">
    <link href="{{asset('public/Eshopper/css/responsive.css')}}" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="{{asset('public/Eshopper/js/html5shiv.js')}}"></script>
    <script src="{{asset('public/Eshopper/js/respond.min.js')}}"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{asset('public/Eshopper/images/ico/favicon.ico')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('public/Eshopper/images/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('public/Eshopper/images/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('public/Eshopper/images/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('public/Eshopper/images/ico/apple-touch-icon-57-precomposed.png')}}">
</head><!--/head-->
