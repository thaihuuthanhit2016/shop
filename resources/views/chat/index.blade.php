<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="UTF-8">
    <title>CodePen - bootstrap chat box</title>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css'>
    <link rel="stylesheet" href="{{asset('public/chat/style.css')}}">

</head>
<body>
<!-- partial:index.partial.html -->
<div class="row bootstrap snippets">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <!-- DIRECT CHAT PRIMARY -->
        <div class="box box-primary direct-chat direct-chat-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Direct Chat</h3>

                <div class="box-tools pull-right">
                    <span data-toggle="tooltip" title="" class="badge bg-light-blue" data-original-title="3 New Messages">3</span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                        <i class="fa fa-comments"></i> </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- Conversations are loaded here -->
                <div class="direct-chat-messages">
                    <!-- Message. Default to the left -->
                    <div id="kq"></div>                    <!-- /.direct-chat-msg -->
                </div>

                <input type="hidden" id="ss" value="{{$id}}">
                <input type="hidden" id="id_login" value="{{Session::get('unique_id')}}">

                <!--/.direct-chat-messages-->

                <!-- Contacts are loaded here -->
                <!-- /.direct-chat-pane -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <form method="post">
                    @csrf
                    <div class="input-group">
                        <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                        <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary btn-flat">Send</button>
                  </span>
                    </div>
                </form>
            </div>
            <!-- /.box-footer-->
        </div>
        <!--/.direct-chat -->
    </div>
    <div class="col-md-4"></div>

</div>
<!-- partial -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    $(document).ready(function() {

        setInterval(function () {
            loaddata();
        }, 1000);
    });
    function loaddata(){
        $.ajax({
            url : "{{asset('loaddata')}}",
            type : "get",
            dataType:"text",
            data : {
                id:$('#ss').val(),
                id_login:$('#id_login').val(),
            },
            success : function (result){
                $('#kq').html(result);
            }
        });
    }
</script>
</body>
</html>
